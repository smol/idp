package integrationtest

import (
	"context"
	"fmt"
	"strings"
	"testing"
	"time"

	"git.autistici.org/smol/idp/internal/testutil"
	"github.com/zitadel/oidc/v3/pkg/client/rp"
	"github.com/zitadel/oidc/v3/pkg/oidc"
)

var deviceScopes = []string{"email", "profile", "offline_access", "openid"}

func TestOIDC_Device(t *testing.T) {
	srvURL, cleanup := makeTestServer(t)
	defer cleanup()

	ctx := context.Background()

	// Give the server a tiny bit of time to actually start.
	time.Sleep(100 * time.Millisecond)

	oidcClient := testutil.NewClient()
	provider, err := rp.NewRelyingPartyOIDC(
		ctx,
		srvURL,
		"device",
		"devicesecret",
		"",
		deviceScopes,
		rp.WithHTTPClient(oidcClient.Client),
	)
	if err != nil {
		t.Fatalf("rp.NewRelyingPartyOIDC: %v", err)
	}

	// Start the device authorization process.
	resp, err := rp.DeviceAuthorization(ctx, deviceScopes, provider, nil)
	if err != nil {
		t.Fatalf("rp.DeviceAuthorization: %v", err)
	}

	targetURL := fmt.Sprintf("%s?user_code=%s", resp.VerificationURI, resp.UserCode)

	// Emulate a browser, and perform the device code authorization workflow.
	ua := testutil.NewUserAgent()

	if err := ua.Request("GET", targetURL, nil); err != nil {
		t.Fatalf("GET %s: %v", targetURL, err)
	}
	if p := ua.URL().Path; !strings.HasPrefix(p, "/auth/login/") {
		t.Fatalf("unexpected URI: %s", ua.URL())
	}

	if err := ua.SubmitForm(map[string]string{
		"username": "test",
		"password": "password",
	}); err != nil {
		t.Fatalf("login error: %v", err)
	}
	if p := ua.URL().Path; !strings.HasPrefix(p, "/auth/device") {
		t.Fatalf("unexpected URI: %s", ua.URL())
	}

	if err := ua.SubmitForm(map[string]string{
		"action": "allow",
	}); err != nil {
		t.Fatalf("user_code submit error: %v", err)
	}
	if p := ua.URL().Path; !strings.HasPrefix(p, "/auth/device") {
		t.Fatalf("unexpected URI: %s", ua.URL())
	}

	// Now on the "original client", retrieve the access token.
	token, err := rp.DeviceAccessToken(ctx, resp.DeviceCode, time.Duration(resp.Interval)*time.Second, provider)
	if err != nil {
		t.Fatalf("device authorization finalization failed: %v", err)
	}

	t.Logf("access token obtained successfully: %+v", token)

	refreshToken := token.RefreshToken

	// Test access token refresh multiple times, just once is not
	// enough to exercise all the token_storage code (namely
	// TokenRequestByRefreshToken and friends).
	for i := 0; i < 3; i++ {
		t.Logf("current refresh token: %s", refreshToken)

		// Attempt renewal using the refresh token.
		newToken, err := rp.RefreshTokens[*oidc.IDTokenClaims](ctx, provider, refreshToken, "", "")
		if err != nil {
			cleanup()
			t.Fatalf("rp.RefreshTokens(attempt %d): %v", i+1, err)
		}

		t.Logf("access token refreshed: %+v", newToken.Token)
		refreshToken = newToken.RefreshToken
	}
}
