package integrationtest

import (
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"testing"
	"time"

	"git.autistici.org/smol/idp"
	"git.autistici.org/smol/idp/internal/cryptutil"
	"git.autistici.org/smol/idp/internal/testutil"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/login"
	idpoidc "git.autistici.org/smol/idp/oidc"
	"git.autistici.org/smol/idp/oidc/storage"
	"git.autistici.org/smol/idp/ui"
	"github.com/coreos/go-oidc/v3/oidc"
	"golang.org/x/oauth2"
)

var fixtures = `
INSERT INTO users (id, username, email, password) VALUES (1, 'test', 'testuser@example.com', 'foo');
INSERT INTO group_membership (group_name, uid) VALUES ('admins', 1);
INSERT INTO users (id, username, email, password) VALUES (2, 'test-otp', 'testuser-otp@example.com', 'foo');
INSERT INTO users (id, username, email, password) VALUES (3, 'test-webauthn', 'testuser-webauthn@example.com', 'foo');
`

func makeTestServer(t *testing.T) (string, func()) {
	t.Helper()

	port := "14093"
	issuer := "http://localhost:" + port

	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}

	udb, cleanup, err := testutil.CreateTestDB(fixtures, dir)
	if err != nil {
		t.Fatalf("failed to init database: %v", err)
	}

	storageC, err := storage.NewStorageWithClients(dir+"/oidc.db", udb, map[string]*storage.Client{
		"web":    storage.WebClient("web", "secret", "http://localhost:14190/callback"),
		"device": storage.DeviceClient("device", "devicesecret"),
	})
	if err != nil {
		t.Fatalf("failed to initialize storage: %v", err)
	}
	storage := storage.Debug(storageC)

	tpl := ui.MustParseTemplates(new(web.Config))

	key := sha256.Sum256([]byte("test"))
	config := &idpoidc.Config{
		Issuer:        issuer,
		EncryptionKey: cryptutil.Binary(key[:]),
	}
	loginConfig := &login.Config{
		AuthService: "login",
	}
	features := &idp.Features{
		Insecure: true,
	}

	loginApp, err := login.NewApp(loginConfig, features, new(testutil.FakeAuthClient), tpl)
	if err != nil {
		t.Fatalf("login app setup error: %v", err)
	}

	router, err := idpoidc.NewApp(config, features, storage, loginApp, tpl)
	if err != nil {
		t.Fatalf("oidc app setup error: %v", err)
	}

	server := startHTTPServer(t, ":"+port, router)

	return issuer, func() {
		server.Close()
		storage.Close()
		cleanup()
		os.RemoveAll(dir)
	}
}

func startHTTPServer(t *testing.T, addr string, h http.Handler) *http.Server {
	t.Helper()

	server := &http.Server{
		Addr:    addr,
		Handler: h,
	}

	go func() {
		if err := server.ListenAndServe(); err != http.ErrServerClosed {
			log.Printf("http.ListenAndServe(): %v", err)
		}
	}()

	return server
}

// Create a OIDC callback Handler, that will attempt a token exchange
// to access the id_token and run a number of sanity checks on it.
func createTestOIDCCallback(client *http.Client, oauth2Config *oauth2.Config, verifier *oidc.IDTokenVerifier, expectedState string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := oidc.ClientContext(r.Context(), client)
		if errMsg := r.FormValue("error"); errMsg != "" {
			http.Error(w, errMsg+": "+r.FormValue("error_description"), http.StatusBadRequest)
			return
		}
		code := r.FormValue("code")
		if code == "" {
			log.Printf("No code in request")
			http.Error(w, "No code in request", http.StatusBadRequest)
			return
		}
		if state := r.FormValue("state"); state != expectedState {
			log.Printf("Bad state")
			http.Error(w, "Bad state", http.StatusBadRequest)
			return
		}
		token, err := oauth2Config.Exchange(ctx, code)
		if err != nil {
			log.Printf("Failed to get token: %v", err)
			http.Error(w, fmt.Sprintf("Failed to get token: %v", err), http.StatusInternalServerError)
			return
		}
		rawIDToken, ok := token.Extra("id_token").(string)
		if !ok {
			log.Printf("No id_token in token response")
			http.Error(w, "no id_token in token response", http.StatusInternalServerError)
			return
		}
		idToken, err := verifier.Verify(r.Context(), rawIDToken)

		if err != nil {
			log.Printf("Failed to verify ID token: %v", err)
			http.Error(w, fmt.Sprintf("failed to verify ID token: %v", err), http.StatusInternalServerError)
			return
		}
		accessToken, ok := token.Extra("access_token").(string)
		if !ok {
			log.Printf("No access_token in token response")
			http.Error(w, "no access_token in token response", http.StatusInternalServerError)
			return
		}
		log.Printf("callback, id_token=%s, access_token=%s", idToken, accessToken)

		// Test group membership claims.
		var claims struct {
			Groups []string `json:"groups"`
		}
		if err := idToken.Claims(&claims); err != nil {
			log.Printf("idToken.Claims(): %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if len(claims.Groups) != 1 || claims.Groups[0] != "admins" {
			log.Printf("unexpected group claims: %v", claims.Groups)
			http.Error(w, "unexpected group claims", http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/?ok=y", http.StatusFound)
	}
}

func createTestOIDCApp(client *http.Client, oauth2Config *oauth2.Config, provider *oidc.Provider) http.Handler {
	state := "boo"
	target := http.NewServeMux()

	target.HandleFunc("/callback", createTestOIDCCallback(
		client,
		oauth2Config,
		provider.Verifier(&oidc.Config{ClientID: "web"}),
		state,
	))

	target.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.FormValue("ok") != "y" {
			http.Redirect(w, r, oauth2Config.AuthCodeURL(state), http.StatusFound)
			return
		}
		io.WriteString(w, "protected") //nolint:errcheck
	})

	return target
}

func TestOIDC(t *testing.T) {
	srvURL, cleanup := makeTestServer(t)
	defer cleanup()

	// Give the server a tiny bit of time to actually start.
	time.Sleep(100 * time.Millisecond)

	oidcClient := testutil.NewClient()
	ctx := oidc.ClientContext(context.Background(), oidcClient.Client)

	provider, err := oidc.NewProvider(ctx, srvURL)
	if err != nil {
		t.Fatalf("oidc.NewProvider: %v", err)
	}

	// Configure the OAuth2 config with the client values.
	oauth2Config := oauth2.Config{
		// client_id and client_secret of the client.
		ClientID:     "web",
		ClientSecret: "secret",

		// Discovery returns the OAuth2 endpoints.
		Endpoint: provider.Endpoint(),

		// "openid" is a required scope for OpenID Connect flows.
		//
		// Other scopes, such as "groups" can be requested.
		Scopes: []string{oidc.ScopeOpenID, "profile", "email", "groups"},
	}

	// Create a test application to validate the OIDC exchange.
	target := createTestOIDCApp(oidcClient.Client, &oauth2Config, provider)

	targetURL := "http://localhost:14190"
	targetSrv := startHTTPServer(t, ":14190", target)
	defer targetSrv.Close()

	oauth2Config.RedirectURL = targetURL + "/callback"

	ua := testutil.NewUserAgent()

	if err := ua.Request("GET", targetURL, nil); err != nil {
		t.Fatalf("GET %s: %v", targetURL, err)
	}
	if p := ua.URL().Path; !strings.HasPrefix(p, "/auth/login/") {
		t.Fatalf("unexpected URI: %s", ua.URL())
	}

	if err := ua.SubmitForm(map[string]string{
		"username": "test",
		"password": "password",
	}); err != nil {
		t.Fatalf("login error: %v", err)
	}
	if s := ua.Data(); s != "protected" {
		t.Fatalf("did not fetch the protected target: '%s'", s)
	}
}
