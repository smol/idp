package idp

type Features struct {
	EnableOTP             bool `yaml:"enable_otp" doc:"enable TOTP 2FA"`
	EnableWebAuthn        bool `yaml:"enable_webauthn" doc:"enable WebAuthN 2FA"`
	EnableASPs            bool `yaml:"enable_app_specific_passwords" doc:"enable app-specific passwords for non-interactive services"`
	EnableMessages        bool `yaml:"enable_messages" doc:"enable broadcast messages"`
	Enforce2FA            bool `yaml:"enforce_2fa" doc:"enforce setup of 2FA for all users"`
	EnableAccountRecovery bool `yaml:"enable_account_recovery" doc:"enable account recovery"`

	Insecure bool `yaml:"insecure" doc:"disable various security measures (cookies, OIDC, ...)"`
}
