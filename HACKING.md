## Tips for hacking on smol/idp

## Code organization

Major components are hosted in top-level subdirectories such as
*auth*, *audit*, *oidc*, *login* and *mgmt*.

When a component has a GRPC API, or generally any public data
associated with it, this is defined in the *proto* subdirectory of the
component. The top-level *proto* directory is used for common data
types like those referenced by the user database (User etc).

The *internal* directory contains lots of smaller packages providing
common functionality. Some highlights:

* *internal/userdb*, the interface to the user database: this offers a
  very narrow API (which simplifies the configurability) to fetch and
  modify authentication-related data of individual users

* *internal/sqlutil*, a minimalistic API for SQLite databases that
  supports migrations

* *internal/sessionstore*, an extension of gorilla/securecookie that
  supports serialization into a struct type instead of their map-based
  API

Finally the *ui* directory contains the sources for the common UI
package (HTML, css, Javascript code), which is built using NPM.

## The *ui* package

Given the complexity of the Javascript bundle-building ecosystem and
the velocity of its evolution, there is really no "good" solution for
a web-oriented JS build system... which is why, after trying various
alternatives, we ultimately ended up with a custom shell script. This
is a bit unfortunate.

The build system uses *sass*, *purgecss* and *esbuild* to produce
minified, pre-compressed assets which are then embedded in the final
binary, along with the HTML templates, using Go's native embedding
functionality.

A few details on the build system:

* It will process SCSS (`.scss`) and Javascript (`.js`) files, and
  copy everything else as-is. It will consider everything not starting
  with an underscore as a top-level file, and process it independently.

* It supports SRI by generating an `assets.go` file defining a map
  (for top-level assets) of asset paths and their paths and hashes, so
  that templates can know about the final path of the processed assets
  (e.g. /assets/something.*hash*.css), and generate the correct SRI
  tags.

There is slightly more detailed information in the comments in
[ui/build.sh](ui/build.sh).
