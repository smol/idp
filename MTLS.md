mTLS Implementation Notes
===

The *idp* GRPC services support mutual TLS authentication and
authorization for service-to-service communication. This assumes the
usage of a locked-down PKI that can be trusted to assert identity.

Authorization works on two levels: at the underlying TLS layer, the
client service must present a valid (i.e. CA-signed) certificate in
order to successfully establish a connection. The `mtls.peer` GRPC
metadata attribute is then set to the Subject Common Name (CN) of the
client certificate.

To authorize individual requests, *idp* uses the EXPERIMENTAL [grpc
authz package](https://pkg.go.dev/google.golang.org/grpc/authz), using
the `mtls.peer` metadata attribute to control access. This policy can
be passed to the server, encoded as a single JSON-encoded value, with
the *--acl* command-line option (or the corresponding *IDP_ACL*
environment variable).

An example of what such a policy might look like for the "auth"
service in a hypothetical setup, only allowing requests for the AuthN
service to the "idp" service (assumed to have a client certificate
with a CN of *idp.auth.svc.cluster.local*):

```json
{
  "name": "my_policy",
  "allow_rules": [
    {
      "name": "allow_AuthN",
      "request": {
        "paths": ["/authn.AuthN/*"],
        "headers": [
          "key": "mtls.peer",
          "values": ["idp.auth.svc.cluster.local"]
        ]
      }
    }
  ],
  "deny_rules": []
}
```
