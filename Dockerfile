FROM docker.io/library/golang:1.23.2 as build
RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -qy install --no-install-recommends \
        npm protobuf-compiler protoc-gen-go protoc-gen-go-grpc brotli xxd
ADD . /src
RUN cd /src && \
    go generate ./... && \
    go build -ldflags="-extldflags=-static" -tags "sqlite_omit_load_extension netgo osusergo" -o smol-idp ./cmd/smol-idp && \
    strip smol-idp

FROM scratch
COPY --from=build /src/smol-idp /smol-idp

ENTRYPOINT ["/smol-idp"]

