package storage

import (
	"context"
	"errors"

	"git.autistici.org/smol/idp/internal/cryptutil"
	jose "github.com/go-jose/go-jose/v4"
	"github.com/zitadel/oidc/v3/pkg/op"
)

type JWTKey struct {
	KeyID     string                   `yaml:"id"`
	Algorithm jose.SignatureAlgorithm  `yaml:"alg"`
	Key       *cryptutil.RSAPrivateKey `yaml:"key"`
}

func (k *JWTKey) ID() string {
	return k.KeyID
}

type signingKey struct {
	*JWTKey
}

func (k *signingKey) Key() any {
	return k.JWTKey.Key.PrivateKey
}

func (k *signingKey) SignatureAlgorithm() jose.SignatureAlgorithm {
	return k.JWTKey.Algorithm
}

type publicKey struct {
	*JWTKey
}

func (k *publicKey) Algorithm() jose.SignatureAlgorithm {
	return k.JWTKey.Algorithm
}

func (k *publicKey) Use() string {
	return "sig"
}

func (k *publicKey) Key() any {
	return &k.JWTKey.Key.PublicKey
}

type keyStorage struct {
	db *keyStorageSQL
}

// SigningKey implements the op.Storage interface
// it will be called when creating the OpenID Provider
func (s *keyStorage) SigningKey(ctx context.Context) (op.SigningKey, error) {
	// in this example the signing key is a static rsa.PrivateKey and the algorithm used is RS256
	// you would obviously have a more complex implementation and store / retrieve the key from your database as well
	key := s.db.SigningKey()
	if key == nil {
		return nil, errors.New("no JWT keys")
	}

	return &signingKey{key}, nil
}

// SignatureAlgorithms implements the op.Storage interface
// it will be called to get the sign
func (s *keyStorage) SignatureAlgorithms(ctx context.Context) ([]jose.SignatureAlgorithm, error) {
	key := s.db.SigningKey()
	if key == nil {
		return nil, errors.New("no JWT keys")
	}

	return []jose.SignatureAlgorithm{key.Algorithm}, nil
}

// KeySet implements the op.Storage interface
// it will be called to get the current (public) keys, among others for the keys_endpoint or for validating access_tokens on the userinfo_endpoint, ...
func (s *keyStorage) KeySet(ctx context.Context) ([]op.Key, error) {
	// as mentioned above, this example only has a single signing key without key rotation,
	// so it will directly use its public key
	//
	// when using key rotation you typically would store the public keys alongside the private keys in your database
	// and give both of them an expiration date, with the public key having a longer lifetime
	keys, err := s.db.GetKeys(ctx)
	if err != nil {
		return nil, err
	}

	pubKeys := make([]op.Key, 0, len(keys))
	for _, key := range keys {
		pubKeys = append(pubKeys, &publicKey{key})
	}
	return pubKeys, nil
}
