package storage

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"sync"
	"time"

	"git.autistici.org/smol/idp/internal/cryptutil"
	"git.autistici.org/smol/idp/internal/random"
	"git.autistici.org/smol/idp/internal/sqlutil"
	jose "github.com/go-jose/go-jose/v4"
	"github.com/google/uuid"
	"github.com/zitadel/oidc/v3/pkg/op"
)

var migrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
CREATE TABLE auth_requests (
  id TEXT NOT NULL PRIMARY KEY,
  code TEXT,
  user_id TEXT NOT NULL,
  created_at DATETIME,
  auth_time DATETIME,
  is_done BOOLEAN,
  expiry DATETIME,
  data BLOB
)
`, `
CREATE INDEX idx_auth_requests_code ON auth_requests(code)
`, `
CREATE TABLE access_tokens (
  access TEXT,
  refresh TEXT,
  user_id TEXT NOT NULL,
  client_id TEXT NOT NULL,
  data BLOB,
  expiry DATETIME
)
`, `
CREATE TABLE refresh_tokens (
  refresh TEXT,
  user_id TEXT NOT NULL,
  client_id TEXT NOT NULL,
  data BLOB,
  expiry DATETIME
)
`, `
CREATE INDEX idx_tokens_access ON access_tokens(access)
`, `
CREATE INDEX idx_tokens_refresh ON refresh_tokens(refresh)
`, `
CREATE INDEX idx_access_tokens_user ON access_tokens(user_id)
`, `
CREATE INDEX idx_refresh_tokens_user ON refresh_tokens(user_id)
`, `
CREATE INDEX idx_access_tokens_client ON access_tokens(client_id)
`, `
CREATE INDEX idx_refresh_tokens_client ON refresh_tokens(client_id)
`, `
CREATE TABLE keys (
  id TEXT NOT NULL,
  alg TEXT NOT NULL,
  key BLOB NOT NULL,
  created_at DATETIME,
  expiry DATETIME
)
`, `
CREATE TABLE device_auth (
  user_code TEXT NOT NULL,
  code TEXT NOT NULL,
  client_id TEXT NOT NULL,
  state BLOB NOT NULL,
  expiry DATETIME
)
`, `
CREATE INDEX idx_device_auth_user_code ON device_auth(user_code)
`, `
CREATE INDEX idx_device_auth_code_client_id ON device_auth(code, client_id)
`,
	),
}

var (
	authRequestTTL = 600 * time.Second
	gcInterval     = 60 * time.Second
	gcTables       = []string{"auth_requests", "access_tokens", "refresh_tokens", "keys", "device_auth"}
)

type DB struct {
	db     *sql.DB
	ticker *time.Ticker
}

func NewDB(path string) (*DB, error) {
	db, err := sqlutil.OpenDB(path, sqlutil.WithMigrations(migrations))
	if err != nil {
		return nil, err
	}

	t := &DB{
		db:     db,
		ticker: time.NewTicker(gcInterval),
	}
	go t.gc()
	return t, nil
}

func (s *DB) Close() {
	s.ticker.Stop()
	s.db.Close() //nolint:errcheck
}

func (s *DB) cleanup() error {
	return sqlutil.WithTx(context.Background(), s.db, func(tx *sql.Tx) error {
		for _, table := range gcTables {
			if _, err := tx.Exec(fmt.Sprintf("DELETE FROM %s WHERE expiry <= ?", table), time.Now()); err != nil {
				return err
			}
		}
		return nil
	})
}

func (s *DB) gc() {
	for range s.ticker.C {
		if err := s.cleanup(); err != nil {
			log.Printf("database cleanup error: %v", err)
		}
	}
}

type authStorageSQL struct {
	db *sql.DB
}

func (s *authStorageSQL) CreateAuthRequest(ctx context.Context, req *AuthRequest) error {
	data, err := json.Marshal(req.AuthRequest)
	if err != nil {
		return err
	}

	expiry := time.Now().Add(authRequestTTL)

	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec(
			"INSERT INTO auth_requests (id, user_id, created_at, auth_time, is_done, expiry, data) VALUES (?, ?, ?, ?, ?, ?, ?)",
			req.ID, req.UserID, req.CreatedAt, req.AuthTime, req.IsDone, expiry, data,
		)
		return err
	})
}

func (s *authStorageSQL) SaveAuthCode(ctx context.Context, id, code string) error {
	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec("UPDATE auth_requests SET code = ? WHERE id = ?", code, id)
		return err
	})
}

func (s *authStorageSQL) DeleteAuthRequest(ctx context.Context, id string) error {
	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec("DELETE FROM auth_requests WHERE id = ?", id)
		return err
	})
}

func (s *authStorageSQL) getAuthRequestTx(tx *sql.Tx, where, arg string) (*AuthRequest, error) {
	var req AuthRequest
	var data []byte
	var code sql.NullString
	err := tx.QueryRow(fmt.Sprintf(
		"SELECT id, code, user_id, created_at, auth_time, is_done, data FROM auth_requests WHERE %s = ? LIMIT 1", where),
		arg).Scan(&req.ID, &code, &req.UserID, &req.CreatedAt, &req.AuthTime, &req.IsDone, &data)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, ErrNotFound
	} else if err != nil {
		return nil, err
	}

	if code.Valid {
		req.Code = code.String
	}

	if err := json.Unmarshal(data, &req.AuthRequest); err != nil {
		return nil, err
	}

	return &req, nil
}

func (s *authStorageSQL) getAuthRequest(ctx context.Context, where, arg string) (req *AuthRequest, err error) {
	err = sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) (err error) {
		req, err = s.getAuthRequestTx(tx, where, arg)
		return
	})

	return
}

func (s *authStorageSQL) AuthRequestByID(ctx context.Context, id string) (*AuthRequest, error) {
	return s.getAuthRequest(ctx, "id", id)
}

func (s *authStorageSQL) AuthRequestByCode(ctx context.Context, code string) (*AuthRequest, error) {
	return s.getAuthRequest(ctx, "code", code)
}

func (s *authStorageSQL) FinalizeAuthRequest(ctx context.Context, id, userID string) error {
	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec("UPDATE auth_requests SET user_id = ?, is_done = ?, auth_time = ? WHERE id = ?", userID, true, time.Now(), id)
		return err
	})
}

type tokenStorageSQL struct {
	db *sql.DB
}

func (s *tokenStorageSQL) CreateAccessToken(ctx context.Context, token *Token) error {
	data, err := json.Marshal(token)
	if err != nil {
		return err
	}

	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec(
			"INSERT INTO access_tokens (access, refresh, user_id, client_id, expiry, data) VALUES (?, ?, ?, ?, ?, ?)",
			token.ID, token.RefreshTokenID, token.Subject, token.ClientID, token.Expiration, data,
		)
		return err
	})
}

func (s *tokenStorageSQL) CreateRefreshToken(ctx context.Context, token *RefreshToken) error {
	data, err := json.Marshal(token)
	if err != nil {
		return err
	}

	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec(
			"INSERT INTO refresh_tokens (refresh, user_id, client_id, expiry, data) VALUES (?, ?, ?, ?, ?)",
			token.ID, token.UserID, token.ClientID, token.Expiration, data,
		)
		return err
	})
}

// renewRefreshToken checks the provided refresh_token and creates a new one based on the current
func (s *tokenStorageSQL) RenewRefreshToken(ctx context.Context, currentRefreshToken string) (string, string, error) {
	var token string
	err := sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		refreshToken, err := s.getRefreshTokenTx(tx, currentRefreshToken)
		if err != nil {
			return fmt.Errorf("invalid refresh token: %w", err)
		}

		// deletes the refresh token and all access tokens which were issued based on this refresh token
		if _, err := tx.Exec("DELETE FROM refresh_tokens WHERE refresh = ?", currentRefreshToken); err != nil {
			return err
		}
		if _, err := tx.Exec("DELETE FROM access_tokens WHERE refresh = ?", currentRefreshToken); err != nil {
			return err
		}

		// create a new refresh token based on the current one
		token = uuid.NewString()
		refreshToken.Token = token
		refreshToken.ID = token

		data, err := json.Marshal(refreshToken)
		if err != nil {
			return err
		}
		_, err = tx.Exec(
			"INSERT INTO refresh_tokens (refresh, user_id, client_id, expiry, data) VALUES (?, ?, ?, ?, ?)",
			refreshToken.ID, refreshToken.UserID, refreshToken.ClientID, refreshToken.Expiration, data,
		)
		return err
	})

	return token, token, err
}

func (s *tokenStorageSQL) DeleteTokensByUserAndClient(ctx context.Context, userID, clientID string) error {
	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec("DELETE FROM access_tokens WHERE user_id = ? AND client_id = ?", userID, clientID)
		if err != nil {
			return err
		}
		_, err = tx.Exec("DELETE FROM refresh_tokens WHERE user_id = ? AND client_id = ?", userID, clientID)
		return err
	})
}

func (s *tokenStorageSQL) GetToken(ctx context.Context, token string) (*Token, error) {
	var t Token

	err := sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) error {
		var data []byte
		err := tx.QueryRow("SELECT data FROM access_tokens WHERE access = ? LIMIT 1", token).Scan(&data)
		if err != nil {
			return err
		}
		return json.Unmarshal(data, &t)
	})

	return &t, err
}

func (s *tokenStorageSQL) getRefreshTokenTx(tx *sql.Tx, token string) (*RefreshToken, error) {
	var data []byte
	err := tx.QueryRow("SELECT data FROM refresh_tokens WHERE refresh = ? LIMIT 1", token).Scan(&data)
	if err != nil {
		return nil, err
	}

	var rt RefreshToken
	if err := json.Unmarshal(data, &rt); err != nil {
		return nil, err
	}

	return &rt, err
}

func (s *tokenStorageSQL) GetRefreshToken(ctx context.Context, token string) (*RefreshToken, error) {
	var rt *RefreshToken
	err := sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) (err error) {
		rt, err = s.getRefreshTokenTx(tx, token)
		return
	})

	return rt, err
}

func (s *tokenStorageSQL) DeleteTokensByID(ctx context.Context, token, clientID string) error {
	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec("DELETE FROM access_tokens WHERE client_id = ? AND access = ?", clientID, token)
		if err != nil {
			return err
		}
		_, err = tx.Exec("DELETE FROM refresh_tokens WHERE client_id = ? AND refresh = ?", clientID, token)
		return err
	})
}

type keyStorageSQL struct {
	db     *sql.DB
	ticker *time.Ticker

	// Cache the signing key, it gets requested a lot.
	mx         sync.Mutex
	signingKey *JWTKey
}

func newKeyStorageSQL(db *sql.DB) (*keyStorageSQL, error) {
	ks := &keyStorageSQL{
		db:     db,
		ticker: time.NewTicker(1 * time.Hour),
	}

	if err := ks.init(); err != nil {
		return nil, err
	}

	go ks.rotate()
	return ks, nil
}

// Ensure there's at least one valid JWT key.
func (s *keyStorageSQL) init() error {
	return sqlutil.WithTx(context.Background(), s.db, func(tx *sql.Tx) error {
		var key *JWTKey

		keys, err := s.queryKeys(tx, 1)
		if err == nil && len(keys) > 0 {
			key = keys[0]
		} else {
			key, err = s.createNewKey(tx)
			if err != nil {
				return err
			}
		}

		s.mx.Lock()
		s.signingKey = key
		s.mx.Unlock()

		return nil
	})
}

func (s *keyStorageSQL) Close() {
	s.ticker.Stop()
}

func (s *keyStorageSQL) SigningKey() *JWTKey {
	s.mx.Lock()
	defer s.mx.Unlock()
	return s.signingKey
}

func (s *keyStorageSQL) GetKeys(ctx context.Context) ([]*JWTKey, error) {
	var keys []*JWTKey
	err := sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) (err error) {
		keys, err = s.queryKeys(tx, 2)
		return
	})
	return keys, err
}

func (s *keyStorageSQL) queryKeys(tx *sql.Tx, n int) ([]*JWTKey, error) {
	var keys []*JWTKey
	now := time.Now()

	rows, err := tx.Query("SELECT id, alg, key FROM keys WHERE expiry > ? ORDER BY created_at DESC LIMIT ?", now, n)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var id, alg string
		var data []byte
		if err := rows.Scan(&id, &alg, &data); err != nil {
			return nil, err
		}

		key, err := x509.ParsePKCS8PrivateKey(data)
		if err != nil {
			return nil, err
		}

		keys = append(keys, &JWTKey{
			KeyID:     id,
			Algorithm: jose.SignatureAlgorithm(alg),
			Key:       &cryptutil.RSAPrivateKey{PrivateKey: key.(*rsa.PrivateKey)},
		})
	}

	return keys, rows.Err()
}

var keyRotationInterval = 720 * time.Hour

func (s *keyStorageSQL) RotateKeys(ctx context.Context) (*JWTKey, error) {
	var key *JWTKey
	err := sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) (err error) {
		key, err = s.createNewKey(tx)
		s.mx.Lock()
		s.signingKey = key
		s.mx.Unlock()
		return
	})
	return key, err
}

func (s *keyStorageSQL) createNewKey(tx *sql.Tx) (*JWTKey, error) {
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}
	data, err := x509.MarshalPKCS8PrivateKey(key)
	if err != nil {
		return nil, err
	}

	jwtKey := &JWTKey{
		KeyID:     random.UUID(),
		Algorithm: jose.RS256,
		Key:       &cryptutil.RSAPrivateKey{PrivateKey: key},
	}

	now := time.Now()
	expiry := now.Add(2 * keyRotationInterval)

	_, err = tx.Exec(
		"INSERT INTO keys (id, alg, key, created_at, expiry) VALUES (?, ?, ?, ?, ?)",
		jwtKey.KeyID, jwtKey.Algorithm, data, now, expiry)

	return jwtKey, err
}

func (s *keyStorageSQL) shouldRotate(tx *sql.Tx) bool {
	var createdAt time.Time
	if err := tx.QueryRow("SELECT MAX(created_at) FROM keys").Scan(&createdAt); err != nil {
		return true
	}
	return time.Now().After(createdAt.Add(keyRotationInterval))
}

func (s *keyStorageSQL) rotate() {
	for range s.ticker.C {
		err := sqlutil.WithTx(context.Background(), s.db, func(tx *sql.Tx) error {
			if !s.shouldRotate(tx) {
				return nil
			}
			key, err := s.createNewKey(tx)
			s.mx.Lock()
			s.signingKey = key
			s.mx.Unlock()
			return err
		})
		if err != nil {
			log.Printf("error rotating JWT key: %v", err)
		}
	}
}

type deviceAuthSQL struct {
	db *sql.DB
}

func (s *deviceAuthSQL) StoreDeviceAuthorization(ctx context.Context, clientID, deviceCode, userCode string, expires time.Time, scopes []string) error {
	state := &op.DeviceAuthorizationState{
		ClientID: clientID,
		Scopes:   scopes,
		Expires:  expires,
	}

	data, err := json.Marshal(state)
	if err != nil {
		return err
	}

	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		_, err := tx.Exec(
			"INSERT INTO device_auth (user_code, code, client_id, state, expiry) VALUES (?, ?, ?, ?, ?)",
			userCode, deviceCode, clientID, data, expires,
		)
		return err
	})
}

func (s *deviceAuthSQL) GetDeviceAuthorizatonState(ctx context.Context, clientID, deviceCode string) (*op.DeviceAuthorizationState, error) {
	var state op.DeviceAuthorizationState
	err := sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) error {
		var data []byte
		if err := tx.QueryRow(
			"SELECT state FROM device_auth WHERE code = ? AND client_id = ? AND expiry > ?",
			deviceCode, clientID, time.Now(),
		).Scan(&data); err != nil {
			return err
		}

		return json.Unmarshal(data, &state)
	})
	return &state, err
}

func (s *deviceAuthSQL) GetDeviceAuthorizationByUserCode(ctx context.Context, userCode string) (*op.DeviceAuthorizationState, error) {
	var state op.DeviceAuthorizationState
	err := sqlutil.WithReadonlyTx(ctx, s.db, func(tx *sql.Tx) error {
		var data []byte
		if err := tx.QueryRow(
			"SELECT state FROM device_auth WHERE user_code = ? AND expiry > ?",
			userCode, time.Now(),
		).Scan(&data); err != nil {
			return err
		}

		return json.Unmarshal(data, &state)
	})
	return &state, err
}

func (s *deviceAuthSQL) updateState(ctx context.Context, userCode, subject string, ok bool) error {
	return sqlutil.WithTx(ctx, s.db, func(tx *sql.Tx) error {
		var data []byte
		if err := tx.QueryRow(
			"SELECT state FROM device_auth WHERE user_code = ? AND expiry > ?",
			userCode, time.Now(),
		).Scan(&data); err != nil {
			return err
		}

		var state op.DeviceAuthorizationState
		if err := json.Unmarshal(data, &state); err != nil {
			return err
		}

		if ok {
			state.Subject = subject
			state.Done = true
		} else {
			state.Denied = true
		}

		data, err := json.Marshal(&state)
		if err != nil {
			return err
		}

		_, err = tx.Exec("UPDATE device_auth SET state = ? WHERE user_code = ?", data, userCode)
		return err
	})
}

func (s *deviceAuthSQL) CompleteDeviceAuthorization(ctx context.Context, userCode, subject string) error {
	return s.updateState(ctx, userCode, subject, true)
}

func (s *deviceAuthSQL) DenyDeviceAuthorization(ctx context.Context, userCode string) error {
	return s.updateState(ctx, userCode, "", false)
}
