package storage

import (
	"context"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/zitadel/oidc/v3/pkg/oidc"
	"github.com/zitadel/oidc/v3/pkg/op"
)

type authStorage struct {
	db *authStorageSQL
}

// CreateAuthRequest implements the op.Storage interface
// it will be called after parsing and validation of the authentication request
func (s *authStorage) CreateAuthRequest(ctx context.Context, authReq *oidc.AuthRequest, userID string) (op.AuthRequest, error) {
	if len(authReq.Prompt) == 1 && authReq.Prompt[0] == "none" {
		// With prompt=none, there is no way for the user to log in
		// so return error right away.
		return nil, oidc.ErrLoginRequired()
	}

	// typically, you'll fill your storage / storage model with the information of the passed object
	request := newAuthRequest(authReq, userID)

	// and save it in your database (for demonstration purposed we will use a simple map)
	if err := s.db.CreateAuthRequest(ctx, request); err != nil {
		return nil, err
	}

	authRequestsCreated.WithLabelValues(authReq.ClientID).Inc()

	// finally, return the request (which implements the AuthRequest interface of the OP
	return request, nil
}

// AuthRequestByID implements the op.Storage interface
// it will be called after the Login UI redirects back to the OIDC endpoint
func (s *authStorage) AuthRequestByID(ctx context.Context, id string) (op.AuthRequest, error) {
	return s.db.AuthRequestByID(ctx, id)
}

// AuthRequestByCode implements the op.Storage interface
// it will be called after parsing and validation of the token request (in an authorization code flow)
func (s *authStorage) AuthRequestByCode(ctx context.Context, code string) (op.AuthRequest, error) {
	return s.db.AuthRequestByCode(ctx, code)
}

// SaveAuthCode implements the op.Storage interface
// it will be called after the authentication has been successful and before redirecting the user agent to the redirect_uri
// (in an authorization code flow)
func (s *authStorage) SaveAuthCode(ctx context.Context, id string, code string) error {
	return s.db.SaveAuthCode(ctx, id, code)
}

// DeleteAuthRequest implements the op.Storage interface
// it will be called after creating the token response (id and access tokens) for a valid
// - authentication request (in an implicit flow)
// - token request (in an authorization code flow)
func (s *authStorage) DeleteAuthRequest(ctx context.Context, id string) error {
	err := s.db.DeleteAuthRequest(ctx, id)
	if err == nil {
		authRequestsDeleted.Inc()
	}
	return err
}

func (s *authStorage) FinalizeAuthRequest(ctx context.Context, id, userID string) error {
	err := s.db.FinalizeAuthRequest(ctx, id, userID)
	if err == nil {
		authRequestsFinalized.Inc()
	}
	return err
}

// Instrumentation.
var (
	authRequestsCreated = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "idp_oidc_auth_requests_created_total",
			Help: "Number of OIDC AuthRequests created.",
		},
		[]string{"client"},
	)
	authRequestsDeleted = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "idp_oidc_auth_requests_deleted_total",
			Help: "Number of OIDC AuthRequests deleted.",
		},
	)
	authRequestsFinalized = prometheus.NewCounter(
		prometheus.CounterOpts{
			Name: "idp_oidc_auth_requests_finalized_total",
			Help: "Number of OIDC AuthRequests finalized successfully.",
		},
	)
)

func init() {
	prometheus.MustRegister(
		authRequestsCreated,
		authRequestsDeleted,
		authRequestsFinalized,
	)
}
