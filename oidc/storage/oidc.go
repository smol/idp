package storage

import (
	"log/slog"
	"time"

	"git.autistici.org/smol/idp/internal/random"
	"github.com/zitadel/oidc/v3/pkg/oidc"
	"github.com/zitadel/oidc/v3/pkg/op"
)

const (
	// CustomScope is an example for how to use custom scopes in this library
	// (in this scenario, when requested, it will return a custom claim).
	CustomScope = "custom_scope"

	// CustomClaim is an example for how to return custom claims with this library.
	CustomClaim = "custom_claim"

	// CustomScopeImpersonatePrefix is an example scope prefix for
	// passing user id to impersonate using token exchange.
	CustomScopeImpersonatePrefix = "custom_scope:impersonate:"

	GroupsScope = "groups"

	// Custom claim representing groups.
	GroupsClaim = "groups"
)

type Token struct {
	ID             string
	ClientID       string
	Subject        string
	RefreshTokenID string
	Audience       []string
	Expiration     time.Time
	Scopes         []string
}

type RefreshToken struct {
	ID         string
	Token      string
	AuthTime   time.Time
	AMR        []string
	Audience   []string
	UserID     string
	ClientID   string
	Expiration time.Time
	Scopes     []string
}

// AuthRequest type for database serialization.
type AuthRequest struct {
	*oidc.AuthRequest `json:",inline"`

	ID        string    `json:"id"`
	Code      string    `json:"code"`
	UserID    string    `json:"user_id"`
	CreatedAt time.Time `json:"created_at"`
	AuthTime  time.Time `json:"auth_time"`
	IsDone    bool      `json:"done,omitempty"`
}

func newAuthRequest(authReq *oidc.AuthRequest, userID string) *AuthRequest {
	return &AuthRequest{
		ID:          random.UUID(),
		AuthRequest: authReq,
		CreatedAt:   time.Now(),
		UserID:      userID,
	}
}

// LogValue allows you to define which fields will be logged.
// Implements the [slog.LogValuer]
func (a *AuthRequest) LogValue() slog.Value {
	return slog.GroupValue(
		slog.String("id", a.ID),
		slog.Time("created_at", a.CreatedAt),
		slog.Any("scopes", a.Scopes),
		slog.String("response_type", string(a.ResponseType)),
		slog.String("client_id", a.ClientID),
		slog.String("redirect_uri", a.RedirectURI),
	)
}

func (a *AuthRequest) GetID() string {
	return a.ID
}

func (a *AuthRequest) GetACR() string {
	return "" // we won't handle acr in this example
}

func (a *AuthRequest) GetAMR() []string {
	// this example only uses password for authentication
	if a.IsDone {
		return []string{"pwd"}
	}
	return nil
}

func (a *AuthRequest) GetAudience() []string {
	return []string{a.ClientID} // this example will always just use the client_id as audience
}

func (a *AuthRequest) GetAuthTime() time.Time {
	return a.AuthTime
}

func (a *AuthRequest) GetClientID() string {
	return a.ClientID
}

func (a *AuthRequest) GetCodeChallenge() *oidc.CodeChallenge {
	return &oidc.CodeChallenge{
		Challenge: a.CodeChallenge,
		Method:    a.CodeChallengeMethod,
	}
}

func (a *AuthRequest) GetNonce() string {
	return a.Nonce
}

func (a *AuthRequest) GetResponseMode() oidc.ResponseMode {
	return "" // we won't handle response mode in this example
}

func (a *AuthRequest) GetScopes() []string {
	return a.Scopes
}

func (a *AuthRequest) GetSubject() string {
	return a.UserID
}

func (a *AuthRequest) Done() bool {
	return a.IsDone
}

// RefreshTokenRequestFromBusiness will simply wrap the storage RefreshToken to implement the op.RefreshTokenRequest interface
func RefreshTokenRequestFromBusiness(token *RefreshToken) op.RefreshTokenRequest {
	return &RefreshTokenRequest{token}
}

type RefreshTokenRequest struct {
	*RefreshToken
}

func (r *RefreshTokenRequest) GetAMR() []string {
	return r.AMR
}

func (r *RefreshTokenRequest) GetAudience() []string {
	return r.Audience
}

func (r *RefreshTokenRequest) GetAuthTime() time.Time {
	return r.AuthTime
}

func (r *RefreshTokenRequest) GetClientID() string {
	return r.ClientID
}

func (r *RefreshTokenRequest) GetScopes() []string {
	return r.Scopes
}

func (r *RefreshTokenRequest) GetSubject() string {
	return r.UserID
}

func (r *RefreshTokenRequest) SetCurrentScopes(scopes []string) {
	r.Scopes = scopes
}
