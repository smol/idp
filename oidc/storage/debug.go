package storage

import (
	"context"
	"log"
	"time"

	"github.com/zitadel/oidc/v3/pkg/oidc"
	"github.com/zitadel/oidc/v3/pkg/op"
)

type DebugStorage struct {
	*Storage
}

func Debug(s *Storage) *DebugStorage {
	return &DebugStorage{Storage: s}
}

func (d *DebugStorage) CreateAuthRequest(ctx context.Context, authReq *oidc.AuthRequest, userID string) (op.AuthRequest, error) {
	log.Printf("storage.CreateAuthRequest(%s): %+v", userID, authReq)
	return d.Storage.CreateAuthRequest(ctx, authReq, userID)
}

func (d *DebugStorage) AuthRequestByCode(ctx context.Context, code string) (op.AuthRequest, error) {
	req, err := d.Storage.AuthRequestByCode(ctx, code)
	log.Printf("storage.AuthRequestByCode(%s), err=%v", code, err)
	return req, err
}

func (d *DebugStorage) AuthRequestByID(ctx context.Context, id string) (op.AuthRequest, error) {
	req, err := d.Storage.AuthRequestByID(ctx, id)
	log.Printf("storage.AuthRequestByID(%s), err=%v", id, err)
	return req, err
}

func (d *DebugStorage) SaveAuthCode(ctx context.Context, id, code string) error {
	log.Printf("storage.SaveAuthCode(%s, %s)", id, code)
	return d.Storage.SaveAuthCode(ctx, id, code)
}

func (d *DebugStorage) DeleteAuthRequest(ctx context.Context, id string) error {
	log.Printf("storage.DeleteAuthRequest(%s)", id)
	return d.Storage.DeleteAuthRequest(ctx, id)
}

func (d *DebugStorage) FinalizeAuthRequest(ctx context.Context, id, userID string) error {
	log.Printf("storage.FinalizeAuthRequest(%s, %s)", id, userID)
	return d.Storage.FinalizeAuthRequest(ctx, id, userID)
}

func (d *DebugStorage) CreateAccessToken(ctx context.Context, request op.TokenRequest) (string, time.Time, error) {
	token, expiry, err := d.Storage.CreateAccessToken(ctx, request)
	log.Printf("storage.CreateAccessToken(subj=%s, scopes=%v) -> %s", request.GetSubject(), request.GetScopes(), token)
	return token, expiry, err
}

func (d *DebugStorage) CreateAccessAndRefreshTokens(ctx context.Context, request op.TokenRequest, currentRefreshToken string) (accessTokenID string, newRefreshToken string, expiration time.Time, err error) {
	accessTokenID, newRefreshToken, expiration, err = d.Storage.CreateAccessAndRefreshTokens(ctx, request, currentRefreshToken)
	log.Printf("storage.CreateAccessAndRefreshTokens(subj=%s) -> %s, %s", request.GetSubject(), accessTokenID, newRefreshToken)
	return
}

func (d *DebugStorage) TerminateSession(ctx context.Context, userID, clientID string) error {
	log.Printf("storage.TerminateSession(%s, %s)", userID, clientID)
	return d.Storage.TerminateSession(ctx, userID, clientID)
}

func (d *DebugStorage) TokenRequestByRefreshToken(ctx context.Context, refreshToken string) (op.RefreshTokenRequest, error) {
	log.Printf("storage.TokenRequestByRefreshToken(%s)", refreshToken)
	return d.Storage.TokenRequestByRefreshToken(ctx, refreshToken)
}

func (d *DebugStorage) GetRefreshTokenInfo(ctx context.Context, clientID string, token string) (userID string, tokenID string, err error) {
	log.Printf("storage.GetRefreshTokenInfo(%s)", token)
	return d.Storage.GetRefreshTokenInfo(ctx, clientID, token)
}

func (d *DebugStorage) RevokeToken(ctx context.Context, tokenIDOrToken, userID, clientID string) *oidc.Error {
	log.Printf("storage.RevokeToken(%s, %s, %s)", tokenIDOrToken, userID, clientID)
	return d.Storage.RevokeToken(ctx, tokenIDOrToken, userID, clientID)
}

func (d *DebugStorage) SetUserinfoFromRequest(ctx context.Context, userinfo *oidc.UserInfo, token op.IDTokenRequest, scopes []string) error {
	log.Printf("storage.SetUserinfoFromRequest(%s, %v)", token.GetSubject(), scopes)
	return d.Storage.SetUserinfoFromRequest(ctx, userinfo, token, scopes)
}

func (d *DebugStorage) ClientCredentials(ctx context.Context, clientID, clientSecret string) (op.Client, error) {
	log.Printf("storage.ClientCredentials(%s)", clientID)
	return d.Storage.ClientCredentials(ctx, clientID, clientSecret)
}

func (d *DebugStorage) ClientCredentialsTokenRequest(ctx context.Context, clientID string, scopes []string) (op.TokenRequest, error) {
	log.Printf("storage.ClientCredentialsTokenRequest(%s, %v)", clientID, scopes)
	return d.Storage.ClientCredentialsTokenRequest(ctx, clientID, scopes)
}
