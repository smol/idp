package storage

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sync"

	"git.autistici.org/smol/idp/userdb"
	jose "github.com/go-jose/go-jose/v4"
	"github.com/zitadel/oidc/v3/pkg/oidc"
	"github.com/zitadel/oidc/v3/pkg/op"
)

var ErrNotFound = errors.New("not found")

// Storage implements the op.Storage interface.
type Storage struct {
	db *DB

	*clientStorage
	*authStorage
	*tokenStorage
	*tokenExchangeStorage
	*opStorage
	*keyStorage
	*deviceAuthSQL

	serviceUsers map[string]*Client
}

func NewStorageWithClients(dbPath string, users userdb.Backend, clients map[string]*Client) (*Storage, error) {
	db, err := NewDB(dbPath)
	if err != nil {
		return nil, err
	}

	authStorageSQL := &authStorageSQL{db: db.db}
	tokenStorageSQL := &tokenStorageSQL{db: db.db}
	authStorage := &authStorage{db: authStorageSQL}
	tokenStorage := &tokenStorage{db: tokenStorageSQL}
	opStorage := &opStorage{
		userdb:  users,
		tokendb: tokenStorageSQL,
	}
	keyStorageSQL, err := newKeyStorageSQL(db.db)
	if err != nil {
		return nil, fmt.Errorf("initializing JWT keys: %w", err)
	}
	keyStorage := &keyStorage{db: keyStorageSQL}
	deviceAuthSQL := &deviceAuthSQL{db: db.db}
	tokenExchangeStorage := &tokenExchangeStorage{opStorage: opStorage}

	return &Storage{
		db:                   db,
		clientStorage:        newClientStorage(clients),
		authStorage:          authStorage,
		tokenStorage:         tokenStorage,
		tokenExchangeStorage: tokenExchangeStorage,
		opStorage:            opStorage,
		keyStorage:           keyStorage,
		deviceAuthSQL:        deviceAuthSQL,

		serviceUsers: make(map[string]*Client),
	}, nil
}

func (s *Storage) Close() {
	s.keyStorage.db.Close()
	s.db.Close()
}

type clientStorage struct {
	lock    sync.Mutex
	clients map[string]*Client
}

func newClientStorage(clients map[string]*Client) *clientStorage {
	// Set the client ID field from the map keys.
	for k, c := range clients {
		c.ID = k
	}

	return &clientStorage{
		clients: clients,
	}
}

// GetClientByClientID implements the op.Storage interface
// it will be called whenever information (type, redirect_uris, ...) about the client behind the client_id is needed
func (s *clientStorage) GetClientByClientID(ctx context.Context, clientID string) (op.Client, error) {
	s.lock.Lock()
	defer s.lock.Unlock()
	client, ok := s.clients[clientID]
	if !ok {
		return nil, fmt.Errorf("client not found")
	}
	return RedirectGlobsClient(client), nil
}

// AuthorizeClientIDSecret implements the op.Storage interface
// it will be called for validating the client_id, client_secret on token or introspection requests
func (s *clientStorage) AuthorizeClientIDSecret(ctx context.Context, clientID, clientSecret string) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	client, ok := s.clients[clientID]
	if !ok {
		return fmt.Errorf("client not found")
	}
	// for this example we directly check the secret
	// obviously you would not have the secret in plain text, but rather hashed and salted (e.g. using bcrypt)
	if client.Secret != clientSecret {
		return fmt.Errorf("invalid secret")
	}
	return nil
}

// GetKeyByIDAndClientID implements the op.Storage interface
// it will be called to validate the signatures of a JWT (JWT Profile Grant and Authentication)
func (s *clientStorage) GetKeyByIDAndClientID(ctx context.Context, keyID, clientID string) (*jose.JSONWebKey, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	client, ok := s.clients[clientID]
	if !ok {
		return nil, fmt.Errorf("client not found")
	}

	key, ok := client.ServiceKeys[keyID]
	if !ok {
		return nil, fmt.Errorf("key not found")
	}
	return &jose.JSONWebKey{
		KeyID: keyID,
		Use:   "sig",
		Key:   key,
	}, nil
}

// ValidateJWTProfileScopes implements the op.Storage interface
// it will be called to validate the scopes of a JWT Profile Authorization Grant request
func (s *Storage) ValidateJWTProfileScopes(ctx context.Context, userID string, scopes []string) ([]string, error) {
	allowedScopes := make([]string, 0)
	for _, scope := range scopes {
		if scope == oidc.ScopeOpenID {
			allowedScopes = append(allowedScopes, scope)
		}
	}
	return allowedScopes, nil
}

// Health implements the op.Storage interface
func (s *Storage) Health(ctx context.Context) error {
	return nil
}

func (s *Storage) ClientCredentials(ctx context.Context, clientID, clientSecret string) (op.Client, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	log.Printf("ClientCredentials(%s, %s)", clientID, clientSecret)

	client, ok := s.serviceUsers[clientID]
	if !ok {
		return nil, errors.New("wrong service user or password")
	}
	if client.Secret != clientSecret {
		return nil, errors.New("wrong service user or password")
	}

	return client, nil
}

func (s *Storage) ClientCredentialsTokenRequest(ctx context.Context, clientID string, scopes []string) (op.TokenRequest, error) {
	client, ok := s.serviceUsers[clientID]
	if !ok {
		return nil, errors.New("wrong service user or password")
	}

	return &oidc.JWTTokenRequest{
		Subject:  client.ID,
		Audience: []string{clientID},
		Scopes:   scopes,
	}, nil
}
