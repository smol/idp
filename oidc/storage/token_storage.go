package storage

import (
	"context"
	"time"

	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/zitadel/oidc/v3/pkg/oidc"
	"github.com/zitadel/oidc/v3/pkg/op"
)

// getInfoFromRequest returns the clientID, authTime and amr depending on the op.TokenRequest type / implementation
func getInfoFromRequest(req op.TokenRequest) (clientID string, authTime time.Time, amr []string) {
	switch r := req.(type) {
	case *AuthRequest: // Code Flow (with scope offline_access)
		return r.ClientID, r.AuthTime, r.GetAMR()
	case *RefreshTokenRequest: // Refresh Token Request
		return r.ClientID, r.AuthTime, r.AMR
	case *op.DeviceAuthorizationState:
		return r.ClientID, r.AuthTime, r.AMR
	default:
		return "", time.Time{}, nil
	}
}

type tokenStorage struct {
	db *tokenStorageSQL
}

// accessToken will store an access_token in-memory based on the provided information
func (s *tokenStorage) accessToken(ctx context.Context, applicationID, refreshTokenID, subject string, audience, scopes []string) (*Token, error) {
	token := &Token{
		ID:             uuid.NewString(),
		ClientID:       applicationID,
		RefreshTokenID: refreshTokenID,
		Subject:        subject,
		Audience:       audience,
		Expiration:     time.Now().Add(5 * time.Minute),
		Scopes:         scopes,
	}

	if err := s.db.CreateAccessToken(ctx, token); err != nil {
		return nil, err
	}

	tokensCreated.WithLabelValues("access", token.ClientID).Inc()

	return token, nil
}

// CreateAccessToken implements the op.Storage interface
// it will be called for all requests able to return an access token (Authorization Code Flow, Implicit Flow, JWT Profile, ...)
func (s *tokenStorage) CreateAccessToken(ctx context.Context, request op.TokenRequest) (string, time.Time, error) {
	var applicationID string
	switch req := request.(type) {
	case *AuthRequest:
		// if authenticated for an app (auth code / implicit flow) we must save the client_id to the token
		applicationID = req.GetClientID()
	case op.TokenExchangeRequest:
		applicationID = req.GetClientID()
	}

	token, err := s.accessToken(ctx, applicationID, "", request.GetSubject(), request.GetAudience(), request.GetScopes())
	if err != nil {
		return "", time.Time{}, err
	}
	return token.ID, token.Expiration, nil
}

// CreateAccessAndRefreshTokens implements the op.Storage interface
// it will be called for all requests able to return an access and refresh token (Authorization Code Flow, Refresh Token Request)
func (s *tokenStorage) CreateAccessAndRefreshTokens(ctx context.Context, request op.TokenRequest, currentRefreshToken string) (accessTokenID string, newRefreshToken string, expiration time.Time, err error) {
	// generate tokens via token exchange flow if request is relevant
	if teReq, ok := request.(op.TokenExchangeRequest); ok {
		return s.exchangeRefreshToken(ctx, teReq)
	}

	// get the information depending on the request type / implementation
	applicationID, authTime, amr := getInfoFromRequest(request)

	// if currentRefreshToken is empty (Code Flow) we will have to create a new refresh token
	if currentRefreshToken == "" {
		refreshTokenID := uuid.NewString()
		accessToken, err := s.accessToken(ctx, applicationID, refreshTokenID, request.GetSubject(), request.GetAudience(), request.GetScopes())
		if err != nil {
			return "", "", time.Time{}, err
		}
		refreshToken, err := s.createRefreshToken(ctx, accessToken, amr, authTime)
		if err != nil {
			return "", "", time.Time{}, err
		}
		return accessToken.ID, refreshToken, accessToken.Expiration, nil
	}

	// if we get here, the currentRefreshToken was not empty, so the call is a refresh token request
	// we therefore will have to check the currentRefreshToken and renew the refresh token
	refreshToken, refreshTokenID, err := s.db.RenewRefreshToken(ctx, currentRefreshToken)
	if err != nil {
		return "", "", time.Time{}, err
	}
	accessToken, err := s.accessToken(ctx, applicationID, refreshTokenID, request.GetSubject(), request.GetAudience(), request.GetScopes())
	if err != nil {
		return "", "", time.Time{}, err
	}

	return accessToken.ID, refreshToken, accessToken.Expiration, nil
}

// createRefreshToken will store a refresh_token in-memory based on the provided information
func (s *tokenStorage) createRefreshToken(ctx context.Context, accessToken *Token, amr []string, authTime time.Time) (string, error) {
	token := &RefreshToken{
		ID:         accessToken.RefreshTokenID,
		Token:      accessToken.RefreshTokenID,
		AuthTime:   authTime,
		AMR:        amr,
		ClientID:   accessToken.ClientID,
		UserID:     accessToken.Subject,
		Audience:   accessToken.Audience,
		Expiration: time.Now().Add(5 * time.Hour),
		Scopes:     accessToken.Scopes,
	}

	if err := s.db.CreateRefreshToken(ctx, token); err != nil {
		return "", err
	}

	tokensCreated.WithLabelValues("refresh", token.ClientID).Inc()

	return token.Token, nil
}

func (s *tokenStorage) exchangeRefreshToken(ctx context.Context, request op.TokenExchangeRequest) (accessTokenID string, newRefreshToken string, expiration time.Time, err error) {
	applicationID := request.GetClientID()
	authTime := request.GetAuthTime()

	refreshTokenID := uuid.NewString()
	accessToken, err := s.accessToken(ctx, applicationID, refreshTokenID, request.GetSubject(), request.GetAudience(), request.GetScopes())
	if err != nil {
		return "", "", time.Time{}, err
	}

	refreshToken, err := s.createRefreshToken(ctx, accessToken, nil, authTime)
	if err != nil {
		return "", "", time.Time{}, err
	}

	return accessToken.ID, refreshToken, accessToken.Expiration, nil
}

// TokenRequestByRefreshToken implements the op.Storage interface
// it will be called after parsing and validation of the refresh token request
func (s *tokenStorage) TokenRequestByRefreshToken(ctx context.Context, refreshToken string) (op.RefreshTokenRequest, error) {
	token, err := s.db.GetRefreshToken(ctx, refreshToken)
	if err != nil {
		return nil, op.ErrInvalidRefreshToken
	}

	return RefreshTokenRequestFromBusiness(token), nil
}

// TerminateSession implements the op.Storage interface
// it will be called after the user signed out, therefore the access and refresh token of the user of this client must be removed
func (s *tokenStorage) TerminateSession(ctx context.Context, userID string, clientID string) error {
	return s.db.DeleteTokensByUserAndClient(ctx, userID, clientID)
}

// GetRefreshTokenInfo looks up a refresh token and returns the token id and user id.
// If given something that is not a refresh token, it must return error.
func (s *tokenStorage) GetRefreshTokenInfo(ctx context.Context, clientID string, token string) (userID string, tokenID string, err error) {
	refreshToken, err := s.db.GetRefreshToken(ctx, token)
	if err != nil {
		return "", "", op.ErrInvalidRefreshToken
	}
	return refreshToken.UserID, refreshToken.ID, nil
}

// RevokeToken implements the op.Storage interface
// it will be called after parsing and validation of the token revocation request
func (s *tokenStorage) RevokeToken(ctx context.Context, tokenIDOrToken string, userID string, clientID string) *oidc.Error {
	_ = s.db.DeleteTokensByID(ctx, tokenIDOrToken, clientID)
	return nil
}

// Instrumentation.
var (
	tokensCreated = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "idp_oidc_tokens_created_total",
			Help: "Number of OIDC auth tokens created.",
		},
		[]string{"type", "client"},
	)
)

func init() {
	prometheus.MustRegister(
		tokensCreated,
	)
}
