package oidc

import (
	"errors"
	"log/slog"
	"net/http"
	"os"
	"time"

	"git.autistici.org/smol/idp"
	"git.autistici.org/smol/idp/internal/cryptutil"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/login"
	"github.com/zitadel/oidc/v3/pkg/op"
	"golang.org/x/text/language"
)

const csrfCookieName = "_csrf"

// Config for the IDP service.
type Config struct {
	Issuer        string                         `yaml:"issuer"`
	EncryptionKey cryptutil.Binary               `yaml:"encryption_key"`
	CSRFKey       cryptutil.SessionEncryptionKey `yaml:"csrf_key" doc:"encryption key for CSRF cookies (disable CSRF if not set)"`
}

func (c *Config) getKey() ([32]byte, error) {
	// The OpenID Provider requires a 32-byte key for (token) encryption.
	var key [32]byte
	if len(c.EncryptionKey) != 32 {
		return key, errors.New("invalid encryption_key")
	}
	copy(key[:], c.EncryptionKey)
	return key, nil
}

// Storage interface required by the IDP service, a combination of the
// OIDC op.Storage and the finalization / deviceAuth APIs from
// login.Storage.
type Storage interface {
	op.Storage
	DeviceAuthStorage
	Finalizer
}

// NewApp creates the OIDC OP along with the login application. We
// also return the LoginApp.WithAuth() middleware func in case we want
// to integrate other apps too.
func NewApp(config *Config, features *idp.Features, storage Storage, loginApp *login.LoginApp, template *web.Template) (http.Handler, error) {
	key, err := config.getKey()
	if err != nil {
		return nil, err
	}

	// Create the OpenIDProvider with the given Storage.
	provider, err := NewOP(storage, config.Issuer, key, features.Insecure)
	if err != nil {
		return nil, err
	}

	// Create the final app.
	mux := http.NewServeMux()

	// Register the OIDC callback function.
	mux.Handle("/auth/callback", loginApp.WithAuth(
		newOIDCCallbackHandler(storage, op.AuthCallbackURL(provider))))

	// Register the device access UI.
	mux.Handle("/auth/device", loginApp.WithAuth(web.CSRF(
		config.CSRFKey,
		csrfCookieName,
		"/auth/device",
	)(newDeviceAuthHandler(storage, template))))

	// Register the login application.
	mux.Handle("/auth/", web.CSRF(
		config.CSRFKey,
		csrfCookieName,
		"/auth/",
	)(loginApp))

	// We register the http handler of the OP on the root, so that
	// the discovery endpoint (/.well-known/openid-configuration)
	// is served on the correct path.
	mux.Handle("/", provider)

	return mux, nil
}

// newOP will create an OpenID Provider for localhost on a specified port with a given encryption key
// and a predefined default logout uri
// it will enable all options (see descriptions)
func NewOP(storage op.Storage, issuer string, key [32]byte, insecure bool) (op.OpenIDProvider, error) {
	config := &op.Config{
		CryptoKey: key,

		// will be used if the end_session endpoint is called without a post_logout_redirect_uri
		DefaultLogoutRedirectURI: "/auth/logout",

		// enables code_challenge_method S256 for PKCE (and therefore PKCE in general)
		CodeMethodS256: true,

		// enables additional client_id/client_secret authentication by form post (not only HTTP Basic Auth)
		AuthMethodPost: true,

		// enables additional authentication by using private_key_jwt
		AuthMethodPrivateKeyJWT: true,

		// enables refresh_token grant use
		GrantTypeRefreshToken: true,

		// enables use of the `request` Object parameter
		RequestObjectSupported: true,

		// this example has only static texts (in English), so we'll set the here accordingly
		SupportedUILocales: []language.Tag{language.English},

		DeviceAuthorization: op.DeviceAuthorizationConfig{
			Lifetime:     10 * time.Minute,
			PollInterval: 5 * time.Second,
			UserFormPath: "/auth/device",
			UserCode:     op.UserCodeBase20,
		},
	}

	logger := slog.New(
		slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			Level: slog.LevelDebug,
		}),
	)

	opts := []op.Option{
		// Pass our logger to the OP
		op.WithLogger(logger.WithGroup("op")),
	}

	if insecure {
		// We must explicitly allow the use of the http issuer.
		opts = append(opts, op.WithAllowInsecure())
	}

	return op.NewProvider(
		config,
		storage,
		op.StaticIssuer(issuer),
		opts...,
	)
}
