package oidc

import (
	"context"
	"errors"
	"net/http"

	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/login"
	"github.com/zitadel/oidc/v3/pkg/op"
)

// DeviceAuthStorage are the Storage methods used by device authorization.
type DeviceAuthStorage interface {
	GetDeviceAuthorizationByUserCode(context.Context, string) (*op.DeviceAuthorizationState, error)
	CompleteDeviceAuthorization(context.Context, string, string) error
	DenyDeviceAuthorization(context.Context, string) error
}

// deviceAuthHandler implements the UI for the OIDC device authorization workflow.
type deviceAuthHandler struct {
	storage  DeviceAuthStorage
	template *web.Template
}

func newDeviceAuthHandler(storage DeviceAuthStorage, template *web.Template) http.Handler {
	return &deviceAuthHandler{
		storage:  storage,
		template: template,
	}
}

func (d *deviceAuthHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	var errmsg string

	auth, _ := login.GetAuth(req.Context())

	code := req.FormValue("user_code")

	// This is a two-step input form (enter the code + confirm
	// access), implemented with fall-through on error.
	if code != "" {
		var err error

		action := req.FormValue("action")
		if action != "" && req.Method == http.MethodPost {
			switch action {
			case "allow":
				err = d.storage.CompleteDeviceAuthorization(req.Context(), code, auth.UserId)
			case "deny":
				err = d.storage.DenyDeviceAuthorization(req.Context(), code)
			default:
				err = errors.New("unknown action")
			}

			if err == nil {
				d.template.WithData(map[string]any{
					"Action": action,
				}).Execute(w, req, "device_confirm_result.html")
				return
			}

			errmsg = err.Error()
		}

		state, err := d.storage.GetDeviceAuthorizationByUserCode(req.Context(), code)
		if err == nil {
			d.template.WithData(map[string]any{
				"Username": auth.Username,
				"ClientID": state.ClientID,
				"Scopes":   state.Scopes,
				"Code":     code,
				"Error":    errmsg,
			}).Execute(w, req, "device_confirm.html")
			return
		}

		errmsg = "Unknown authorization code"
	}

	d.template.WithData(map[string]any{
		"Username": auth.Username,
		"Code":     code,
		"Error":    errmsg,
	}).Execute(w, req, "device_code.html")
}
