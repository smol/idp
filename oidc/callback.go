package oidc

import (
	"context"
	"log"
	"net/http"

	"git.autistici.org/smol/idp/login"
)

type callbackFunc func(context.Context, string) string

// Finalizer is the Storage interface used by the OIDC AuthRequest handler.
type Finalizer interface {
	FinalizeAuthRequest(context.Context, string, string) error
}

// oidcCallbackHandler implements the OIDC AuthRequest callback interface.
type oidcCallbackHandler struct {
	finalizer Finalizer
	callback  callbackFunc
}

func newOIDCCallbackHandler(finalizer Finalizer, callback callbackFunc) http.Handler {
	return &oidcCallbackHandler{
		finalizer: finalizer,
		callback:  callback,
	}
}

// OIDC request finalizer (callback) handler.
func (h *oidcCallbackHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	authRequestID := req.FormValue("authRequestId")
	if authRequestID == "" {
		http.Error(w, "Missing authRequestId", http.StatusBadRequest)
		return
	}

	auth, _ := login.GetAuth(req.Context())

	if err := h.finalizer.FinalizeAuthRequest(req.Context(), authRequestID, auth.UserId); err != nil {
		log.Printf("FinalizeAuthRequest: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Redirect to the OP callback.
	uri := h.callback(req.Context(), authRequestID)
	http.Redirect(w, req, uri, http.StatusFound)
}
