package account

import (
	"context"
	"errors"
	"log"

	accpb "git.autistici.org/smol/idp/account/proto"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Server struct {
	db userdb.Backend

	accpb.UnimplementedAccountServer
}

func New(db userdb.Backend) *Server {
	return &Server{db: db}
}

func (s *Server) GetUser(ctx context.Context, req *accpb.GetUserRequest) (*pb.User, error) {
	var user *userdb.User
	var err error

	switch {
	case req.UserId != "":
		user, err = s.db.GetUserByID(ctx, req.UserId)
	case req.Username != "":
		user, err = s.db.GetUserByName(ctx, req.Username)
	default:
		return nil, status.Error(
			codes.InvalidArgument,
			"empty request")
	}

	if err != nil {
		return nil, status.Error(
			codes.NotFound,
			err.Error())
	}

	defer user.Rollback() //nolint:errcheck

	log.Printf("GetUser(%+v) -> %+v", req, user)

	return user.User, nil
}

func (s *Server) GetVerificationToken(ctx context.Context, req *accpb.GetVerificationTokenRequest) (*pb.VerificationToken, error) {
	user, err := s.db.GetUserByID(ctx, req.UserId)
	if err != nil {
		return nil, status.Error(
			codes.NotFound,
			err.Error())
	}

	defer user.Rollback() //nolint:errcheck

	return user.GetVerificationToken(req.TokenType, req.Token)
}

func (s *Server) Exec(ctx context.Context, req *accpb.ExecRequest) (*emptypb.Empty, error) {
	user, err := s.db.GetUserByID(ctx, req.UserId)
	if err != nil {
		return nil, status.Error(
			codes.NotFound,
			err.Error())
	}
	defer user.Rollback() //nolint:errcheck

	for _, op := range req.Operations {
		var err error
		switch op := op.Op.(type) {
		case *accpb.Op_SetEncryptedPrimaryPassword:
			err = user.SetEncryptedPrimaryPassword(op.SetEncryptedPrimaryPassword.Password)
		case *accpb.Op_SetTotpSecret:
			err = user.SetTOTPSecret(op.SetTotpSecret.TotpSecret)
		case *accpb.Op_DeleteTotpSecret:
			err = user.DeleteTOTPSecret()
		case *accpb.Op_AddEncryptedRecoveryToken:
			err = user.AddEncryptedRecoveryToken(op.AddEncryptedRecoveryToken.Token)
		case *accpb.Op_DeleteRecoveryToken:
			err = user.DeleteRecoveryToken(op.DeleteRecoveryToken.Token)
		case *accpb.Op_AddEncryptedAppSpecificPassword:
			err = user.AddEncryptedAppSpecificPassword(op.AddEncryptedAppSpecificPassword.Asp)
		case *accpb.Op_UpdateAppSpecificPassword:
			err = user.UpdateAppSpecificPassword(op.UpdateAppSpecificPassword.Asp)
		case *accpb.Op_DeleteAppSpecificPassword:
			err = user.DeleteAppSpecificPassword(op.DeleteAppSpecificPassword.Asp)
		case *accpb.Op_AddWebauthnRegistration:
			err = user.AddWebAuthnRegistration(op.AddWebauthnRegistration.Registration)
		case *accpb.Op_UpdateWebauthnRegistration:
			err = user.UpdateWebAuthnRegistration(op.UpdateWebauthnRegistration.Registration)
		case *accpb.Op_DeleteWebauthnRegistration:
			err = user.DeleteWebAuthnRegistration(op.DeleteWebauthnRegistration.Registration)
		case *accpb.Op_SetUserInfo:
			err = user.SetUserInfo(op.SetUserInfo.UserInfo)
		case *accpb.Op_AddVerificationToken:
			err = user.AddVerificationToken(op.AddVerificationToken.Token)
		case *accpb.Op_DeleteVerificationToken:
			err = user.DeleteVerificationToken(op.DeleteVerificationToken.Token)

		default:
			err = errors.New("unknown op")
		}

		if err != nil {
			return nil, status.Error(
				codes.Aborted,
				err.Error())
		}
	}

	if err := user.Commit(); err != nil {
		return nil, status.Error(
			codes.FailedPrecondition,
			err.Error())
	}

	return new(emptypb.Empty), nil
}
