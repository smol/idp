package userdb

import (
	"context"
	"log"
	"time"

	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	pb "git.autistici.org/smol/idp/proto"
)

type txFunc func(context.Context, *User) Tx

// Create a Backend that will just wrap its transaction objects with a
// custom function.
type txWrapper struct {
	Backend
	fn txFunc
}

func newTxWrapper(b Backend, fn txFunc) Backend {
	return &txWrapper{
		Backend: b,
		fn:      fn,
	}
}

func (b *txWrapper) wrap(ctx context.Context, u *User) (*User, error) {
	// Create a new user wrapping the original transaction.
	return &User{
		Tx:   b.fn(ctx, u),
		User: u.User,
	}, nil
}

func (b *txWrapper) GetUserByName(ctx context.Context, name string) (*User, error) {
	u, err := b.Backend.GetUserByName(ctx, name)
	if err != nil {
		return nil, err
	}
	return b.wrap(ctx, u)
}

func (b *txWrapper) GetUserByID(ctx context.Context, id string) (*User, error) {
	u, err := b.Backend.GetUserByID(ctx, id)
	if err != nil {
		return nil, err
	}
	return b.wrap(ctx, u)
}

type AuthLogContextFunc func(context.Context) (*authpb.Log, error)

// WithAudit wraps a Backend with audit logging of all changes to user data.
func WithAudit(b Backend, client apb.AuditClient, ctxFn AuthLogContextFunc) Backend {
	return newTxWrapper(b, func(ctx context.Context, u *User) Tx {
		authLog, err := ctxFn(ctx)
		if err != nil {
			log.Printf("warning: can't get authLog from context: %v", err)
		}
		return &acctTx{
			Tx:      u.Tx,
			userID:  u.User.Id,
			authLog: authLog,
			client:  client,
		}
	})
}

type acctTx struct {
	Tx

	userID  string
	authLog *authpb.Log
	client  apb.AuditClient
}

var ingestRPCTimeout = 2 * time.Second

func (tx *acctTx) emitLog(l *apb.Log) {
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), ingestRPCTimeout)
		defer cancel()
		if _, err := tx.client.Ingest(ctx, l); err != nil {
			log.Printf("audit.Ingest error: %v", err)
		}
	}()
}

func (tx *acctTx) SetEncryptedPrimaryPassword(password string) error {
	if err := tx.Tx.SetEncryptedPrimaryPassword(password); err != nil {
		return err
	}
	if tx.authLog != nil {
		tx.emitLog(apb.NewPasswordChangeLog(tx.userID, tx.authLog))
	}
	return nil
}

func (tx *acctTx) AddEncryptedAppSpecificPassword(asp *pb.AppSpecificPassword) error {
	if err := tx.Tx.AddEncryptedAppSpecificPassword(asp); err != nil {
		return err
	}
	if tx.authLog != nil {
		tx.emitLog(apb.NewAddAppSpecificPasswordLog(tx.userID, asp.Id, tx.authLog))
	}
	return nil
}

func (tx *acctTx) DeleteAppSpecificPassword(asp *pb.AppSpecificPassword) error {
	if err := tx.Tx.DeleteAppSpecificPassword(asp); err != nil {
		return err
	}
	if tx.authLog != nil {
		tx.emitLog(apb.NewDeleteAppSpecificPasswordLog(tx.userID, asp.Id, tx.authLog))
	}
	return nil
}

func (tx *acctTx) AddWebAuthnRegistration(reg *pb.WebAuthnRegistration) error {
	if err := tx.Tx.AddWebAuthnRegistration(reg); err != nil {
		return err
	}
	if tx.authLog != nil {
		tx.emitLog(apb.NewAddWebAuthnLog(tx.userID, reg.KeyHandle, tx.authLog))
	}
	return nil
}

func (tx *acctTx) DeleteWebAuthnRegistration(reg *pb.WebAuthnRegistration) error {
	if err := tx.Tx.DeleteWebAuthnRegistration(reg); err != nil {
		return err
	}
	if tx.authLog != nil {
		tx.emitLog(apb.NewDeleteWebAuthnLog(tx.userID, reg.KeyHandle, tx.authLog))
	}
	return nil
}

func (tx *acctTx) SetTOTPSecret(totp string) error {
	if err := tx.Tx.SetTOTPSecret(totp); err != nil {
		return err
	}
	if tx.authLog != nil {
		tx.emitLog(apb.NewSetOTPLog(tx.userID, tx.authLog))
	}
	return nil
}

func (tx *acctTx) DeleteTOTPSecret() error {
	if err := tx.Tx.DeleteTOTPSecret(); err != nil {
		return err
	}
	if tx.authLog != nil {
		tx.emitLog(apb.NewDeleteOTPLog(tx.userID, tx.authLog))
	}
	return nil
}
