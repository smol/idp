package userdb

import "testing"

func TestPasswordHasher(t *testing.T) {
	enc, err := PasswordHasher.Hash("password")
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%s", enc)

	updated, err := PasswordHasher.Verify(enc, "password")
	if err != nil {
		t.Fatalf("Verify(): %v", err)
	}
	if updated != "" {
		t.Fatalf("Verify() returned non-empty update '%s'", updated)
	}
}
