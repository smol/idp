package userdb

import (
	pb "git.autistici.org/smol/idp/proto"
)

// ReadOnlyTx is an implementation of Tx that always returns ErrReadOnly.
type ReadOnlyTx struct{}

func (*ReadOnlyTx) Commit() error   { return nil }
func (*ReadOnlyTx) Rollback() error { return nil }

func (*ReadOnlyTx) SetEncryptedPrimaryPassword(string) error { return ErrReadOnly }
func (*ReadOnlyTx) SetTOTPSecret(string) error               { return ErrReadOnly }
func (*ReadOnlyTx) DeleteTOTPSecret() error                  { return ErrReadOnly }
func (*ReadOnlyTx) AddEncryptedAppSpecificPassword(_ *pb.AppSpecificPassword) error {
	return ErrReadOnly
}
func (*ReadOnlyTx) UpdateAppSpecificPassword(_ *pb.AppSpecificPassword) error   { return ErrReadOnly }
func (*ReadOnlyTx) DeleteAppSpecificPassword(_ *pb.AppSpecificPassword) error   { return ErrReadOnly }
func (*ReadOnlyTx) AddWebAuthnRegistration(_ *pb.WebAuthnRegistration) error    { return ErrReadOnly }
func (*ReadOnlyTx) UpdateWebAuthnRegistration(_ *pb.WebAuthnRegistration) error { return ErrReadOnly }
func (*ReadOnlyTx) DeleteWebAuthnRegistration(_ *pb.WebAuthnRegistration) error { return ErrReadOnly }
func (*ReadOnlyTx) SetUserInfo(_ *pb.UserInfo) error                            { return ErrReadOnly }
func (*ReadOnlyTx) AddVerificationToken(_ *pb.VerificationToken) error          { return ErrReadOnly }
func (*ReadOnlyTx) AddEncryptedRecoveryToken(_ *pb.RecoveryToken) error         { return ErrReadOnly }
func (*ReadOnlyTx) DeleteRecoveryToken(_ *pb.RecoveryToken) error               { return ErrReadOnly }
func (*ReadOnlyTx) DeleteVerificationToken(_ *pb.VerificationToken) error       { return ErrReadOnly }

func (*ReadOnlyTx) GetVerificationToken(_ string, _ string) (*pb.VerificationToken, error) {
	return nil, ErrNotFound
}
