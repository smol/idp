package userdb

import (
	"context"
	"errors"
	"path/filepath"
	"time"

	"git.autistici.org/smol/idp/internal/random"
	pb "git.autistici.org/smol/idp/proto"
	"github.com/zitadel/passwap"
	"github.com/zitadel/passwap/argon2"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gopkg.in/yaml.v3"
)

var (
	// ErrNotFound is returned by GetUser when a user is not in the db.
	ErrNotFound = errors.New("not found")

	// ErrReadOnly implies that the backend can't be modified.
	ErrReadOnly = errors.New("read-only backend")
)

// Default password hasher.
var PasswordHasher = passwap.NewSwapper(
	argon2.NewArgon2id(argon2.RecommendedIDParams),
)

// Spec configures a userdb backend. Parameters are type-specific,
// their parsing deferred to the backend constructors.
type Spec struct {
	Type   string    `yaml:"type" doc:"backend type"`
	Params yaml.Node `yaml:"params" doc:"backend-specific configuration"`
}

// Registry of known backend types.
type backendFactoryFunc func(*yaml.Node, string) (Backend, error)

var backendRegistry = make(map[string]backendFactoryFunc)

func Register(name string, f func(*yaml.Node, string) (Backend, error)) {
	backendRegistry[name] = f
}

func getBackendFactory(name string) (backendFactoryFunc, bool) {
	bf, ok := backendRegistry[name]
	return bf, ok
}

func New(spec *Spec, configPath string) (Backend, error) {
	bf, ok := getBackendFactory(spec.Type)
	if !ok {
		return nil, errors.New("unknown backend")
	}

	return bf(&spec.Params, filepath.Dir(configPath))
}

// Backend provides access to the user database with a transactional
// API. The transaction is always scoped to a specific user, so it is
// "hidden" in the User object.
type Backend interface {
	Close()

	GetUserByID(context.Context, string) (*User, error)
	GetUserByName(context.Context, string) (*User, error)
}

// Tx is the transactional write API to the user database. Note
// that after calling these methods, the outer User object will not be
// updated to reflect the changes. The Tx object embeds the user identity.
//
// This interface only accepts encrypted credentials (the various
// AddEncrypted* methods). The User type wraps this with an API that
// accepts cleartext secrets and encrypts them, with matching methods
// lacking the 'Encrypted' part.
//
// FIXME: this is a Context-wrapping API (mostly due to the semantics
// of the underlying database/sql.Tx) and this is bad.
type Tx interface {
	Commit() error
	Rollback() error

	SetEncryptedPrimaryPassword(string) error

	SetTOTPSecret(string) error
	DeleteTOTPSecret() error

	AddEncryptedRecoveryToken(*pb.RecoveryToken) error
	DeleteRecoveryToken(*pb.RecoveryToken) error

	AddEncryptedAppSpecificPassword(*pb.AppSpecificPassword) error
	UpdateAppSpecificPassword(*pb.AppSpecificPassword) error
	DeleteAppSpecificPassword(*pb.AppSpecificPassword) error

	AddWebAuthnRegistration(*pb.WebAuthnRegistration) error
	UpdateWebAuthnRegistration(*pb.WebAuthnRegistration) error
	DeleteWebAuthnRegistration(*pb.WebAuthnRegistration) error

	SetUserInfo(*pb.UserInfo) error

	GetVerificationToken(string, string) (*pb.VerificationToken, error)
	AddVerificationToken(*pb.VerificationToken) error
	DeleteVerificationToken(*pb.VerificationToken) error
}

// User object. Fields are conceptually read-only: manipulation is
// delegated to the underlying Tx interface. There are specific
// methods for credential manipulation, to ensure that the user
// invariants are preserved: like the ability to decrypt the user
// encryption key when changing passwords, etc.
type User struct {
	*pb.User

	Tx
}

func (u *User) DisableSecondFactorAuth() error {
	for _, reg := range u.WebAuthnRegistrations {
		if err := u.Tx.DeleteWebAuthnRegistration(reg); err != nil {
			return err
		}
	}

	for _, asp := range u.AppSpecificPasswords {
		if err := u.Tx.DeleteAppSpecificPassword(asp); err != nil {
			return err
		}
	}

	if u.TotpSecret != "" {
		if err := u.Tx.DeleteTOTPSecret(); err != nil {
			return err
		}
	}

	return nil
}

// UnlockCredentials are used whenever we need to encrypt new
// credentials, in order for the API to eventually support
// password-derived encryption keys with transparent re-keying.
type UnlockCredentials struct {
	AuthID   *pb.AuthenticatorID
	Password string
}

func (u *User) SetPrimaryPassword(unlock *UnlockCredentials, password string) error {
	// This function can only be called with primary auth method.
	if unlock.AuthID.Type != pb.AuthenticatorID_PRIMARY_PASSWORD {
		return errors.New("can only unlock using primary password")
	}

	enc, err := PasswordHasher.Hash(password)
	if err != nil {
		return err
	}
	u.EncryptedPassword = enc

	return u.Tx.SetEncryptedPrimaryPassword(u.EncryptedPassword)
}

// AddRecoveryToken overrides the low-level method to manage encryption keys.
func (u *User) AddRecoveryToken(unlock *UnlockCredentials, tokenType pb.RecoveryToken_Type, id, token string, expiry time.Time) error {
	enc, err := PasswordHasher.Hash(token)
	if err != nil {
		return err
	}

	rtok := &pb.RecoveryToken{
		Id:             id,
		Type:           tokenType,
		EncryptedToken: enc,
	}
	if !expiry.IsZero() {
		rtok.ExpiresAt = timestamppb.New(expiry)
	}

	if err := u.Tx.AddEncryptedRecoveryToken(rtok); err != nil {
		return err
	}

	return nil
}

// DeleteRecoveryToken overrides the low-level method to manage encryption keys.
// func (u *User) DeleteRecoveryToken(rtok *pb.RecoveryToken) error {
// 	if err := u.Tx.DeleteRecoveryToken(rtok); err != nil {
// 		return err
// 	}
// 	return nil
// }

// AddAppSpecificPassword overrides the low-level method to manage encryption keys.
func (u *User) AddAppSpecificPassword(unlock *UnlockCredentials, password, service, name string) error {
	enc, err := PasswordHasher.Hash(password)
	if err != nil {
		return err
	}

	asp := &pb.AppSpecificPassword{
		Id:                random.Hex(12),
		Service:           service,
		EncryptedPassword: enc,
		Name:              name,
	}

	if err := u.Tx.AddEncryptedAppSpecificPassword(asp); err != nil {
		return err
	}

	// TODO: not do this? No need to maintain state in User.
	u.AppSpecificPasswords = append(u.AppSpecificPasswords, asp)
	return nil
}

// DeleteAppSpecificPassword overrides the low-level method to manage encryption keys.
// func (u *User) DeleteAppSpecificPassword(asp *pb.AppSpecificPassword) error {
// 	if err := u.Tx.DeleteAppSpecificPassword(asp); err != nil {
// 		return err
// 	}
// 	return nil
// }
