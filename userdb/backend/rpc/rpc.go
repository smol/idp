package rpc

import (
	"context"

	accpb "git.autistici.org/smol/idp/account/proto"
	"git.autistici.org/smol/idp/internal/netutil"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gopkg.in/yaml.v3"
)

type rpcParams struct {
	Endpoint netutil.GRPCEndpointSpec `yaml:"endpoint"`
}

type rpcBackend struct {
	conn   *grpc.ClientConn
	client accpb.AccountClient
}

func newRPCBackend(params *yaml.Node, _ string) (userdb.Backend, error) {
	var rparams rpcParams

	if err := params.Decode(&rparams); err != nil {
		return nil, err
	}

	conn, err := rparams.Endpoint.Dial()
	if err != nil {
		return nil, err
	}

	return &rpcBackend{
		conn:   conn,
		client: accpb.NewAccountClient(conn),
	}, nil
}

func init() {
	userdb.Register("rpc", newRPCBackend)
}

func (b *rpcBackend) Close() {
	b.conn.Close()
}

func (b *rpcBackend) GetUserByID(ctx context.Context, userID string) (*userdb.User, error) {
	return b.getUser(ctx, &accpb.GetUserRequest{UserId: userID})
}

func (b *rpcBackend) GetUserByName(ctx context.Context, username string) (*userdb.User, error) {
	return b.getUser(ctx, &accpb.GetUserRequest{Username: username})
}

func (b *rpcBackend) getUser(ctx context.Context, req *accpb.GetUserRequest) (*userdb.User, error) {
	u, err := b.client.GetUser(ctx, req)
	if err != nil {
		if status.Code(err) == codes.NotFound {
			err = userdb.ErrNotFound
		}
		return nil, err
	}

	tx := &rpcTx{
		userID: u.Id,
		ctx:    ctx,
		client: b.client,
	}
	return &userdb.User{
		Tx:   tx,
		User: u,
	}, nil
}

type rpcTx struct {
	userID  string
	changes []*accpb.Op

	ctx    context.Context
	client accpb.AccountClient
}

func (t *rpcTx) Commit() error {
	if len(t.changes) == 0 {
		return nil
	}

	_, err := t.client.Exec(t.ctx, &accpb.ExecRequest{
		UserId:     t.userID,
		Operations: t.changes,
	})
	return err
}

func (t *rpcTx) Rollback() error { return nil }

func (t *rpcTx) SetEncryptedPrimaryPassword(password string) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_SetEncryptedPrimaryPassword{
			SetEncryptedPrimaryPassword: &accpb.OpSetEncryptedPrimaryPassword{
				Password: password,
			},
		},
	})
	return nil
}

func (t *rpcTx) SetTOTPSecret(secret string) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_SetTotpSecret{
			SetTotpSecret: &accpb.OpSetTOTPSecret{
				TotpSecret: secret,
			},
		},
	})
	return nil
}

func (t *rpcTx) DeleteTOTPSecret() error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_DeleteTotpSecret{
			DeleteTotpSecret: &accpb.OpDeleteTOTPSecret{},
		},
	})
	return nil
}

func (t *rpcTx) AddEncryptedRecoveryToken(token *pb.RecoveryToken) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_AddEncryptedRecoveryToken{
			AddEncryptedRecoveryToken: &accpb.OpAddEncryptedRecoveryToken{
				Token: token,
			},
		},
	})
	return nil
}

func (t *rpcTx) DeleteRecoveryToken(token *pb.RecoveryToken) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_DeleteRecoveryToken{
			DeleteRecoveryToken: &accpb.OpDeleteRecoveryToken{
				Token: token,
			},
		},
	})
	return nil
}

func (t *rpcTx) AddEncryptedAppSpecificPassword(asp *pb.AppSpecificPassword) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_AddEncryptedAppSpecificPassword{
			AddEncryptedAppSpecificPassword: &accpb.OpAddEncryptedAppSpecificPassword{
				Asp: asp,
			},
		},
	})
	return nil
}

func (t *rpcTx) UpdateAppSpecificPassword(asp *pb.AppSpecificPassword) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_UpdateAppSpecificPassword{
			UpdateAppSpecificPassword: &accpb.OpUpdateAppSpecificPassword{
				Asp: asp,
			},
		},
	})
	return nil
}

func (t *rpcTx) DeleteAppSpecificPassword(asp *pb.AppSpecificPassword) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_DeleteAppSpecificPassword{
			DeleteAppSpecificPassword: &accpb.OpDeleteAppSpecificPassword{
				Asp: asp,
			},
		},
	})
	return nil
}

func (t *rpcTx) AddWebAuthnRegistration(reg *pb.WebAuthnRegistration) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_AddWebauthnRegistration{
			AddWebauthnRegistration: &accpb.OpAddWebAuthnRegistration{
				Registration: reg,
			},
		},
	})
	return nil
}

func (t *rpcTx) UpdateWebAuthnRegistration(reg *pb.WebAuthnRegistration) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_UpdateWebauthnRegistration{
			UpdateWebauthnRegistration: &accpb.OpUpdateWebAuthnRegistration{
				Registration: reg,
			},
		},
	})
	return nil
}

func (t *rpcTx) DeleteWebAuthnRegistration(reg *pb.WebAuthnRegistration) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_DeleteWebauthnRegistration{
			DeleteWebauthnRegistration: &accpb.OpDeleteWebAuthnRegistration{
				Registration: reg,
			},
		},
	})
	return nil
}

func (t *rpcTx) SetUserInfo(info *pb.UserInfo) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_SetUserInfo{
			SetUserInfo: &accpb.OpSetUserInfo{
				UserInfo: info,
			},
		},
	})
	return nil
}

func (t *rpcTx) AddVerificationToken(token *pb.VerificationToken) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_AddVerificationToken{
			AddVerificationToken: &accpb.OpAddVerificationToken{
				Token: token,
			},
		},
	})
	return nil
}

func (t *rpcTx) DeleteVerificationToken(token *pb.VerificationToken) error {
	t.changes = append(t.changes, &accpb.Op{
		Op: &accpb.Op_DeleteVerificationToken{
			DeleteVerificationToken: &accpb.OpDeleteVerificationToken{
				Token: token,
			},
		},
	})
	return nil
}

func (t *rpcTx) GetVerificationToken(tokenType, token string) (*pb.VerificationToken, error) {
	return t.client.GetVerificationToken(t.ctx, &accpb.GetVerificationTokenRequest{
		UserId:    t.userID,
		TokenType: tokenType,
		Token:     token,
	})
}
