package static

import (
	"context"
	"encoding/base64"

	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
	"gopkg.in/yaml.v3"
)

type staticParams struct {
	Users        []*User  `yaml:"users" doc:"list of static users"`
	StaticGroups []string `yaml:"static_groups" doc:"groups that users from this backend will always be members of"`
}

// User type needed to bridge between YAML and the internal User representation.
type User struct {
	Name              string   `yaml:"name"`
	Email             string   `yaml:"email"`
	EncryptedPassword string   `yaml:"password"`
	TOTPSecret        string   `yaml:"totp_secret"`
	Groups            []string `yaml:"groups"`

	// WebAuthN registrations are encoded as emitted by modern
	// versions of pamu2fcfg: both values are base64-encoded
	// (standard, with padding). The key is actually in COSE format.
	WebAuthnRegistrations []struct {
		KeyHandle string `yaml:"key_handle"`
		PublicKey string `yaml:"public_key"`
		Comment   string `yaml:"comment"`
	} `yaml:"webauthn_registrations"`

	AppSpecificPasswords []struct {
		ID                string `yaml:"id"`
		Service           string `yaml:"service"`
		EncryptedPassword string `yaml:"password"`
		Comment           string `yaml:"comment"`
	} `yaml:"app_specific_passwords"`
}

func (f *User) toUser(extraGroups []string) *userdb.User {
	user := &userdb.User{
		Tx: new(userdb.ReadOnlyTx),
		User: &pb.User{
			Id: f.Name,
			Info: &pb.UserInfo{
				Name:   f.Name,
				Email:  f.Email,
				Groups: append(f.Groups, extraGroups...),
			},
			EncryptedPassword: f.EncryptedPassword,
			TotpSecret:        f.TOTPSecret,
			ReadOnly:          true,
		},
	}

	for _, asp := range f.AppSpecificPasswords {
		user.AppSpecificPasswords = append(user.AppSpecificPasswords, &pb.AppSpecificPassword{
			Id:                asp.ID,
			Service:           asp.Service,
			EncryptedPassword: asp.EncryptedPassword,
			Name:              asp.Comment,
		})
	}

	for _, wr := range f.WebAuthnRegistrations {
		kh, err := base64.StdEncoding.DecodeString(wr.KeyHandle)
		if err != nil {
			continue
		}
		pk, err := base64.StdEncoding.DecodeString(wr.PublicKey)
		if err != nil {
			continue
		}
		user.WebAuthnRegistrations = append(user.WebAuthnRegistrations, &pb.WebAuthnRegistration{
			KeyHandle: string(kh),
			PublicKey: string(pk),
			Name:      wr.Comment,
		})
	}

	return user
}

type staticBackend struct {
	users map[string]*userdb.User
}

func (b *staticBackend) Close() {}

func (b *staticBackend) GetUserByName(_ context.Context, name string) (*userdb.User, error) {
	u, ok := b.users[name]
	if !ok {
		return nil, userdb.ErrNotFound
	}
	return u, nil
}

func (b *staticBackend) GetUserByID(ctx context.Context, id string) (*userdb.User, error) {
	return b.GetUserByName(ctx, id)
}

func usersToMap(users []*User, extraGroups []string) map[string]*userdb.User {
	m := make(map[string]*userdb.User)
	for _, u := range users {
		m[u.Name] = u.toUser(extraGroups)
	}
	return m
}

func newStaticBackendFromParams(params *yaml.Node, dir string) (userdb.Backend, error) {
	var fparams staticParams
	if err := params.Decode(&fparams); err != nil {
		return nil, err
	}

	return NewStaticBackend(fparams.Users, fparams.StaticGroups)
}

func NewStaticBackend(users []*User, groups []string) (userdb.Backend, error) {
	return &staticBackend{
		users: usersToMap(users, groups),
	}, nil
}

func init() {
	userdb.Register("static", newStaticBackendFromParams)
}
