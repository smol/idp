package allbackends_test

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"testing"
	"time"

	"git.autistici.org/smol/idp/account"
	accpb "git.autistici.org/smol/idp/account/proto"
	"git.autistici.org/smol/idp/internal/testutil"
	"git.autistici.org/smol/idp/userdb"
	"google.golang.org/grpc"
	"gopkg.in/yaml.v3"
)

var sqlFixtures = `
INSERT INTO users (id, username, email, password) VALUES (1, 'test', 'testuser@example.com', '$argon2id$v=19$m=65536,t=1,p=4$4cRvKr/X9u9NKMev1jYhSg$WqVnGxKaWBPZrLqV2Zj/+A/NeNANlkzocs+86LubNBM');
INSERT INTO users (id, username, email, password, totp_secret) VALUES (2, 'test-otp', 'testuser@example.com', '$argon2id$v=19$m=65536,t=1,p=4$4cRvKr/X9u9NKMev1jYhSg$WqVnGxKaWBPZrLqV2Zj/+A/NeNANlkzocs+86LubNBM', 'RGHD5VPD3NNMTH3VAZHSP36YFWM5RPFS');
INSERT INTO users (id, username, email, password) VALUES (3, 'test-sqlonly', 'testsqlonly@example.com', '$argon2id$v=19$m=65536,t=1,p=4$4cRvKr/X9u9NKMev1jYhSg$WqVnGxKaWBPZrLqV2Zj/+A/NeNANlkzocs+86LubNBM');
`

var staticFixtures = `
users:
  - name: test
    email: testuser@example.com
    password: "$argon2id$v=19$m=65536,t=1,p=4$4cRvKr/X9u9NKMev1jYhSg$WqVnGxKaWBPZrLqV2Zj/+A/NeNANlkzocs+86LubNBM"
  - name: test-otp
    email: testuser@example.com
    password: "$argon2id$v=19$m=65536,t=1,p=4$4cRvKr/X9u9NKMev1jYhSg$WqVnGxKaWBPZrLqV2Zj/+A/NeNANlkzocs+86LubNBM"
    totp_secret: "RGHD5VPD3NNMTH3VAZHSP36YFWM5RPFS"
`

func createTestDbStaticSpec() *userdb.Spec {
	var n yaml.Node
	yaml.Unmarshal([]byte(staticFixtures), &n) //nolint:errcheck
	return &userdb.Spec{
		Type:   "static",
		Params: n,
	}
}

func createTestChainBackend() (userdb.Backend, func(), error) {
	fdb, err := userdb.New(createTestDbStaticSpec(), "")
	if err != nil {
		return nil, nil, fmt.Errorf("static: %w", err)
	}

	sdb, cleanup, err := testutil.CreateTestDB(sqlFixtures, "")
	if err != nil {
		return nil, nil, fmt.Errorf("sql: %w", err)
	}

	b := userdb.NewChainBackend([]userdb.Backend{fdb, sdb})
	return b, cleanup, nil
}

func createTestGRPCBackend(b userdb.Backend) (userdb.Backend, func(), error) {
	authSrv := account.New(b)

	l, err := net.Listen("tcp", ":0")
	if err != nil {
		return nil, nil, err
	}

	addr := l.Addr().String()
	log.Printf("grpc server started on %s", addr)

	server := grpc.NewServer()
	accpb.RegisterAccountServer(server, authSrv)

	go server.Serve(l) //nolint:errcheck

	rpcConfig := fmt.Sprintf(`
endpoint:
  addr: "%s"
`, addr)
	var n yaml.Node
	yaml.Unmarshal([]byte(rpcConfig), &n) //nolint:errcheck
	bb, err := userdb.New(&userdb.Spec{
		Type:   "rpc",
		Params: n,
	}, "")

	return bb, func() {
		server.GracefulStop()
	}, err
}

func setPassword(user *userdb.User, pw string) error {
	if err := user.SetEncryptedPrimaryPassword(pw); err != nil {
		return fmt.Errorf("user.SetEncryptedPrimaryPassword(): %w", err)
	}
	if err := user.Commit(); err != nil {
		return fmt.Errorf("user.Commit(): %w", err)
	}
	return nil
}

func runTestNotFound(t *testing.T, db userdb.Backend) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	username := "nosuchuser"
	user, err := db.GetUserByName(ctx, username)
	if !errors.Is(err, userdb.ErrNotFound) {
		t.Fatalf("GetUserByName(%s): expected ErrNotFound, got err=%v, user=%+v", username, err, user)
	}
}

func runTest(t *testing.T, db userdb.Backend, username string, expectReadonly bool) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	user, err := db.GetUserByName(ctx, username)
	if err != nil {
		t.Fatalf("GetUserByName(%s): %v", username, err)
	}

	if user.Info.Name != username {
		t.Fatalf("User.Info.Name is %s, expected %s", user.Info.Name, username)
	}

	newPw := "new password"
	err = setPassword(user, newPw)
	switch {
	case expectReadonly && err == nil:
		t.Fatalf("user %s: successfully changed password on a read-only backend!", username)
	case !expectReadonly && err != nil:
		t.Fatalf("user %s: failed to set password: %v", username, err)
	}

	if !expectReadonly {
		user, err = db.GetUserByName(ctx, username)
		if err != nil {
			t.Fatalf("GetUserByName(%s) (2nd time): %v", username, err)
		}
		if user.EncryptedPassword != newPw {
			t.Fatalf("user %s: did not set primary password (current value=%s)", username, user.EncryptedPassword)
		}
	}
}

func TestBackend_SQL(t *testing.T) {
	db, cleanup, err := testutil.CreateTestDB(sqlFixtures, "")
	if err != nil {
		t.Fatal(err)
	}
	defer cleanup()

	runTest(t, db, "test", false)
	runTestNotFound(t, db)
}

func TestBackend_Static(t *testing.T) {
	db, err := userdb.New(createTestDbStaticSpec(), "")
	if err != nil {
		t.Fatalf("userdb.New(): %v", err)
	}
	defer db.Close()

	runTest(t, db, "test", true)
	runTestNotFound(t, db)
}

func TestBackend_Chain(t *testing.T) {
	db, cleanup, err := createTestChainBackend()
	if err != nil {
		t.Fatalf("%v", err)
	}
	defer cleanup()

	runTest(t, db, "test", true) // static backend comes first
	runTest(t, db, "test-sqlonly", false)
	runTestNotFound(t, db)
}

func TestBackend_RPC(t *testing.T) {
	sdb, cleanup, err := testutil.CreateTestDB(sqlFixtures, "")
	if err != nil {
		t.Fatalf("sql backend: %v", err)
	}
	defer cleanup()

	db, cleanup2, err := createTestGRPCBackend(sdb)
	if err != nil {
		t.Fatalf("grpc backend: %v", err)
	}
	defer cleanup2()

	runTest(t, db, "test", false)
	runTestNotFound(t, db)
}
