package sqlbackend

import (
	"database/sql"
	"fmt"

	"git.autistici.org/smol/idp/userdb"
)

func setupSQLite(db *sql.DB) error {
	db.SetMaxOpenConns(1)

	if _, err := db.Exec("PRAGMA main.journal = WAL"); err != nil {
		return fmt.Errorf("error setting journal=WAL: %w", err)
	}

	if _, err := db.Exec("PRAGMA main.synchronous = NORMAL"); err != nil {
		return fmt.Errorf("error setting sync=NORMAL: %w", err)
	}

	return nil
}

// NewSQLiteBackendWithQueryMap is a convenience function used to
// programmatically create a SQLite-backed userdb.
func NewSQLiteBackendWithQueryMap(db *sql.DB, queries map[string]string) userdb.Backend {
	// Merge the provided queries with the defaults.
	m := make(map[string]string)
	for k, v := range defaultQueries {
		m[k] = v
	}
	for k, v := range queries {
		m[k] = v
	}

	return &sqlBackend{
		db:       db,
		queryMap: queryMap(m),
	}
}
