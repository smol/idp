package sqlbackend

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gopkg.in/yaml.v3"
)

var fixtures = `
INSERT INTO users (id, username, email, password) VALUES (1, 'admin', 'admin@example.com', 'foo');
INSERT INTO asps (uid, id, service, password, comment) VALUES (1, 'abcdef', 'service1', 'foo', 'comment1');
INSERT INTO asps (uid, id, service, password, comment) VALUES (1, 'zxcvbn', 'service2', 'foo', 'comment2');
`

func mkConfig(driver, dburi string) *yaml.Node {
	s := fmt.Sprintf("driver: \"%s\"\ndburi: \"%s\"\n", driver, dburi)
	var n yaml.Node
	yaml.Unmarshal([]byte(s), &n) //nolint:errcheck
	return &n
}

func execSQL(t *testing.T, db *sqlBackend, sqlstr string) {
	t.Helper()

	for _, stmt := range strings.Split(sqlstr, ";\n") {
		stmt = strings.TrimSpace(stmt)
		if stmt == "" {
			continue
		}
		if _, err := db.db.Exec(stmt); err != nil {
			t.Fatalf("sql error in statement \"%s\": %v", stmt, err)
		}
	}
}

func TestUserDB_SQL(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	b, err := newSQLBackend(mkConfig("sqlite3", filepath.Join(dir, "user.db")), "")
	if err != nil {
		t.Fatal(err)
	}
	defer b.Close()

	execSQL(t, b.(*sqlBackend), DefaultSchema)
	execSQL(t, b.(*sqlBackend), fixtures)

	u, err := b.GetUserByName(context.Background(), "admin")
	if err != nil {
		t.Fatalf("GetUser: %v", err)
	}
	if u.Id != "1" {
		t.Errorf("user.Id is %s, expected 1", u.Id)
	}
	if u.Info.Name != "admin" {
		t.Errorf("user.Name is %s, expected admin", u.Info.Name)
	}
	if n := len(u.AppSpecificPasswords); n != 2 {
		t.Errorf("found %d ASPs, expected 2", n)
	}
}
