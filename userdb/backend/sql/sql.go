package sqlbackend

import (
	"context"
	"database/sql"
	"errors"
	"strconv"
	"time"

	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gopkg.in/yaml.v3"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
	// TODO: add other database/sql clients.
)

var ErrUndefinedQuery = errors.New("query not configured")

type sqlParams struct {
	Driver  string            `yaml:"driver"`
	DBURI   string            `yaml:"dburi"`
	Queries map[string]string `yaml:"queries"`
}

type queryMap map[string]string

func (q queryMap) get(name string) (string, bool) {
	s, ok := q[name]
	if s == "" {
		ok = false
	}
	return s, ok
}

type mappedTx struct {
	*sql.Tx

	queryMap queryMap
}

func (tx *mappedTx) QueryRow(name string, args ...any) *sql.Row {
	query, ok := tx.queryMap.get(name)
	if !ok {
		// Wouldn't it be nice if we could inject an error...
		return new(sql.Row)
	}
	return tx.Tx.QueryRow(query, args...)
}

func (tx *mappedTx) Query(name string, args ...any) (*sql.Rows, error) {
	query, ok := tx.queryMap.get(name)
	if !ok {
		return nil, ErrUndefinedQuery
	}
	return tx.Tx.Query(query, args...)
}

func (tx *mappedTx) Exec(name string, args ...any) (sql.Result, error) {
	query, ok := tx.queryMap.get(name)
	if !ok {
		return nil, ErrUndefinedQuery
	}
	return tx.Tx.Exec(query, args...)
}

type sqlTx struct {
	*mappedTx

	uid int64
}

func (tx *sqlTx) SetEncryptedPrimaryPassword(s string) error {
	_, err := tx.Exec(
		"set_encrypted_primary_password",
		sql.Named("uid", tx.uid),
		sql.Named("password", s),
	)
	return err
}

func (tx *sqlTx) SetTOTPSecret(s string) error {
	_, err := tx.Exec(
		"set_totp_secret",
		sql.Named("uid", tx.uid),
		sql.Named("secret", s),
	)
	return err
}

func (tx *sqlTx) DeleteTOTPSecret() error {
	_, err := tx.Exec(
		"delete_totp_secret",
		sql.Named("uid", tx.uid),
	)
	return err
}

func (tx *sqlTx) AddEncryptedAppSpecificPassword(asp *pb.AppSpecificPassword) error {
	_, err := tx.Exec(
		"add_app_specific_password",
		sql.Named("uid", tx.uid),
		sql.Named("asp_id", asp.Id),
		sql.Named("asp_service", asp.Service),
		sql.Named("asp_password", asp.EncryptedPassword),
		sql.Named("asp_name", asp.Name),
	)
	return err
}

func (tx *sqlTx) UpdateAppSpecificPassword(asp *pb.AppSpecificPassword) error {
	_, err := tx.Exec(
		"update_app_specific_password",
		sql.Named("uid", tx.uid),
		sql.Named("asp_id", asp.Id),
		sql.Named("asp_name", asp.Name),
	)
	return err
}

func (tx *sqlTx) DeleteAppSpecificPassword(asp *pb.AppSpecificPassword) error {
	_, err := tx.Exec(
		"delete_app_specific_password",
		sql.Named("uid", tx.uid),
		sql.Named("asp_id", asp.Id),
	)
	return err
}

func (tx *sqlTx) AddWebAuthnRegistration(w *pb.WebAuthnRegistration) error {
	_, err := tx.Exec(
		"add_webauthn_registration",
		sql.Named("uid", tx.uid),
		sql.Named("registration_key_handle", w.KeyHandle),
		sql.Named("registration_public_key", w.PublicKey),
		sql.Named("registration_name", w.Name),
	)
	return err
}

func (tx *sqlTx) UpdateWebAuthnRegistration(w *pb.WebAuthnRegistration) error {
	_, err := tx.Exec(
		"update_webauthn_registration",
		sql.Named("uid", tx.uid),
		sql.Named("registration_key_handle", w.KeyHandle),
		sql.Named("registration_name", w.Name),
	)
	return err
}

func (tx *sqlTx) DeleteWebAuthnRegistration(w *pb.WebAuthnRegistration) error {
	_, err := tx.Exec(
		"delete_webauthn_registration",
		sql.Named("uid", tx.uid),
		sql.Named("registration_key_handle", w.KeyHandle),
	)
	return err
}

func (tx *sqlTx) SetUserInfo(info *pb.UserInfo) error {
	_, err := tx.Exec(
		"set_user_info",
		sql.Named("uid", tx.uid),
		sql.Named("name", info.Name),
		sql.Named("email", info.Email),
		sql.Named("locale", info.Locale),
		sql.Named("timezone", info.Timezone),
	)
	return err
}

func getUser(tx *mappedTx, queryMap queryMap, queryName, argName, arg string) (int64, *userdb.User, error) {
	user := userdb.User{
		User: &pb.User{
			Info: new(pb.UserInfo),
		},
	}
	var totpSecret sql.NullString

	// Main query.
	var uid int64
	if err := tx.QueryRow(
		queryName,
		sql.Named(argName, arg),
	).Scan(
		&uid,
		&user.Info.Name,
		&user.Info.Email,
		&user.Info.Locale,
		&user.Info.Timezone,
		&user.EncryptedPassword,
		&totpSecret,
		&user.ReadOnly,
	); err != nil {
		return 0, nil, err
	}
	user.Id = strconv.FormatInt(uid, 10)
	if totpSecret.Valid {
		user.TotpSecret = totpSecret.String
	}

	// Sub-queries.
	for _, f := range []augmentUserFunc{
		getUserGroups,
		getUserASPs,
		getUserWebAuthnRegistrations,
		getUserRecoveryTokens,
	} {
		if err := f(tx, queryMap, uid, &user); err != nil {
			return 0, nil, err
		}
	}

	return uid, &user, nil
}

func multiRowQuery(tx *mappedTx, query string, uid int64, f func(*sql.Rows) error) error {
	rows, err := tx.Query(query, sql.Named("uid", uid))
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		if err := f(rows); err != nil {
			return err
		}
	}

	return rows.Err()
}

type augmentUserFunc func(*mappedTx, queryMap, int64, *userdb.User) error

func getUserGroups(tx *mappedTx, queryMap queryMap, uid int64, user *userdb.User) error {
	return multiRowQuery(tx, "get_user_groups", uid, func(rows *sql.Rows) error {
		var groupName string
		if err := rows.Scan(&groupName); err != nil {
			return err
		}
		user.Info.Groups = append(user.Info.Groups, groupName)
		return nil
	})
}

func getUserRecoveryTokens(tx *mappedTx, queryMap queryMap, uid int64, user *userdb.User) error {
	return multiRowQuery(tx, "get_recovery_tokens", uid, func(rows *sql.Rows) error {
		var rtok pb.RecoveryToken
		if err := rows.Scan(&rtok.Id, &rtok.EncryptedToken); err != nil {
			return err
		}
		user.RecoveryTokens = append(user.RecoveryTokens, &rtok)
		return nil
	})
}

func getUserASPs(tx *mappedTx, queryMap queryMap, uid int64, user *userdb.User) error {
	return multiRowQuery(tx, "get_user_asps", uid, func(rows *sql.Rows) error {
		var asp pb.AppSpecificPassword
		var name sql.NullString
		if err := rows.Scan(&asp.Id, &asp.Service, &asp.EncryptedPassword, &name); err != nil {
			return err
		}
		if name.Valid {
			asp.Name = name.String
		}
		user.AppSpecificPasswords = append(user.AppSpecificPasswords, &asp)
		return nil
	})
}

func getUserWebAuthnRegistrations(tx *mappedTx, queryMap queryMap, uid int64, user *userdb.User) error {
	return multiRowQuery(tx, "get_user_webauthn_registrations", uid, func(rows *sql.Rows) error {
		var reg pb.WebAuthnRegistration
		var name sql.NullString
		if err := rows.Scan(&reg.KeyHandle, &reg.PublicKey, &name); err != nil {
			return err
		}
		if name.Valid {
			reg.Name = name.String
		}
		user.WebAuthnRegistrations = append(user.WebAuthnRegistrations, &reg)
		return nil
	})
}

func (tx *sqlTx) AddEncryptedRecoveryToken(rtok *pb.RecoveryToken) error {
	_, err := tx.Exec(
		"add_recovery_token",
		sql.Named("uid", tx.uid),
		sql.Named("id", rtok.Id),
		sql.Named("token", rtok.EncryptedToken),
	)
	return err
}

func (tx *sqlTx) DeleteRecoveryToken(rtok *pb.RecoveryToken) error {
	_, err := tx.Exec(
		"delete_recovery_token",
		sql.Named("uid", tx.uid),
		sql.Named("id", rtok.Id),
	)
	return err
}

func (tx *sqlTx) AddVerificationToken(v *pb.VerificationToken) error {
	_, err := tx.Exec(
		"add_verification_token",
		sql.Named("uid", tx.uid),
		sql.Named("type", v.Type),
		sql.Named("token", v.Token),
		sql.Named("expires_at", v.ExpiresAt.AsTime()),
		sql.Named("arg", v.Arg),
	)
	return err
}

func (tx *sqlTx) GetVerificationToken(typ, token string) (*pb.VerificationToken, error) {
	var v pb.VerificationToken
	var expiresAt time.Time

	if err := tx.QueryRow(
		"get_verification_token",
		sql.Named("uid", tx.uid),
		sql.Named("type", typ),
		sql.Named("token", token),
	).Scan(
		&v.Type,
		&v.Token,
		&expiresAt,
		&v.Arg,
	); err != nil {
		return nil, err
	}

	v.ExpiresAt = timestamppb.New(expiresAt)

	return &v, nil
}

func (tx *sqlTx) DeleteVerificationToken(v *pb.VerificationToken) error {
	_, err := tx.Exec(
		"delete_verification_token",
		sql.Named("uid", tx.uid),
		sql.Named("type", v.Type),
		sql.Named("token", v.Token),
	)
	return err
}

type sqlBackend struct {
	db       *sql.DB
	queryMap queryMap
}

func (b *sqlBackend) Close() {
	b.db.Close()
}

func (b *sqlBackend) GetUserByName(ctx context.Context, username string) (*userdb.User, error) {
	return b.getUserWithQuery(ctx, "get_user_by_name", "username", username)
}

func (b *sqlBackend) GetUserByID(ctx context.Context, id string) (*userdb.User, error) {
	return b.getUserWithQuery(ctx, "get_user_by_id", "id", id)
}

func (b *sqlBackend) getUserWithQuery(ctx context.Context, queryName, argName, arg string) (*userdb.User, error) {
	tx, err := b.db.BeginTx(ctx, nil)
	if err != nil {
		return nil, err
	}

	mtx := &mappedTx{Tx: tx, queryMap: b.queryMap}
	uid, user, err := getUser(mtx, b.queryMap, queryName, argName, arg)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, userdb.ErrNotFound
	} else if err != nil {
		return nil, err
	}

	if user.ReadOnly {
		user.Tx = new(userdb.ReadOnlyTx)
	} else {
		user.Tx = &sqlTx{
			mappedTx: mtx,
			uid:      uid,
		}
	}

	return user, nil
}

func newSQLBackendFromParams(sparams *sqlParams) (userdb.Backend, error) {
	db, err := sql.Open(sparams.Driver, sparams.DBURI)
	if err != nil {
		return nil, err
	}

	// Driver-specific tuning.
	switch sparams.Driver {
	case "sqlite3":
		err = setupSQLite(db)
	default:
		err = errors.New("unsupported sql driver")
	}
	if err != nil {
		return nil, err
	}

	return &sqlBackend{
		db:       db,
		queryMap: queryMap(sparams.Queries),
	}, nil
}

func newSQLBackend(params *yaml.Node, _ string) (userdb.Backend, error) {
	sparams := sqlParams{
		Driver:  "sqlite3",
		Queries: defaultQueries,
	}
	if err := params.Decode(&sparams); err != nil {
		return nil, err
	}

	return newSQLBackendFromParams(&sparams)
}

func init() {
	userdb.Register("sql", newSQLBackend)
}
