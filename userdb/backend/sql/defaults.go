package sqlbackend

var DefaultSchema = `
  CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL,
    email TEXT NOT NULL,
    locale TEXT NOT NULL DEFAULT 'en',
    timezone TEXT NOT NULL DEFAULT 'UTC',
    password TEXT NOT NULL,
    totp_secret TEXT
  );
  CREATE INDEX users_username ON users(username);
  CREATE TABLE group_membership (
    group_name TEXT NOT NULL,
    uid INTEGER NOT NULL
  );
  CREATE UNIQUE INDEX idx_group_membership ON group_membership(group_name, uid);
  CREATE TABLE asps (
    uid INTEGER NOT NULL,
    id TEXT NOT NULL,
    service TEXT NOT NULL,
    password TEXT NOT NULL,
    comment TEXT
  );
  CREATE INDEX asp_uid_idx ON asps(uid);
  CREATE INDEX asp_uid_id_idx ON asps(uid, id);
  CREATE TABLE webauthn (
    uid INTEGER NOT NULL,
    key_handle TEXT NOT NULL,
    public_key TEXT NOT NULL,
    comment TEXT
  );
  CREATE INDEX webauthn_uid_idx ON webauthn(uid);
  CREATE INDEX webauthn_uid_key_handle_idx ON webauthn(uid, key_handle);
  CREATE TABLE recovery_tokens (
    uid INTEGER NOT NULL,
    id TEXT NOT NULL,
    encrypted_token TEXT NOT NULL
  );
  CREATE INDEX recovery_tokens_uid_idx ON recovery_tokens(uid);
  CREATE TABLE verification_tokens (
    uid INTEGER NOT NULL,
    type TEXT NOT NULL,
    token TEXT NOT NULL,
    expires_at DATETIME,
    arg TEXT
  );
  CREATE INDEX verification_tokens_uid_token_idx ON verification_tokens(uid, type, token);
`

var defaultQueries = map[string]string{
	"set_encrypted_primary_password":  "UPDATE users SET password = :password WHERE id = :uid",
	"set_totp_secret":                 "UPDATE users SET totp_secret = :secret WHERE id = :uid",
	"delete_totp_secret":              "UPDATE users SET totp_secret = NULL WHERE id = :uid",
	"add_app_specific_password":       "INSERT INTO asps (uid, id, service, password, comment) VALUES (:uid, :asp_id, :asp_service, :asp_password, :asp_name)",
	"update_app_specific_password":    "UPDATE asps SET comment = :asp_name WHERE uid = :uid AND asp_id = :asp_id",
	"delete_app_specific_password":    "DELETE FROM asps WHERE uid = :uid AND id = :asp_id",
	"add_webauthn_registration":       "INSERT INTO webauthn (uid, key_handle, public_key, comment) VALUES (:uid, :registration_key_handle, :registration_public_key, :registration_name)",
	"update_webauthn_registration":    "UPDATE webauthn SET comment = :registration_name WHERE uid = :uid AND key_handle = :registration_key_handle",
	"delete_webauthn_registration":    "DELETE FROM webauthn WHERE uid = :uid AND key_handle = :registration_key_handle",
	"set_user_info":                   "UPDATE users SET name = :name, email = :email, locale = :locale, timezone = :timezone WHERE uid = :uid",
	"get_user_by_name":                "SELECT id, username, email, locale, timezone, password, totp_secret, 0 FROM users WHERE username = :username",
	"get_user_by_id":                  "SELECT id, username, email, locale, timezone, password, totp_secret, 0 FROM users WHERE id = :id",
	"get_user_asps":                   "SELECT id, service, password, comment FROM asps WHERE uid = :uid",
	"get_user_webauthn_registrations": "SELECT key_handle, public_key, comment FROM webauthn WHERE uid = :uid",
	"get_user_encrypted_private_keys": "SELECT authenticator_id, data FROM encryption_keys WHERE uid = :uid",
	"get_user_groups":                 "SELECT group_name FROM group_membership WHERE uid = :uid",
	"add_verification_token":          "INSERT INTO verification_tokens (uid, type, token, expires_at, arg) VALUES (:uid, :type, :token, :expires_at, :arg)",
	"get_verification_token":          "SELECT type, token, expires_at, arg FROM verification_tokens WHERE uid = :uid AND type = :type AND token = :token LIMIT 1",
	"delete_verification_token":       "DELETE FROM verification_tokens WHERE uid = :uid AND type = :type AND token = :token",
	"add_recovery_token":              "INSERT OR REPLACE INTO recovery_tokens (uid, id, encrypted_token) VALUES (:uid, :id, :token)",
	"get_recovery_tokens":             "SELECT id, encrypted_token FROM recovery_tokens WHERE uid = :uid",
	"delete_recovery_token":           "DELETE FROM recovery_tokens WHERE uid = :uid AND id = :id",
}
