package userdb

import (
	"context"
	"fmt"

	"gopkg.in/yaml.v3"
)

type chainParams struct {
	Backends []*Spec `yaml:"backends"`
}

type chainBackend struct {
	backends []Backend
}

func (m *chainBackend) Close() {
	for _, b := range m.backends {
		b.Close()
	}
}

func (m *chainBackend) GetUserByName(ctx context.Context, name string) (*User, error) {
	for _, b := range m.backends {
		if user, err := b.GetUserByName(ctx, name); err == nil {
			return user, err
		}
	}
	return nil, ErrNotFound
}

func (m *chainBackend) GetUserByID(ctx context.Context, id string) (*User, error) {
	for _, b := range m.backends {
		if user, err := b.GetUserByID(ctx, id); err == nil {
			return user, err
		}
	}
	return nil, ErrNotFound
}

func newChainBackendFromParams(params *yaml.Node, dir string) (Backend, error) {
	var mparams chainParams
	if err := params.Decode(&mparams); err != nil {
		return nil, err
	}
	var backends []Backend
	for i, spec := range mparams.Backends {
		b, err := New(spec, dir)
		if err != nil {
			return nil, fmt.Errorf("backend %d: %w", i+1, err)
		}
		backends = append(backends, b)
	}
	return NewChainBackend(backends), nil
}

func NewChainBackend(backends []Backend) Backend {
	return &chainBackend{backends: backends}
}

func init() {
	Register("chain", newChainBackendFromParams)
}
