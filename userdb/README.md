Userdb Configuration
===

Configuration of the user database in *smol/idp* is managed via
YAML-encoded objects. Multiple backend types are supported, each with its own configuration.

Generally speaking a userdb configuration looks like this:

```yaml
type: "<backend-type>"
params: {...}
```

Where *params* holds backend-specific configuration parameters.

# Backends

Note that *smol/idp* does not offer an API to create or delete users,
or otherwise modify parameters which aren't strictly
authentication-related. This is considered the responsibility of your
application layer.

### `static` - Static user database

The *static* userdb backend is useful when your user database is
provisioned and managed by an external system (e.g. via configuration
management). It is read-only.

The user list should be specified as the *users* attribute of
*params*. Each user object can have the following attributes:

* *name* - Username (and user ID).
* *email* - Email address.
* *password* - Encrypted password.
* *totp_secret* - TOTP secret.
* *groups* - List of groups the user is a member of.
* *webauthn_registrations* - List of WebAuthN registrations. Each
  registration must have *key_handle*, *public_key* and an optional
  *comment* attributes.
* *app_specific_passwords* - List of application-specific passwords
  for non-interactive, non-OIDC services. Each object should have
  *id*, *service*, *password* and optional *comment* attributes.

As a convenience, it is possible to ensure that all users from this
backend are members of a set of fixed groups, by specifying the
*static_groups* attribute in *params*.

### `sql` - Generic SQL user database

The *sql* userdb backend can talk to a supported SQL database, using
configurable queries that can be adapted to (almost) arbitrary
schemas. Use it to connect to an externally managed database, e.g.
your main application's. This allows integration of the authentication
layer with existing business logic.

The following *params* are understood:

* *driver* - Database driver (according to Go's database/sql). Only
  `sqlite3` is supported, and is also the default.
* *dburi* - Database URI. For sqlite3 this is the database path.
* *queries* - Map of SQL queries.

#### SQL schema

The SQL backend does **not** manage the database schema in any way, it
is best thought of as a client to an existing database. But a simple
SQL schema for testing purposes is provided for reference in
[backend/sql/defaults.go](backend/sql/defaults.go).

#### SQL query configuration

The set of default SQL queries used by the backend can be examined in
[backend/sql/defaults.go](backend/sql/defaults.go). These can be
individually overridden, in the *queries* configuration parameter,
with anything that returns the same columns in the same order and
accept the same query parameters. Query parameters are named
(recognizable by the syntax `:param`), so their order does not matter.

### `chain` - Concatenate backends

The *chain* userdb backend lets you combine multiple backends, in
sequence. The only accepted configuration parameter is *backends*, a
list of further userdb backend configurations.

### `rpc` - Remote access over GRPC

The *rpc* userdb backend can access the user database over a GRPC API,
defined in [account/proto/account.proto](account/proto/account.proto)
as the *Account* GRPC service. This solution can be useful for
splitting out compute and storage elements of the service
architecture.

The *Account server* is a reference implementation of a server for
this API, exposing a userdb backend.
