package audit

import (
	"context"
	"database/sql"

	apb "git.autistici.org/smol/idp/audit/proto"
	"git.autistici.org/smol/idp/internal/sqlutil"
)

type reader struct {
	db *sql.DB
}

func (r *reader) GetLogs(ctx context.Context, req *apb.GetLogsRequest) (*apb.GetLogsResponse, error) {
	if req.Count == 0 {
		req.Count = 100
	}

	var resp apb.GetLogsResponse
	err := sqlutil.WithReadonlyTx(ctx, r.db, func(tx *sql.Tx) error {
		rows, err := tx.Query(`
			SELECT
  		          timestamp, log_type, user_id, username, service, mechanism, authenticator_id, remote_zone,
		          device_id, device_user_agent, device_browser, device_os, device_mobile, device_unknown
			FROM logs
			WHERE user_id = ? AND timestamp > ?
			ORDER BY timestamp DESC LIMIT ?
`,
			req.UserId,
			req.Since.AsTime(),
			req.Count,
		)
		if err != nil {
			return err
		}
		defer rows.Close()

		for rows.Next() {
			l, err := scanLog(rows)
			if err != nil {
				return err
			}
			resp.Entries = append(resp.Entries, l)
		}

		return rows.Err()
	})
	return &resp, err
}
