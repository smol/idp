package audit

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"testing"

	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/random"
	pb "git.autistici.org/smol/idp/proto"
)

func createTestServer(t *testing.T, numRecords, numUsers, numDevices, numBrowsers int) (*Server, func()) {
	t.Helper()

	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}

	srv, err := New(&Config{DB: dir + "/audit.db"})
	if err != nil {
		t.Fatal(err)
	}

	// Fix the random seed, or TestAnalysis becomes flaky.
	rnd := rand.New(rand.NewSource(42))

	for i := 0; i < numRecords; i++ {
		entry := apb.NewLoginLog(&authpb.Log{
			UserId:    fmt.Sprintf("%d", i%numUsers),
			Username:  fmt.Sprintf("user%d", (i%numUsers)+1),
			Service:   "svc1",
			SessionId: random.UUID(),
			ClientInfo: &authpb.ClientInfo{
				DeviceInfo: &pb.DeviceInfo{
					Id:      fmt.Sprintf("device/%03d", rnd.Intn(numDevices)),
					Browser: fmt.Sprintf("browser/%03d", rnd.Intn(numBrowsers)),
					Os:      fmt.Sprintf("os/%03d", rnd.Intn(3)),
				},
			},
		})

		if _, err := srv.Ingest(context.Background(), entry); err != nil {
			t.Fatalf("Ingest: %v", err)
		}
	}

	return srv, func() {
		srv.Close()
		os.RemoveAll(dir)
	}
}

func TestAnalysis(t *testing.T) {
	srv, cleanup := createTestServer(t, 1000, 1, 2, 2)
	defer cleanup()

	for i, td := range []struct {
		username, device, browser, os string
		expectUnknown                 bool
	}{
		{"user1", "device/001", "browser/001", "os/001", false}, // known device
		{"user1", "device/001", "browser/new", "os/new", false}, // known device (it's just the device)
		{"user2", "device/001", "browser/001", "os/001", true},  // wrong user
		{"user1", "device/new", "browser/001", "os/001", false}, // new device, known fingerprint
		{"user1", "device/new", "browser/new", "os/new", true},  // new device, unknown fingerprint
		{"user1", "device/new", "browser/new", "os/001", true},  // new device, fingerprint similarity below threshold
	} {
		resp, err := srv.CheckDevice(context.Background(), &apb.CheckRequest{
			Username: td.username,
			Service:  "svc1",
			DeviceInfo: &pb.DeviceInfo{
				Id:      td.device,
				Browser: td.browser,
				Os:      td.os,
			},
		})
		if err != nil {
			t.Fatalf("%d: CheckDevice: %v", i, err)
		}
		if resp.Unknown != td.expectUnknown {
			t.Errorf("%d: CheckDevice.Unknown is %v, expected %v", i, resp.Unknown, td.expectUnknown)
		}
	}
}
