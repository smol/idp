package audit

import (
	"context"
	"database/sql"
	"log"

	apb "git.autistici.org/smol/idp/audit/proto"
	"git.autistici.org/smol/idp/internal/sqlutil"
)

func hasDeviceID(tx *sql.Tx, username, deviceID string) bool {
	var n int

	//nolint:errcheck
	tx.QueryRow(`
		SELECT 1 FROM logs WHERE username = ? AND device_id = ? LIMIT 1
`, username, deviceID).Scan(&n)

	return n == 1
}

func requestTokens(req *apb.CheckRequest) map[string]float64 {
	dist := make(map[string]float64)

	if req.RemoteZone != "" {
		dist["zone:"+req.RemoteZone] = 1
	}
	if di := req.DeviceInfo; di != nil {
		dist["browser:"+di.Browser] = 1
		dist["os:"+di.Os] = 1
	}

	return dist
}

func historicalTokenDistribution(tx *sql.Tx, username string) (map[string]float64, error) {
	rows, err := tx.Query(`
		SELECT
		  CONCAT('zone:', remote_zone) AS token, COUNT(*) AS n
		FROM logs
		WHERE username = ? AND remote_zone IS NOT NULL
		GROUP BY token

		UNION

		SELECT
		  CONCAT('browser:', device_browser) AS token, COUNT(*) AS n
		FROM logs
		WHERE username = ? AND device_browser IS NOT NULL
		GROUP BY token

		UNION

		SELECT
		  CONCAT('os:', device_os) AS token, COUNT(*) AS n
		FROM logs
		WHERE username = ? AND device_os IS NOT NULL
		GROUP BY token
`,
		username, username, username)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	dist := make(map[string]float64)
	var total float64
	for rows.Next() {
		var token string
		var n int
		if err := rows.Scan(&token, &n); err == nil {
			dist[token] = float64(n)
			total += float64(n)
		}
	}

	for k, v := range dist {
		dist[k] = v / total
	}

	return dist, rows.Err()
}

func multiplyDist(a, b map[string]float64) float64 {
	if len(a) > len(b) {
		a, b = b, a
	}

	var x float64
	var n int
	for k, v := range a {
		bv, ok := b[k]
		if !ok {
			continue
		}
		x += v * bv
		n++
	}

	if n == 0 {
		return 0
	}

	return x / float64(n)
}

const similarityThreshold = 0.2

type analyzer struct {
	db *sql.DB
}

// CheckDevice evaluates the comparison of the request parameters with
// the distribution of historical values for successful logins.
func (a *analyzer) CheckDevice(ctx context.Context, req *apb.CheckRequest) (*apb.CheckResponse, error) {
	var resp apb.CheckResponse

	err := sqlutil.WithReadonlyTx(ctx, a.db, func(tx *sql.Tx) error {
		// Known device ID?
		if req.DeviceInfo != nil && hasDeviceID(tx, req.Username, req.DeviceInfo.Id) {
			return nil
		}

		hdist, err := historicalTokenDistribution(tx, req.Username)
		if err != nil {
			return err
		}

		rdist := requestTokens(req)
		similarity := multiplyDist(rdist, hdist)
		log.Printf("rdist=%+v, hdist=%+v, similarity=%v", rdist, hdist, similarity)
		resp.Unknown = (similarity < similarityThreshold)

		return nil
	})

	return &resp, err
}
