package audit

import (
	"context"
	"database/sql"
	"log"
	"sync"
	"time"

	apb "git.autistici.org/smol/idp/audit/proto"
	"git.autistici.org/smol/idp/internal/sqlutil"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var compactionInterval = 10 * time.Minute

var timestampRounding = 12 * time.Hour

type ingestor struct {
	db *sql.DB

	ch     chan *apb.Log
	stopCh chan struct{}
	wg     sync.WaitGroup

	mx             sync.Mutex
	needCompaction map[string]struct{}
}

func newIngestor(db *sql.DB) *ingestor {
	g := &ingestor{
		db:             db,
		ch:             make(chan *apb.Log, 100),
		stopCh:         make(chan struct{}),
		needCompaction: make(map[string]struct{}),
	}
	g.wg.Add(2)
	go g.insertionGoroutine()
	go g.compactionGoroutine()
	return g
}

func (g *ingestor) Close() {
	close(g.stopCh)
	g.wg.Wait()
}

func (g *ingestor) insertionGoroutine() {
	defer g.wg.Done()

	for {
		select {
		case <-g.stopCh:
			return

		case l := <-g.ch:
			err := sqlutil.WithTx(
				context.Background(),
				g.db,
				func(tx *sql.Tx) error {
					return insertLog(tx, l)
				})
			if err != nil {
				log.Printf("database error: %v", err)
				// TODO: increment error counter
			}
		}
	}
}

var emptyVar = new(emptypb.Empty)

func (g *ingestor) Ingest(ctx context.Context, req *apb.Log) (*emptypb.Empty, error) {
	g.mx.Lock()
	g.needCompaction[req.AuthLog.UserId] = struct{}{}
	g.mx.Unlock()

	// Modify the incoming record to satisfy PII anonymization
	// requirements.
	//
	// Requirement #1: timestamp rounding. Using Time.Truncate()
	// is better than Time.Round() because it does not insert
	// records in the future.
	req.Timestamp = timestamppb.New(req.Timestamp.AsTime().Truncate(timestampRounding))

	select {
	case g.ch <- req:
		return emptyVar, nil
	case <-ctx.Done():
		return nil, ctx.Err()
	}
}

func (g *ingestor) runCompaction(tx *sql.Tx, userID string) error {
	keep := 30
	_, err := tx.Exec(`
		WITH rows AS (
		  SELECT rowid, ROW_NUMBER() OVER (
		    PARTITION BY service, authenticator_id
		    ORDER BY timestamp DESC
		  ) n
		  FROM logs
		  WHERE user_id = ?
		)
		DELETE FROM logs WHERE rowid IN (
		  SELECT rowid FROM rows WHERE n > ?
		)
`,
		userID, keep)
	return err
}

func (g *ingestor) compact() {
	// Swap the needCompaction map for a new empty one.
	g.mx.Lock()
	needCompaction := g.needCompaction
	g.needCompaction = make(map[string]struct{})
	g.mx.Unlock()

	for userID := range needCompaction {
		err := sqlutil.WithTx(
			context.Background(),
			g.db,
			func(tx *sql.Tx) error {
				return g.runCompaction(tx, userID)
			})
		if err != nil {
			log.Printf("error compacting %s: %v", userID, err)
		}
	}
}

func (g *ingestor) compactionGoroutine() {
	defer g.wg.Done()

	tick := time.NewTicker(compactionInterval)
	defer tick.Stop()

	for {
		select {
		case <-g.stopCh:
			return

		case <-tick.C:
			g.compact()
		}
	}
}
