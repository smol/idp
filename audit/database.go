package audit

import (
	"database/sql"
	"time"

	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/sqlutil"
	pb "git.autistici.org/smol/idp/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var migrations = []func(*sql.Tx) error{
	sqlutil.Statement(`
CREATE TABLE logs (
  timestamp INTEGER NOT NULL,
  log_type TEXT NOT NULL,
  user_id TEXT NOT NULL,
  username TEXT NOT NULL,
  service TEXT NOT NULL,
  mechanism INTEGER NOT NULL,
  authenticator_id TEXT NOT NULL,
  remote_zone TEXT,
  device_id TEXT,
  device_user_agent TEXT,
  device_browser TEXT,
  device_os TEXT,
  device_mobile BOOL,
  device_unknown BOOL
)
`, `
CREATE INDEX idx_logs_username_device_id ON logs(username, device_id)
`, `
CREATE INDEX idx_logs_user_id ON logs(user_id)
`, `
CREATE INDEX idx_logs_user_id_service_authid ON logs(user_id, service, authenticator_id)
`),
}

func setString(target *sql.NullString, s string) {
	target.Valid = true
	target.String = s
}

func setBool(target *sql.NullBool, value bool) {
	target.Valid = true
	target.Bool = value
}

func insertLog(tx *sql.Tx, log *apb.Log) error {
	var remoteZone, deviceId, deviceUA, deviceBrowser, deviceOS sql.NullString
	var deviceMobile, deviceUnknown sql.NullBool

	if log.AuthLog.ClientInfo != nil {
		if s := log.AuthLog.ClientInfo.RemoteZone; s != "" {
			setString(&remoteZone, s)
		}
		setBool(&deviceUnknown, log.AuthLog.ClientInfo.Unknown)

		if di := log.AuthLog.ClientInfo.DeviceInfo; di != nil {
			setString(&deviceId, di.Id)
			setString(&deviceUA, di.UserAgent)
			setString(&deviceBrowser, di.Browser)
			setString(&deviceOS, di.Os)
			setBool(&deviceMobile, di.Mobile)
		}
	}

	_, err := tx.Exec(`
		INSERT INTO logs (
		  timestamp, log_type, user_id, username, service, mechanism, authenticator_id, remote_zone,
		  device_id, device_user_agent, device_browser, device_os, device_mobile, device_unknown
		) VALUES (
		  ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
		)`,
		log.Timestamp.AsTime(),
		log.Type,
		log.AuthLog.UserId,
		log.AuthLog.Username,
		log.AuthLog.Service,
		log.AuthLog.Mechanism,
		log.AuthLog.AuthenticatorId.String(),
		remoteZone,
		deviceId,
		deviceUA,
		deviceBrowser,
		deviceOS,
		deviceMobile,
		deviceUnknown,
	)
	return err
}

func scanLog(rows *sql.Rows) (*apb.Log, error) {
	var timestamp time.Time
	var remoteZone, deviceId, deviceUA, deviceBrowser, deviceOS sql.NullString
	var deviceMobile, deviceUnknown sql.NullBool
	var authIdStr string

	log := apb.Log{
		AuthLog: &authpb.Log{
			ClientInfo: &authpb.ClientInfo{},
		},
	}

	if err := rows.Scan(
		&timestamp,
		&log.Type,
		&log.AuthLog.UserId,
		&log.AuthLog.Username,
		&log.AuthLog.Service,
		&log.AuthLog.Mechanism,
		&authIdStr,
		&remoteZone,
		&deviceId,
		deviceUA,
		deviceBrowser,
		deviceOS,
		deviceMobile,
		deviceUnknown,
	); err != nil {
		return nil, err
	}

	log.Timestamp = timestamppb.New(timestamp)

	if remoteZone.Valid {
		log.AuthLog.ClientInfo.RemoteZone = remoteZone.String
	}
	if deviceId.Valid {
		log.AuthLog.ClientInfo.DeviceInfo = &pb.DeviceInfo{
			Id:        deviceId.String,
			UserAgent: deviceUA.String,
			Browser:   deviceBrowser.String,
			Os:        deviceOS.String,
			Mobile:    deviceMobile.Bool,
		}
	}
	if deviceUnknown.Valid {
		log.AuthLog.ClientInfo.Unknown = deviceUnknown.Bool
	}

	return &log, nil
}
