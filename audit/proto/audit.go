package proto

import (
	authpb "git.autistici.org/smol/idp/authn/proto"
	timestamppb "google.golang.org/protobuf/types/known/timestamppb"
)

func NewLoginLog(authLog *authpb.Log) *Log {
	return &Log{
		Type:      Log_TYPE_LOGIN,
		Timestamp: timestamppb.Now(),
		AuthLog:   authLog,
		Subject:   authLog.UserId,
	}
}

func NewPasswordChangeLog(userID string, authLog *authpb.Log) *Log {
	return &Log{
		Type:      Log_TYPE_PRIMARY_PASSWORD_CHANGE,
		Timestamp: timestamppb.Now(),
		AuthLog:   authLog,
		Subject:   userID,
	}
}

func NewAddAppSpecificPasswordLog(userID, aspID string, authLog *authpb.Log) *Log {
	return &Log{
		Type:      Log_TYPE_ASP_CREATED,
		Timestamp: timestamppb.Now(),
		AuthLog:   authLog,
		Subject:   userID,
		Arg:       aspID,
	}
}

func NewDeleteAppSpecificPasswordLog(userID, aspID string, authLog *authpb.Log) *Log {
	return &Log{
		Type:      Log_TYPE_ASP_DELETED,
		Timestamp: timestamppb.Now(),
		AuthLog:   authLog,
		Subject:   userID,
		Arg:       aspID,
	}
}

func NewAddWebAuthnLog(userID, kh string, authLog *authpb.Log) *Log {
	return &Log{
		Type:      Log_TYPE_WEBAUTHN_CREATED,
		Timestamp: timestamppb.Now(),
		AuthLog:   authLog,
		Subject:   userID,
		Arg:       kh,
	}
}

func NewDeleteWebAuthnLog(userID, kh string, authLog *authpb.Log) *Log {
	return &Log{
		Type:      Log_TYPE_WEBAUTHN_DELETED,
		Timestamp: timestamppb.Now(),
		AuthLog:   authLog,
		Subject:   userID,
		Arg:       kh,
	}
}

func NewSetOTPLog(userID string, authLog *authpb.Log) *Log {
	return &Log{
		Type:      Log_TYPE_TOTP_SET,
		Timestamp: timestamppb.Now(),
		AuthLog:   authLog,
		Subject:   userID,
	}
}

func NewDeleteOTPLog(userID string, authLog *authpb.Log) *Log {
	return &Log{
		Type:      Log_TYPE_TOTP_DELETED,
		Timestamp: timestamppb.Now(),
		AuthLog:   authLog,
		Subject:   userID,
	}
}
