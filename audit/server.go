package audit

import (
	"context"

	apb "git.autistici.org/smol/idp/audit/proto"
	"git.autistici.org/smol/idp/internal/sqlutil"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Server struct {
	*ingestor
	*analyzer
	*reader

	*apb.UnimplementedAuditServer
}

type Config struct {
	DB string `yaml:"db_path" doc:"path to the database file"`
}

func New(conf *Config) (*Server, error) {
	db, err := sqlutil.OpenDB(
		conf.DB,
		sqlutil.WithMigrations(migrations),
	)
	if err != nil {
		return nil, err
	}

	return &Server{
		ingestor: newIngestor(db),
		analyzer: &analyzer{db: db},
		reader:   &reader{db: db},
	}, nil
}

func (s *Server) Close() {
	s.ingestor.Close()

	// Really any of the db references will do.
	s.ingestor.db.Close()
}

func (s *Server) Ingest(ctx context.Context, req *apb.Log) (*emptypb.Empty, error) {
	return s.ingestor.Ingest(ctx, req)
}

func (s *Server) CheckDevice(ctx context.Context, req *apb.CheckRequest) (*apb.CheckResponse, error) {
	return s.analyzer.CheckDevice(ctx, req)
}

func (s *Server) GetLogs(ctx context.Context, req *apb.GetLogsRequest) (*apb.GetLogsResponse, error) {
	return s.reader.GetLogs(ctx, req)
}
