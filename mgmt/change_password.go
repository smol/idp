package mgmt

import (
	"net/http"

	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/internal/web/forms"
	"git.autistici.org/smol/idp/userdb"
)

const changePasswordWorkflow = "change-password"

var minPasswordLength = 8

type changePasswordAction struct {
	form forms.Form
}

func newChangePasswordAction(authClient AuthClient, authService string) *changePasswordAction {
	return &changePasswordAction{
		form: forms.Form{
			Fields: []forms.Field{
				forms.NewPasswordField("cur_password", "Current password").
					WithValidators(primaryPasswordValidator(authClient, authService)),
				forms.NewPasswordField("new_password", "New password").
					WithDescription("Choose a new, strong password. Preferably use a password manager if you can.").
					WithValidators(
						forms.MinLength(minPasswordLength),
						forms.GoodQualityPassword(2, []string{"passwords", "english_wikipedia", "surnames"}),
					),
				forms.NewPasswordField("new_password_confirm", "New password (confirm)"),
			},
			Validators: []forms.FormValidatorFunc{
				forms.FormFieldsEqual("new_password", "new_password_confirm",
					"The new passwords are not identical, try again."),
				forms.FormFieldsDiffer("cur_password", "new_password",
					"The new password can't be the same as the old one, try again."),
			},
		},
	}
}

func (a *changePasswordAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	form, ok := a.form.ValidateOnSubmit(req)
	if ok {
		err := user.SetPrimaryPassword(
			unlockCredentials(form),
			form.Get("new_password"),
		)

		if err == nil {
			return withCompleteWorkflow(
				withFlashOk(
					newRedirect("/account/overview"),
					"Password changed successfully"),
				changePasswordWorkflow)
		}

		form.AddError(err)
	}

	return htmlResponse(template.WithData(map[string]any{
		"Form": form,
	}), req, "action_change_password.html")
}
