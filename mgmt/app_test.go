package mgmt

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"git.autistici.org/smol/idp"
	"git.autistici.org/smol/idp/internal/testutil"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/login"
	lpb "git.autistici.org/smol/idp/login/proto"
	"git.autistici.org/smol/idp/ui"
	"git.autistici.org/smol/idp/userdb"
	"github.com/gorilla/securecookie"
)

var fixtures = `
INSERT INTO users (id, username, email, password) VALUES (1, 'admin', 'admin@example.com', 'foo');
INSERT INTO asps (uid, id, service, password, comment) VALUES (1, 'abcdef', 'service1', 'foo', 'comment1');
INSERT INTO asps (uid, id, service, password, comment) VALUES (1, 'zxcvbn', 'service2', 'foo', 'comment2');
`

func withFakeLogin(h http.Handler) http.Handler {
	auth := lpb.Auth{
		UserId:   "1",
		Username: "admin",
	}
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		req = login.AddAuthToRequest(req, &auth)
		h.ServeHTTP(w, req)
	})
}

func mkTestApp(t *testing.T) (*httptest.Server, userdb.Backend, func()) {
	db, cleanup, err := testutil.CreateTestDB(fixtures, "")
	if err != nil {
		t.Fatal(err)
	}

	uitpl := ui.MustParseTemplates(new(web.Config))

	var conf Config
	conf.WebAuthn.DisplayName = "foo"
	conf.WebAuthn.RPID = "foo.example.com"
	conf.WebAuthn.Origin = "foo.example.com"
	conf.CookieAuthKey = securecookie.GenerateRandomKey(64)
	conf.CookieEncKey = securecookie.GenerateRandomKey(32)
	features := &idp.Features{
		EnableASPs:     true,
		EnableOTP:      true,
		EnableWebAuthn: true,
		EnableMessages: true,
	}

	app, err := NewApp(db, &conf, features, new(testutil.FakeAuthClient), uitpl)
	if err != nil {
		t.Fatalf("NewApp: %v", err)
	}

	srv := httptest.NewServer(withFakeLogin(app))

	return srv, db, func() {
		srv.Close()
		cleanup()
	}
}

func TestApp_OverviewPages(t *testing.T) {
	srv, _, cleanup := mkTestApp(t)
	defer cleanup()

	client := testutil.NewClient()

	for _, uri := range []string{
		"/",
		"/account/overview",
		"/account/pwchange",
		"/account/asp/list",
		"/account/asp/create",
		"/account/asp/edit?id=abcdef",
		"/account/webauthn/list",
		"/account/webauthn/register",
		"/account/recovery/reset",
	} {
		_, _, err := client.Request("GET", srv.URL+uri, nil)
		if err != nil {
			t.Errorf("GET %s: %v", uri, err)
			continue
		}
	}
}
