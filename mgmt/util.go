package mgmt

import (
	"net/http"

	"git.autistici.org/smol/idp/internal/sessionstore"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/internal/web/forms"
	"git.autistici.org/smol/idp/login"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
)

// Minimal web framework, tailored for this specific kind of app.

// Generic account action handler.
//
// Handlers are structured in such a way as to decouple the database
// transaction (which should be fast) from serving the HTTP response
// (which can be slow depending on how the client consumes the input).
type actionHandler interface {
	ServeAction(*http.Request, *userdb.User, *web.Template) Response
}

type Response interface {
	http.Handler
}

// HTML (templated) deferred response.
func htmlResponse(tpl *web.Template, _ *http.Request, name string) Response {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		tpl.WithFlashes(w, req).Execute(w, req, name)
	})
}

// Redirect.
type redirectResponse struct {
	uri string
}

func newRedirect(uri string) Response {
	return &redirectResponse{uri}
}

func (r *redirectResponse) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	http.Redirect(w, req, r.uri, http.StatusFound)
}

// 404 Not Found.
type notFoundResponse struct{}

func notFound() *notFoundResponse { return new(notFoundResponse) }

func (r *notFoundResponse) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	http.NotFound(w, req)
}

// HTTP error response.
type errorResponse struct {
	code int
}

func newErrorResponse(code int) *errorResponse {
	return &errorResponse{code: code}
}

func (r *errorResponse) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	http.Error(w, "", r.code)
}

// Wrapper to run something before the response.
type beforeResponse struct {
	wrap Response
	f    func(http.ResponseWriter, *http.Request)
}

func withBeforeResponse(wrap Response, f func(http.ResponseWriter, *http.Request)) Response {
	return &beforeResponse{
		wrap: wrap,
		f:    f,
	}
}

func (r *beforeResponse) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	r.f(w, req)
	r.wrap.ServeHTTP(w, req)
}

func withCompleteWorkflow(wrap Response, workflow string) Response {
	return withBeforeResponse(wrap, func(w http.ResponseWriter, req *http.Request) {
		login.CompleteWorkflow(w, req, workflow)
	})
}

// Response middleware that flashes a message.
func withFlashOk(wrap Response, msg string) Response {
	return withBeforeResponse(wrap, func(w http.ResponseWriter, req *http.Request) {
		web.FlashOK(w, req, msg)
	})
}

// Response middleware that flashes an error message.
func withFlashErr(wrap Response, msg string) Response {
	return withBeforeResponse(wrap, func(w http.ResponseWriter, req *http.Request) {
		web.FlashErr(w, req, msg)
	})
}

func withSetSessionCookie(wrap Response, store *sessionstore.Store, value any) Response {
	return withBeforeResponse(wrap, func(w http.ResponseWriter, req *http.Request) {
		store.Set(w, value) //nolint:errcheck
	})
}

func withDeleteSessionCookie(wrap Response, store *sessionstore.Store) Response {
	return withBeforeResponse(wrap, func(w http.ResponseWriter, req *http.Request) {
		store.Delete(w) //nolint:errcheck
	})
}

func unlockCredentials(form *forms.Form) *userdb.UnlockCredentials {
	return &userdb.UnlockCredentials{
		AuthID:   pb.NewPrimaryPasswordAuthenticatorID(),
		Password: form.Get("cur_password"),
	}
}
