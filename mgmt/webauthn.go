package mgmt

import (
	"encoding/hex"
	"fmt"
	"net/http"
	"strings"

	"git.autistici.org/smol/idp/internal/cryptutil"
	"git.autistici.org/smol/idp/internal/sessionstore"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/internal/web/forms"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
	"github.com/go-webauthn/webauthn/protocol"
	"github.com/go-webauthn/webauthn/webauthn"
)

const enable2FAWorkflow = "enable-2fa"

// Short-term storage for webauthn sessionData is implemented as a client-side cookie.
const webauthnSessionCookieName = "webauthn_session"

type listWebAuthnAction struct{}

func (a *listWebAuthnAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	return htmlResponse(template, req, "action_webauthn_list.html")
}

var registerWebAuthnForm = forms.Form{
	Fields: []forms.Field{
		forms.NewHiddenField("webauthn_response").
			WithValidators(forms.NotEmpty),
		forms.NewTextField("comment", "Comment").
			WithDescription(`A short name or comment to let you recognize this specific client
 (something like "laptop" or "phone" for instance, or whatever you like).`).
			WithValidators(forms.Strip, forms.NotEmpty),
	},
}

type registerWebAuthnAction struct {
	store *sessionstore.Store
	wh    *webauthn.WebAuthn
}

func newRegisterWebAuthnAction(displayName, rpid, origin string, authKey cryptutil.SessionAuthenticationKey, encKey cryptutil.SessionEncryptionKey) (*registerWebAuthnAction, error) {
	wh, err := webauthn.New(&webauthn.Config{
		RPDisplayName: displayName,
		RPID:          rpid,
		RPOrigins:     []string{origin},
	})
	if err != nil {
		return nil, err
	}

	store := sessionstore.New(webauthnSessionCookieName, "/account/webauthn/register", authKey, encKey, 900)

	return &registerWebAuthnAction{
		store: store,
		wh:    wh,
	}, nil
}

func (a *registerWebAuthnAction) createCredential(req *http.Request, user *userdb.User, responseData string) (*webauthn.Credential, error) {
	var sessionData webauthn.SessionData

	if err := a.store.Get(req, &sessionData); err != nil {
		return nil, fmt.Errorf("failed to retrieve session data: %w", err)
	}

	parsedResponse, err := protocol.ParseCredentialCreationResponseBody(strings.NewReader(responseData))
	if err != nil {
		return nil, fmt.Errorf("failed to parse webauthn response: %w", err)
	}

	cred, err := a.wh.CreateCredential(user, sessionData, parsedResponse)
	if err != nil {
		return nil, fmt.Errorf("failed to create WebAuthn credential: %w", err)
	}

	return cred, nil
}

func (a *registerWebAuthnAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	oldTokenCount := len(user.WebAuthnRegistrations)

	form, ok := registerWebAuthnForm.ValidateOnSubmit(req)
	if ok {
		cred, err := a.createCredential(req, user, form.Get("webauthn_response"))
		if err == nil {
			err = user.AddWebAuthnRegistration(&pb.WebAuthnRegistration{
				Name:      form.Get("comment"),
				KeyHandle: string(cred.ID),
				PublicKey: string(cred.PublicKey),
			})

			if err == nil {
				var resp Response
				if oldTokenCount == 0 {
					resp = newRedirect("/account/interstitial/2fa-enabled")
				} else {
					resp = withCompleteWorkflow(
						withFlashOk(
							newRedirect("/account/webauthn/list"),
							"Token registered successfully."),
						enable2FAWorkflow)
				}
				return withDeleteSessionCookie(resp, a.store)
			}
		}

		// If we fail registration, it's likely not something
		// the user can do anything about, so avoid showing
		// the form again but flash an error instead.
		return withFlashErr(
			newRedirect("/account/webauthn/list"),
			err.Error())
	}

	if form.Get("webauthn_response") == "" {
		// Only generate a webauthn challenge on a 'fresh' request.
		requireResidentKey := false
		cc, sessionData, err := a.wh.BeginRegistration(
			user,
			webauthn.WithAuthenticatorSelection(protocol.AuthenticatorSelection{
				RequireResidentKey: &requireResidentKey,
				ResidentKey:        protocol.ResidentKeyRequirementDiscouraged,
				UserVerification:   protocol.VerificationPreferred,
			}),
		)
		if err != nil {
			return newErrorResponse(http.StatusInternalServerError)
		}
		return withSetSessionCookie(
			htmlResponse(template.WithData(map[string]any{
				"Form":              form,
				"WebAuthnChallenge": cc,
			}), req, "action_webauthn_register.html"),
			a.store, sessionData)
	}

	return htmlResponse(template.WithData(map[string]any{
		"Form":              form,
		"WebAuthnChallenge": nil,
	}), req, "action_webauthn_register.html")
}

var editWebAuthnForm = editASPForm

type editWebAuthnAction struct{}

func (a *editWebAuthnAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	keyHandle, _ := hex.DecodeString(req.FormValue("key"))
	reg := user.GetWebAuthnRegistrationByKeyHandle(string(keyHandle))
	if reg == nil {
		return notFound()
	}

	form, ok := editWebAuthnForm.WithDefaults(map[string]string{"comment": reg.Name}).ValidateOnSubmit(req)
	if ok {
		reg.Name = form.Get("comment")
		err := user.UpdateWebAuthnRegistration(reg)

		if err == nil {
			return withFlashOk(
				newRedirect("/account/webauthn/list"),
				"Comment updated.")
		}

		form.AddError(err)
	}

	return htmlResponse(template.WithData(map[string]any{
		"Form":         form,
		"Registration": reg,
	}), req, "action_webauthn_edit.html")
}

type deleteWebAuthnAction struct{}

func (a *deleteWebAuthnAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	keyHandle, _ := hex.DecodeString(req.FormValue("key"))
	reg := user.GetWebAuthnRegistrationByKeyHandle(string(keyHandle))
	if reg == nil {
		return notFound()
	}

	if err := user.DeleteWebAuthnRegistration(reg); err != nil {
		return withFlashErr(
			newRedirect("/account/webauthn/list"),
			err.Error())
	}

	return withFlashOk(
		newRedirect("/account/webauthn/list"),
		"Hardware token deleted")
}
