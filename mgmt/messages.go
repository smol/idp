package mgmt

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"

	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/userdb"
)

type messageAction struct {
	messages map[string]*template.Template
}

func (a *messageAction) ServeAction(req *http.Request, user *userdb.User, tpl *web.Template) Response {
	id := req.FormValue("msgid")
	msg, ok := a.messages[id]
	if !ok {
		return notFound()
	}

	if req.Method == "POST" {
		// Register that the message has been seen.
		return newRedirect("/account/overview")
	}

	var buf bytes.Buffer
	if err := msg.Execute(&buf, map[string]any{"User": user}); err != nil {
		return newErrorResponse(http.StatusInternalServerError)
	}
	return htmlResponse(tpl.WithData(map[string]any{
		// Message contents are under our control and we trust
		// them to be valid HTML.
		"Content": template.HTML(buf.String()), //nolint:gosec
	}), req, "account_message.html")
}

func newMessageAction(msgs map[string]string) (*messageAction, error) {
	tpls := make(map[string]*template.Template)
	for name, tplSrc := range msgs {
		tpl, err := template.New(name).Parse(tplSrc)
		if err != nil {
			return nil, fmt.Errorf("template error in '%s': %w", name, err)
		}
		tpls[name] = tpl
	}
	return &messageAction{messages: tpls}, nil
}
