package mgmt

import (
	"net/http"
	"time"

	"git.autistici.org/smol/idp/internal/random"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/internal/web/forms"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
)

const resetRecoveryWorkflow = "set-recovery"

const mainRecoveryTokenID = "recovery"

var resetRecoveryForm = forms.Form{
	Fields: []forms.Field{
		forms.NewPasswordField("cur_password", "Current password"),
	},
}

type resetRecoveryAction struct{}

func (a *resetRecoveryAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	form, ok := resetRecoveryForm.ValidateOnSubmit(req)
	if ok {
		token := random.Base32(40)

		err := user.AddRecoveryToken(
			unlockCredentials(form),
			pb.RecoveryToken_TOKEN_OFFLINE,
			mainRecoveryTokenID,
			token,
			time.Time{},
		)

		if err == nil {
			return withCompleteWorkflow(
				htmlResponse(template.WithData(map[string]any{
					"Token": token,
				}), req, "action_recovery_reset_ok.html"),
				resetRecoveryWorkflow)
		}

		form.AddError(err)
	}

	return htmlResponse(template.WithData(map[string]any{
		"Form": form,
	}), req, "action_recovery_reset.html")
}
