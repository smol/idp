package mgmt

import (
	"net/http"

	"git.autistici.org/smol/idp/internal/random"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/internal/web/forms"
	"git.autistici.org/smol/idp/userdb"
)

type listASPAction struct{}

func (a *listASPAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	return htmlResponse(template, req, "action_asp_list.html")
}

type createASPAction struct {
	form forms.Form
}

func aspServicesToOptions(l []string) []forms.Option {
	var out []forms.Option
	for _, value := range l {
		out = append(out, forms.Option{Name: value, Value: value})
	}
	return out
}

func newCreateASPAction(aspServices []string, authClient AuthClient, authService string) *createASPAction {
	choices := aspServicesToOptions(aspServices)

	// Dynamic form that requires configuration variables.
	return &createASPAction{
		form: forms.Form{
			Fields: []forms.Field{
				forms.NewPasswordField("cur_password", "Current password").
					WithValidators(primaryPasswordValidator(authClient, authService)),
				forms.NewSelectField("service", "Service", choices).
					WithValidators(forms.OneOf(choices)),
				forms.NewTextField("comment", "Comment").
					WithDescription(`A short name or comment to let you recognize this specific client
 (something like "laptop" or "phone" for instance, or whatever you like).`).
					WithValidators(forms.Strip, forms.NotEmpty),
			},
		},
	}
}

func (a *createASPAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	form, ok := a.form.ValidateOnSubmit(req)
	if ok {

		newPw := random.Hex(32)

		err := user.AddAppSpecificPassword(
			unlockCredentials(form),
			newPw,
			form.Get("service"),
			form.Get("comment"),
		)

		if err == nil {
			return htmlResponse(template.WithData(map[string]any{
				"Comment":  form.Get("comment"),
				"Service":  form.Get("service"),
				"Password": newPw,
			}), req, "action_asp_create_ok.html")
		}

		form.AddError(err)
	}

	return htmlResponse(template.WithData(map[string]any{
		"Form": form,
	}), req, "action_asp_create.html")
}

var editASPForm = forms.Form{
	Fields: []forms.Field{
		forms.NewTextField("comment", "Comment").
			WithDescription(`A short name or comment to let you recognize this specific client
 (something like "laptop" or "phone" for instance, or whatever you like).`).
			WithValidators(forms.Strip, forms.NotEmpty),
	},
}

type editASPAction struct{}

func (a *editASPAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	asp := user.GetAppSpecificPasswordByID(req.FormValue("id"))
	if asp == nil {
		return notFound()
	}

	form, ok := editASPForm.WithDefaults(map[string]string{"comment": asp.Name}).ValidateOnSubmit(req)
	if ok {
		asp.Name = form.Get("comment")
		err := user.UpdateAppSpecificPassword(asp)

		if err == nil {
			return withFlashOk(
				newRedirect("/account/asp/list"),
				"Comment updated")
		}

		form.AddError(err)
	}

	return htmlResponse(template.WithData(map[string]any{
		"Form": form,
		"ASP":  asp,
	}), req, "action_asp_edit.html")
}

type deleteASPAction struct{}

func (a *deleteASPAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	asp := user.GetAppSpecificPasswordByID(req.FormValue("id"))
	if asp == nil {
		return notFound()
	}

	if err := user.DeleteAppSpecificPassword(asp); err != nil {
		return withFlashErr(
			newRedirect("/account/asp/list"),
			err.Error())
	}

	return withFlashOk(
		newRedirect("/account/asp/list"),
		"Application specific password deleted")
}
