package mgmt

import (
	"context"
	"errors"
	"log"
	"net/http"
	"time"

	"git.autistici.org/smol/idp"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/cryptutil"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/internal/web/forms"
	"git.autistici.org/smol/idp/login"
	"git.autistici.org/smol/idp/userdb"
	"google.golang.org/grpc"
)

type app struct {
	db       userdb.Backend
	template *web.Template
}

// AuthClient is a wrapper interface for an id/auth.Client that adds
// support for a Logout event. This allows injection of state-aware
// components that can trigger on both successful authentication and
// logout to maintain external session-scoped state.
type AuthClient interface {
	Authenticate(context.Context, *authpb.Request, ...grpc.CallOption) (*authpb.Response, error)
}

type Config struct {
	KnownASPServices []string          `yaml:"known_asp_services"`
	Messages         map[string]string `yaml:"messages"`

	WebAuthn struct {
		DisplayName string `yaml:"display_name"`
		RPID        string `yaml:"rpid"`
		Origin      string `yaml:"origin"`
	} `yaml:"webauthn"`

	ConfirmAuthService string `yaml:"confirm_auth_service" doc:"authentication service for password confirmations"`

	CookieAuthKey cryptutil.SessionAuthenticationKey `yaml:"cookie_auth_key" doc:"authentication key for cookies"`
	CookieEncKey  cryptutil.SessionEncryptionKey     `yaml:"cookie_enc_key" doc:"encryption key for cookies"`
	CSRFKey       cryptutil.SessionEncryptionKey     `yaml:"csrf_key" doc:"encryption key for CSRF cookies (disable CSRF if not set)"`
}

func NewApp(db userdb.Backend, config *Config, features *idp.Features, authClient AuthClient, template *web.Template) (http.Handler, error) {
	mux := http.NewServeMux()

	template = template.WithData(map[string]any{
		"Features": features,
	})

	app := &app{
		db:       db,
		template: template,
	}

	if features.EnableASPs {
		mux.Handle("/account/asp/list", app.actionHandler(new(listASPAction)))
		mux.Handle("/account/asp/create", app.actionHandler(newCreateASPAction(
			config.KnownASPServices, authClient, config.ConfirmAuthService)))
		mux.Handle("/account/asp/edit", app.actionHandler(new(editASPAction)))
		mux.Handle("/account/asp/delete", app.actionHandler(new(deleteASPAction)))
	}

	if features.EnableWebAuthn {
		login.RegisterWorkflow(enable2FAWorkflow, "/account/webauthn/register", "/account/interstitial/2fa-enabled")
		wa, err := newRegisterWebAuthnAction(
			config.WebAuthn.DisplayName, config.WebAuthn.RPID, config.WebAuthn.Origin,
			config.CookieAuthKey, config.CookieEncKey)
		if err != nil {
			return nil, err
		}
		mux.Handle("/account/webauthn/list", app.actionHandler(new(listWebAuthnAction)))
		mux.Handle("/account/webauthn/register", app.actionHandler(wa))
		mux.Handle("/account/webauthn/edit", app.actionHandler(new(editWebAuthnAction)))
		mux.Handle("/account/webauthn/delete", app.actionHandler(new(deleteWebAuthnAction)))

		mux.Handle("/account/interstitial/2fa-enabled", app.actionHandler(new(interstitialTFAEnabledAction)))
	}

	if features.EnableMessages {
		ma, err := newMessageAction(config.Messages)
		if err != nil {
			return nil, err
		}
		login.RegisterWorkflow("show-messages", "/account/message")
		mux.Handle("/account/message", app.actionHandler(ma))
	}

	login.RegisterWorkflow(changePasswordWorkflow, "/account/change-password")
	mux.Handle("/account/pwchange", app.actionHandler(
		newChangePasswordAction(authClient, config.ConfirmAuthService)))

	login.RegisterWorkflow(resetRecoveryWorkflow, "/account/recovery/reset")
	mux.Handle("/account/recovery/reset", app.actionHandler(new(resetRecoveryAction)))

	mux.Handle("/account/overview", app.actionHandler(new(overviewAction)))

	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path == "" || req.URL.Path == "/" {
			http.Redirect(w, req, "/account/overview", http.StatusFound)
			return
		}
		http.NotFound(w, req)
	})

	h := web.WithFlash(config.CookieAuthKey, config.CookieEncKey, "/account/", mux)
	h = web.CSRF(
		config.CSRFKey,
		"_mgmt_csrf",
		"/account/",
	)(h)

	return h, nil
}

func (app *app) actionHandler(h actionHandler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		auth, ok := login.GetAuth(req.Context())
		if !ok {
			// Woah.
			log.Printf("failed to retrieve authenticated session")
			http.Error(w, "No session", http.StatusBadRequest)
			return
		}

		user, err := app.db.GetUserByID(req.Context(), auth.UserId)
		if err != nil {
			// Woah again.
			log.Printf("failed to retrieve user '%s' from authenticated session", auth.Username)
			http.Error(w, "No user", http.StatusBadRequest)
			return
		}

		resp := h.ServeAction(req, user, app.template.WithData(map[string]any{
			"User": user,
		}))

		if err := user.Commit(); err != nil {
			log.Printf("commit error: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		resp.ServeHTTP(w, req)
	})
}

type overviewAction struct{}

func (a *overviewAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	return htmlResponse(template, req, "account_overview.html")
}

type interstitialTFAEnabledAction struct{}

func (a *interstitialTFAEnabledAction) ServeAction(req *http.Request, user *userdb.User, template *web.Template) Response {
	return withCompleteWorkflow(
		htmlResponse(template, req, "account_interstitial_2fa_enabled.html"),
		enable2FAWorkflow)
}

var authRPCTimeout = 5 * time.Second

func primaryPasswordValidator(client AuthClient, service string) forms.FieldValidatorFunc {
	return func(ctx context.Context, s string) (string, error) {
		ctx, cancel := context.WithTimeout(ctx, authRPCTimeout)
		defer cancel()

		auth, _ := login.GetAuth(ctx)

		resp, err := client.Authenticate(ctx, &authpb.Request{
			Username:  auth.Username,
			Password:  s,
			Service:   service,
			Mechanism: authpb.Mechanism_PASSWORD,
		})
		if err != nil {
			return "", err
		}
		if resp.Status != authpb.Response_STATUS_OK {
			return "", errors.New("invalid password")
		}
		return s, nil
	}
}
