//go:generate npm install
//go:generate env GO_PACKAGE=git.autistici.org/smol/idp npm run build
package ui

import (
	"embed"
	"net/http"

	"git.autistici.org/smol/idp/internal/web"
)

var (
	//go:embed assets
	assetsFS embed.FS

	//go:embed templates
	templateFS embed.FS
)

func WithAssets(h http.Handler) http.Handler {
	mux := http.NewServeMux()

	mux.Handle("/assets/", web.AssetsHandler(assetsFS))
	mux.Handle("/", h)

	return mux
}

func MustParseTemplates(config *web.Config) *web.Template {
	return web.MustParseTemplates(templateFS, assetInfo, config)
}
