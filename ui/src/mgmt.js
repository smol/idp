import { bufferEncode, bufferDecode } from './_encode.js';

function showWebauthnStatus(msg) {
    var p = document.createElement('p');
    p.appendChild(document.createTextNode(msg));
    var actel = document.getElementById('webauthn-activity');
    actel.innerHTML = '';
    actel.appendChild(p);
    return p;
}

function registerCredential(makeCredentialOptions) {
    makeCredentialOptions.challenge = bufferDecode(makeCredentialOptions.challenge);
    makeCredentialOptions.user.id = bufferDecode(makeCredentialOptions.user.id);
    if (makeCredentialOptions.excludeCredentials) {
        for (var i = 0; i < makeCredentialOptions.excludeCredentials.length; i++) {
            makeCredentialOptions.excludeCredentials[i].id = bufferDecode(makeCredentialOptions.excludeCredentials[i].id);
        }
    }
    console.log("Credential Creation Options:");
    console.log(makeCredentialOptions);

    navigator.credentials.create({
        publicKey: makeCredentialOptions,
    }).then((newCredential) => {
        console.log("PublicKeyCredential Created");
        console.log(newCredential);

        // Move data into Arrays incase it is super long
	let attestationObject = new Uint8Array(newCredential.response.attestationObject);
	let clientDataJSON = new Uint8Array(newCredential.response.clientDataJSON);
	let rawId = new Uint8Array(newCredential.rawId);

        // Save the data in the form.
        document.getElementById('field-webauthn_response').value = JSON.stringify({
	    id: newCredential.id,
	    rawId: bufferEncode(rawId),
	    type: newCredential.type,
	    response: {
		attestationObject: bufferEncode(attestationObject),
		clientDataJSON: bufferEncode(clientDataJSON),
	    },
	});

        // Write some text for feedback.
        showWebauthnStatus('Touch recorded, now pick a short memorable name for this token:');

    }).catch((err) => {
        console.info(err);
        showWebauthnStatus('Error: ' + err.message).classList.add('error');
    });
}

// Top-level initialization.
document.addEventListener("DOMContentLoaded", function() {

    // Initialize WebAuthN registration logic.
    var webauthnEl = document.querySelector(".webauthn-data");
    if (webauthnEl) {
        registerCredential(JSON.parse(webauthnEl.dataset.webauthnRegistration).publicKey);
    }

});
