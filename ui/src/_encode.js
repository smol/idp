import { fromByteArray } from 'base64-js';

// Encode an ArrayBuffer into a base64 (url-safe) string.
export function bufferEncode(value) {
    return fromByteArray(value)
        .replace(/\+/g, "-")
        .replace(/\//g, "_")
        .replace(/=/g, "");
}

// Decode a base64 (url-safe) string.
export function bufferDecode(value) {
    if (value) {
        value = value.replace(/-/g, '+').replace(/_/g, '/');
    }
    return Uint8Array.from(atob(value), c => c.charCodeAt(0));
}
