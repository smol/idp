#!/bin/sh
#
# Simple JS / CSS project builder.
#
# Uses ESBuild, SASS and PurgeCSS to bundle and minify Javascript and
# CSS sources (in SOURCE_DIR), hash the entry points, pre-compress all
# output files.
#
# It also generates an "assets.go" file with a map of entry points
# that references their current hashes and SRI tags (see ui.go for the
# integration on the Go side). Entry points are any (S)CSS and JS
# files in SOURCE_DIR.

SOURCE_DIR="src"
OUTPUT_DIR="assets"

# PurgeCSS parameters.
TEMPLATES_GLOB="templates/**/*.html"

set -e

run() {
    echo "$*" >&2
    "$@"
}

inject_extension() {
    local path="$1"
    local ext="$2"
    echo "${path}" | sed -e "s,\(\.[^.]*\)\$,${ext}\\1,"
}

relpath() {
    local base="${1#/}/"
    local path="$2"
    echo "${path}" | sed -e "s,^${base},,"
}

purgecss_opts() {
    if [ -e purgecss.config.js ]; then
        echo "--config purgecss.config.js"
    fi
}

# Phase 0: ensure output directories exist
for dir in $(find "${SOURCE_DIR}" -type d -printf '%P\n'); do
    dir="${OUTPUT_DIR}/${dir}"
    test -d "${dir}" || mkdir -p "${dir}"
done

# Phase 1: preprocessing, run 'sass' on .scss files, and convert them
# to .css files. Ignore files starting with a '_' or '.' as those are
# considered modules to be included by other, top-level .scss files.
phase1_sources=$(find "${SOURCE_DIR}" -type f -print)
phase2_sources=
phase3_sources=
tmp_outputs=
for src in ${phase1_sources}; do
    case "$(basename "${src}")" in
        _*|.*)
            ;;
        *.scss)
            css="${src%.scss}.css"
            run sass -I node_modules --no-source-map "${src}" "${css}"
            phase2_sources="${phase2_sources} ${css}"
            tmp_outputs="${tmp_outputs} ${css}"
            ;;
        *.js|*.css)
            phase2_sources="${phase2_sources} ${src}"
            ;;
        *)
            # No processing, skip directly to phase3.
            phase3_sources="${phase3_sources} ${src}"
            ;;
    esac
done

# Phase 2: CSS bundle generation. Invoke 'esbuild' in a single pass
# with all sources, run 'purgecss' on each .css file separately.
run esbuild --bundle --minify --tree-shaking --outdir=${OUTPUT_DIR} ${phase2_sources}
for src in ${phase2_sources}; do
    asset_path="${OUTPUT_DIR}/$(relpath "${SOURCE_DIR}" "${src}")"
    case "${src}" in
        *.css)
            # Replace the .css in-place using .min.css as a temporary file.
            tmp=$(inject_extension "${asset_path}" .min)
            run purgecss --content "${TEMPLATES_GLOB}" $(purgecss_opts) --css "${asset_path}" --output "${tmp}"
            mv -f "${tmp}" "${asset_path}"
            ;;
    esac
    phase3_sources="${phase3_sources} ${asset_path}"
done

# Phase 3: post-processing. Ensure all files are copied to the
# destination directory, using hashed filenames for top-level
# processed files. Create the assets.go file containing asset hashes
# for SRI. Pre-compress known file types.
exec 4> assets.go
cat >&4 <<EOF
package ui

import "${GO_PACKAGE}/internal/web"

var assetInfo = map[string]web.AssetInfo{
EOF

gen_sri_tag() {
    echo -n "sha384-"
    shasum -b -a 384 "$1" | awk '{ print $1 }' | xxd -r -p | base64
}

hash_file() {
    local real_path="$1"
    local path="$2"
    local hash=$(md5sum "${real_path}" | cut -c-8)
    local sri_tag=$(gen_sri_tag "${real_path}")
    local outpath=$(inject_extension "${path}" ".${hash}")
    local real_outpath="${OUTPUT_DIR}/${outpath}"
    mv -f "${real_path}" "${real_outpath}"
    echo "${path} -> ${outpath}" >&2
    echo "\t\"${path}\": web.AssetInfo{Path: \"${outpath}\", SRI: \"${sri_tag}\"}," >&4
    echo "${real_outpath}"
}

for src in ${phase3_sources}; do
    echo "processing source ${src}" >&2

    # Copy to output dir if necessary
    case "${src}" in
        ${SOURCE_DIR}/*)
            asset_path="${OUTPUT_DIR}/$(relpath "${SOURCE_DIR}" "${src}")"
            cp -v -f "${src}" "${asset_path}"
            src="${asset_path}"
            ;;
    esac

    # Hash the file if necessary.
    case "${src}" in
        *.css|*.js)
            src=$(hash_file "${src}" "$(relpath "${OUTPUT_DIR}" "${src}")")
            ;;
    esac

    # Compress the asset.
    case "${src}" in
        *.css|*.js|*.svg)
            gzip --keep -9f "${src}"
            brotli --keep -9f "${src}"
            ;;
    esac
done

echo "}" >&4

# Cleanup
if [ -n "${tmp_outputs}" ]; then
    rm -f ${tmp_outputs}
fi

echo " [!] Done" >&2

exit 0
