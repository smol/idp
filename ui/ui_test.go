package ui

import (
	"testing"

	"git.autistici.org/smol/idp/internal/web"
)

func TestTemplates(t *testing.T) {
	tpl := MustParseTemplates(&web.Config{}).Lookup("login.html")
	if tpl == nil {
		t.Fatalf("could not find login.html template")
	}
}
