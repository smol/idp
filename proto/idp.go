package proto

import (
	"encoding/hex"
	"errors"
	"fmt"
	"strings"

	"github.com/go-webauthn/webauthn/webauthn"
)

func NewPrimaryPasswordAuthenticatorID() *AuthenticatorID {
	return &AuthenticatorID{
		Type: AuthenticatorID_PRIMARY_PASSWORD,
	}
}

func NewOTPAuthenticatorID() *AuthenticatorID {
	return &AuthenticatorID{
		Type: AuthenticatorID_OTP,
	}
}

func NewAppSpecificPasswordAuthenticatorID(aspID string) *AuthenticatorID {
	return &AuthenticatorID{
		Type: AuthenticatorID_APP_SPECIFIC_PASSWORD,
		Id:   aspID,
	}
}

func NewWebAuthnAuthenticatorID(cred *webauthn.Credential) *AuthenticatorID {
	return &AuthenticatorID{
		Type: AuthenticatorID_WEBAUTHN,
		Id:   string(cred.ID),
	}
}

func NewRecoveryAuthenticatorID(token *RecoveryToken) *AuthenticatorID {
	return &AuthenticatorID{
		Type: AuthenticatorID_RECOVERY,
		Id:   token.Type.String(),
	}
}

func ParseAuthenticatorID(s string) (*AuthenticatorID, error) {
	var aid AuthenticatorID

	if n := strings.IndexByte(s, '/'); n >= 0 {
		aid.Id = s[n+1:]
		s = s[:n]
	}
	typ, ok := AuthenticatorID_Type_value[s]
	if !ok {
		return nil, errors.New("unknown authenticator_id type")
	}
	aid.Type = AuthenticatorID_Type(typ)

	return &aid, nil
}

func (a *AuthenticatorID) EncodeToString() string {
	if a.Id == "" {
		return a.Type.String()
	}
	return fmt.Sprintf("%s/%s", a.Type.String(), a.Id)
}

func (a *AuthenticatorID) Equal(b *AuthenticatorID) bool {
	return a.Type == b.Type && a.Id == b.Id
}

func (w *WebAuthnRegistration) ToCredential() (webauthn.Credential, error) {
	return webauthn.Credential{
		ID:        []byte(w.KeyHandle),
		PublicKey: []byte(w.PublicKey),
	}, nil
}

func (w *WebAuthnRegistration) HexKeyHandle() string {
	return hex.EncodeToString([]byte(w.KeyHandle))
}

func (u *User) GetAppSpecificPasswordByID(id string) *AppSpecificPassword {
	for _, asp := range u.AppSpecificPasswords {
		if asp.Id == id {
			return asp
		}
	}
	return nil
}

func (u *User) GetWebAuthnRegistrationByKeyHandle(handle string) *WebAuthnRegistration {
	for _, reg := range u.WebAuthnRegistrations {
		if reg.KeyHandle == handle {
			return reg
		}
	}
	return nil
}

func (u *User) GetRecoveryToken(typ RecoveryToken_Type, id string) *RecoveryToken {
	for _, rt := range u.RecoveryTokens {
		if rt.Type == typ && rt.Id == id {
			return rt
		}
	}
	return nil
}

// Methods to make User compatible with the webauthn.User interface.

func (u *User) WebAuthnID() []byte          { return []byte(u.Id) }
func (u *User) WebAuthnName() string        { return u.Info.Name }
func (u *User) WebAuthnDisplayName() string { return u.Info.Name }
func (u *User) WebAuthnIcon() string        { return "" }

func (u *User) WebAuthnCredentials() []webauthn.Credential {
	out := make([]webauthn.Credential, 0, len(u.WebAuthnRegistrations))
	for _, r := range u.WebAuthnRegistrations {
		cred, err := r.ToCredential()
		if err != nil {
			panic(err)
		}
		out = append(out, cred)
	}
	return out
}
