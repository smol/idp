package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"github.com/google/subcommands"

	"git.autistici.org/smol/idp/userdb"
)

type pwhashCommand struct{}

func (c *pwhashCommand) Name() string     { return "pwhash" }
func (c *pwhashCommand) Synopsis() string { return "hash a password" }
func (c *pwhashCommand) Usage() string {
	return `pwhash <PASSWORD>:
        Hash a password and print the result to stdout. Useful for debugging
        and manual configuration of the 'file' backend.

`
}

func (c *pwhashCommand) SetFlags(f *flag.FlagSet) {}

func (c *pwhashCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() != 1 {
		log.Printf("error: wrong number of arguments")
		return subcommands.ExitUsageError
	}

	enc, err := userdb.PasswordHasher.Hash(f.Arg(0))
	if err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	fmt.Println(enc)

	return subcommands.ExitSuccess
}

func init() {
	subcommands.Register(&pwhashCommand{}, "debug")
}
