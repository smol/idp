package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"git.autistici.org/smol/idp/audit"
	apb "git.autistici.org/smol/idp/audit/proto"
	"github.com/google/subcommands"
)

type auditConfig struct {
	Config audit.Config `yaml:",inline"`
}

type auditCommand struct {
	*configCommandBase
	*serverCommandBase
}

func newAuditCommand() *auditCommand {
	return &auditCommand{
		configCommandBase: newConfigCommand("/etc/idp/audit.yml"),
		serverCommandBase: newServerCommand(":4150", ":4151"),
	}
}

func (c *auditCommand) Name() string     { return "audit" }
func (c *auditCommand) Synopsis() string { return "run the audit server" }
func (c *auditCommand) Usage() string {
	return fmt.Sprintf(`audit [<flags>]

Run the audit server. This is a GRPC service that collects user activity and
maintains a subset of it for analytical purposes.

Run "%s audit --help-config" for documentation on the configuration
file syntax and structure.

Command-line flags can also be set via environment variables with matching
uppercase names and a "IDP_" prefix, e.g. "IDP_ADDR" etc.

`, subcommands.DefaultCommander.Name())
}

func (c *auditCommand) SetFlags(f *flag.FlagSet) {
	c.configCommandBase.SetFlags(f)
	c.serverCommandBase.SetFlags(f)
	defaultsFromEnv(f)
}

func (c *auditCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	var config auditConfig

	if c.maybeShowConfigHelp(c.Name(), &config) {
		return subcommands.ExitSuccess
	}

	if err := c.Load(&config); err != nil {
		log.Printf("configuration error: %v", err)
		return subcommands.ExitUsageError
	}

	auditSrv, err := audit.New(&config.Config)
	if err != nil {
		log.Printf("initialization error: %v", err)
		return subcommands.ExitFailure
	}
	defer auditSrv.Close()

	server, err := c.makeGRPCServer()
	if err != nil {
		log.Printf("error creating GRPC server: %v", err)
		return subcommands.ExitFailure
	}
	apb.RegisterAuditServer(server, auditSrv)

	return c.runGRPCServer(ctx, server)
}

func init() {
	subcommands.Register(newAuditCommand(), "")
}
