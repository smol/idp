package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"

	accountserver "git.autistici.org/smol/idp/account"
	accpb "git.autistici.org/smol/idp/account/proto"
	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/netutil"
	"git.autistici.org/smol/idp/login"
	"git.autistici.org/smol/idp/userdb"
	"github.com/google/subcommands"
)

type accountConfig struct {
	UserDB userdb.Spec `yaml:"userdb" doc:"user database backend"`

	Audit *netutil.GRPCEndpointSpec `yaml:"audit" doc:"configure the audit backend"`
}

type accountCommand struct {
	*configCommandBase
	*serverCommandBase
}

func newAccountCommand() *accountCommand {
	return &accountCommand{
		configCommandBase: newConfigCommand("/etc/idp/account.yml"),
		serverCommandBase: newServerCommand(":4151", ":4152"),
	}
}

func (c *accountCommand) Name() string     { return "account" }
func (c *accountCommand) Synopsis() string { return "run the account server" }
func (c *accountCommand) Usage() string {
	return fmt.Sprintf(`account [<flags>]

Run the account server. It is a GRPC service, exporting access to the
underlying user database.

Run "%s account --help-config" for documentation on the configuration
file syntax and structure.

Command-line flags can also be set via environment variables with matching
uppercase names and a "IDP_" prefix, e.g. "IDP_ADDR" etc.

`, subcommands.DefaultCommander.Name())
}

func (c *accountCommand) SetFlags(f *flag.FlagSet) {
	c.configCommandBase.SetFlags(f)
	c.serverCommandBase.SetFlags(f)
	defaultsFromEnv(f)
}

func (c *accountCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	var config accountConfig

	if c.maybeShowConfigHelp(c.Name(), &config) {
		return subcommands.ExitSuccess
	}

	if err := c.Load(&config); err != nil {
		log.Printf("configuration error: %v", err)
		return subcommands.ExitUsageError
	}

	// Audit server client.
	audConn, err := config.Audit.Dial()
	if err != nil {
		log.Printf("couldn't set up connection with authentication server: %v", err)
		return subcommands.ExitFailure
	}
	defer audConn.Close()
	aud := apb.NewAuditClient(audConn)

	// Initialize userdb, and wrap it with an audit wrapper.
	udb, err := userdb.New(&config.UserDB, c.configPath)
	if err != nil {
		log.Printf("error initializing database: %v", err)
		return subcommands.ExitFailure
	}
	udb = userdb.WithAudit(udb, aud, func(ctx context.Context) (*authpb.Log, error) {
		// Log the authenticated user as the operator.
		if auth, ok := login.GetAuth(ctx); ok {
			return auth.MakeAuthLog(), nil
		}
		return nil, errors.New("unauthenticated")
	})
	defer udb.Close()

	accountSrv := accountserver.New(udb)

	server, err := c.makeGRPCServer()
	if err != nil {
		log.Printf("error creating GRPC server: %v", err)
		return subcommands.ExitFailure
	}
	accpb.RegisterAccountServer(server, accountSrv)

	return c.runGRPCServer(ctx, server)
}

func init() {
	subcommands.Register(newAccountCommand(), "")
}
