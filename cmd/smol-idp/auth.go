package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"path/filepath"

	"git.autistici.org/smol/idp"
	apb "git.autistici.org/smol/idp/audit/proto"
	"git.autistici.org/smol/idp/authn"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/netutil"
	"git.autistici.org/smol/idp/userdb"
	"github.com/google/subcommands"
)

type authConfig struct {
	UserDB userdb.Spec `yaml:"userdb" doc:"user database backend"`

	Features idp.Features `yaml:"features"`

	Audit *netutil.GRPCEndpointSpec `yaml:"audit" doc:"configure the audit backend"`

	Config authn.Config `yaml:",inline"`
}

type authCommand struct {
	*configCommandBase
	*serverCommandBase
}

func newAuthCommand() *authCommand {
	return &authCommand{
		configCommandBase: newConfigCommand("/etc/idp/auth.yml"),
		serverCommandBase: newServerCommand(":4141", ":4142"),
	}
}

func (c *authCommand) Name() string     { return "auth" }
func (c *authCommand) Synopsis() string { return "run the authentication server" }
func (c *authCommand) Usage() string {
	return fmt.Sprintf(`auth [<flags>]

Run the authentication server. It is a GRPC service, responsible for
authenticating users according to configurable rules, controlling all access
in a single stateless service.

The server needs access to the user database, and to a short-term storage
service (currently Memcache) for anti-replay and rate-limiting
functionality.

Run "%s auth --help-config" for documentation on the configuration
file syntax and structure.

Command-line flags can also be set via environment variables with matching
uppercase names and a "IDP_" prefix, e.g. "IDP_ADDR" etc.

`, subcommands.DefaultCommander.Name())
}

func (c *authCommand) SetFlags(f *flag.FlagSet) {
	c.configCommandBase.SetFlags(f)
	c.serverCommandBase.SetFlags(f)
	defaultsFromEnv(f)
}

func (c *authCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	var config authConfig

	if c.maybeShowConfigHelp(c.Name(), &config) {
		return subcommands.ExitSuccess
	}

	if err := c.Load(&config); err != nil {
		log.Printf("configuration error: %v", err)
		return subcommands.ExitUsageError
	}

	// Audit server client.
	audConn, err := config.Audit.Dial()
	if err != nil {
		log.Printf("couldn't set up connection with authentication server: %v", err)
		return subcommands.ExitFailure
	}
	defer audConn.Close()
	aud := apb.NewAuditClient(audConn)

	udb, err := userdb.New(&config.UserDB, c.configPath)
	if err != nil {
		log.Printf("error initializing database: %v", err)
		return subcommands.ExitFailure
	}

	authSrv, err := authn.New(udb, &config.Config, &config.Features, aud, filepath.Dir(c.configPath))
	if err != nil {
		log.Printf("initialization error: %v", err)
		return subcommands.ExitFailure
	}

	server, err := c.makeGRPCServer()
	if err != nil {
		log.Printf("error creating GRPC server: %v", err)
		return subcommands.ExitFailure
	}
	authpb.RegisterAuthNServer(server, authSrv)

	return c.runGRPCServer(ctx, server)
}

func init() {
	subcommands.Register(newAuthCommand(), "")
}
