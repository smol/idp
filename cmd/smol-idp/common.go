package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"git.autistici.org/smol/idp/internal/loaders"
	"git.autistici.org/smol/idp/internal/netutil"
	"git.autistici.org/smol/idp/internal/yamlschema"
	"github.com/google/subcommands"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"

	_ "git.autistici.org/smol/idp/userdb/backend/all"
)

const envFlagPrefix = "IDP_"

// Read flag defaults from the environment.
func defaultsFromEnv(ff *flag.FlagSet) {
	ff.VisitAll(func(f *flag.Flag) {
		envName := envFlagPrefix + strings.ToUpper(strings.ReplaceAll(f.Name, "-", "_"))
		if s := os.Getenv(envName); s != "" {
			if err := f.Value.Set(s); err != nil {
				log.Printf("error setting flag %s from env variable %s: %v", f.Name, envName, err)
				return
			}
			f.DefValue = s
		}
	})
}

// Helper type for commands that need a configuration file.
type configCommandBase struct {
	configPath string
	configHelp bool
}

func newConfigCommand(defaultConfigPath string) *configCommandBase {
	return &configCommandBase{
		configPath: defaultConfigPath,
	}
}

func (c *configCommandBase) SetFlags(f *flag.FlagSet) {
	f.BoolVar(&c.configHelp, "help-config", false, "dump configuration schema")
	f.StringVar(&c.configPath, "config", c.configPath, "configuration file")
}

func (c *configCommandBase) Load(config any) error {
	return loaders.LoadYAML(c.configPath, config)
}

func (c *configCommandBase) maybeShowConfigHelp(name string, obj any) bool {
	if c.configHelp {
		fmt.Printf("Schema for the \"%s %s\" configuration file\n",
			subcommands.DefaultCommander.Name(), name)
		yamlschema.DumpSchema(os.Stdout, obj, envFlagPrefix)
		fmt.Printf("\n")
	}
	return c.configHelp
}

// Helper type for commands that have a SSL configuration.
type sslCommandBase struct {
	sslCert string
	sslKey  string
	sslCA   string
}

func (c *sslCommandBase) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.sslCert, "ssl-cert", "", "SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", "", "SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", "", "SSL CA `path`")
}

// Helper type for server-like (HTTP/GRPC) commands.
type serverCommandBase struct {
	*sslCommandBase

	addr        string
	metricsAddr string
	aclSpec     string
}

func newServerCommand(defaultAddr, defaultMetricsAddr string) *serverCommandBase {
	return &serverCommandBase{
		sslCommandBase: new(sslCommandBase),
		addr:           defaultAddr,
		metricsAddr:    defaultMetricsAddr,
	}
}

func (c *serverCommandBase) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.addr, "addr", c.addr, "address to listen on")
	f.StringVar(&c.metricsAddr, "metrics-addr", c.metricsAddr, "address for HTTP instrumentation server")
	f.StringVar(&c.aclSpec, "acl", "", "ACL policy (JSON-encoded)")
	c.sslCommandBase.SetFlags(f)
}

func (c *serverCommandBase) runGenericServer(ctx context.Context, f func(context.Context) error) subcommands.ExitStatus {
	g, ctx := errgroup.WithContext(ctx)

	if c.metricsAddr != "" {
		g.Go(func() error {
			return c.runMetricsHTTPServer(ctx)
		})
	}

	g.Go(func() error {
		return f(ctx)
	})

	if err := g.Wait(); err != nil {
		log.Printf("fatal error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

func (c *serverCommandBase) runMetricsHTTPServer(ctx context.Context) error {
	h := http.NewServeMux()
	h.HandleFunc("/health", func(w http.ResponseWriter, req *http.Request) {
		io.WriteString(w, "ok") //nolint:errcheck
	})
	h.Handle("/metrics", promhttp.Handler())
	server := newHTTPServer(c.metricsAddr, h)
	log.Printf("starting metrics HTTP server on %s", c.metricsAddr)
	return netutil.RunHTTPServerWithContext(ctx, server)
}

func (c *serverCommandBase) runHTTPServer(ctx context.Context, h http.Handler) subcommands.ExitStatus {
	return c.runGenericServer(ctx, func(ctx context.Context) error {
		server := newHTTPServer(c.addr, h)
		proto := "HTTP"
		if c.sslCert != "" {
			tlsConfig, err := netutil.ServerTLSConfig(c.sslCert, c.sslKey, c.sslCA)
			if err != nil {
				return err
			}
			server.TLSConfig = tlsConfig
			proto = "HTTPS"
		}

		log.Printf("starting %s server on %s", proto, c.addr)
		return netutil.RunHTTPServerWithContext(ctx, server)
	})
}

func (c *serverCommandBase) runHTTPServerWithTimeout(ctx context.Context, h http.Handler, timeout time.Duration) subcommands.ExitStatus {
	if timeout > 0 {
		h = http.TimeoutHandler(h, timeout, "Request timed out")
	}
	return c.runHTTPServer(ctx, h)
}

func (c *serverCommandBase) runGRPCServer(ctx context.Context, server *grpc.Server) subcommands.ExitStatus {
	return c.runGenericServer(ctx, func(ctx context.Context) error {
		log.Printf("starting GRPC server on %s", c.addr)
		return netutil.RunGRPCServerWithContext(ctx, server, c.addr)
	})
}

func (c *serverCommandBase) makeGRPCServer() (*grpc.Server, error) {
	spec := netutil.GRPCServerSpec{
		ACL: c.aclSpec,
		TLSParams: netutil.TLSParams{
			SSLCert: c.sslCert,
			SSLKey:  c.sslKey,
			SSLCA:   c.sslCA,
		},
	}
	opts, srvMetrics, err := spec.ServerOptions()
	if err != nil {
		return nil, err
	}
	server := grpc.NewServer(opts...)
	srvMetrics.InitializeMetrics(server)
	return server, nil
}

func newHTTPServer(addr string, h http.Handler) *http.Server {
	return &http.Server{
		Addr:              addr,
		Handler:           h,
		ReadTimeout:       300 * time.Second,
		ReadHeaderTimeout: 5 * time.Second,
		IdleTimeout:       300 * time.Second,
	}
}
