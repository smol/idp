package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"git.autistici.org/smol/idp"
	apb "git.autistici.org/smol/idp/audit/proto"
	"git.autistici.org/smol/idp/authn"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/netutil"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/internal/yamlschema"
	"git.autistici.org/smol/idp/login"
	"git.autistici.org/smol/idp/mgmt"
	"git.autistici.org/smol/idp/oidc"
	"git.autistici.org/smol/idp/oidc/storage"
	"git.autistici.org/smol/idp/ui"
	"git.autistici.org/smol/idp/userdb"
	"github.com/google/subcommands"
	"github.com/gorilla/handlers"
)

type bundleConfig struct {
	// Common.
	UserDB userdb.Spec `yaml:"userdb" doc:"user database backend"`

	Features idp.Features `yaml:"features"`

	Web web.Config `yaml:"ui"`

	Audit *netutil.GRPCEndpointSpec `yaml:"audit" doc:"configure the audit backend"`

	// Auth server.
	AuthN authn.Config `yaml:"authn"`

	// OIDC.
	OIDC struct {
		DbPath  string                     `yaml:"db_path" doc:"path to the session database file"`
		Config  oidc.Config                `yaml:",inline"`
		Clients map[string]*storage.Client `yaml:"clients"`
	} `yaml:"oidc"`

	// Login
	Login login.Config `yaml:"login"`

	HTTP struct {
		netutil.TLSParams
		Timeout time.Duration `yaml:"request_timeout" doc:"request timeout"`
	} `yaml:"http"`

	// Mgmt app.
	Mgmt mgmt.Config `yaml:"mgmt"`
}

type bundleCommand struct {
	*configCommandBase
	*serverCommandBase

	debug bool
}

func newBundleCommand() *bundleCommand {
	return &bundleCommand{
		configCommandBase: newConfigCommand("/etc/idp/bundle.yml"),
		serverCommandBase: newServerCommand(":3131", ":3132"),
	}
}

func (c *bundleCommand) Name() string     { return "bundle" }
func (c *bundleCommand) Synopsis() string { return "run an all-in-one server" }
func (c *bundleCommand) Usage() string {
	return fmt.Sprintf(`bundle [<flags>]

Run all services (except for audit) in a single process.

Run "%s bundle --help-config" for documentation on the configuration
file syntax and structure.

Command-line flags can also be set via environment variables with matching
uppercase names and a "IDP_" prefix, e.g. "IDP_ADDR" etc.

`, subcommands.DefaultCommander.Name())
}

func (c *bundleCommand) SetFlags(f *flag.FlagSet) {
	c.configCommandBase.SetFlags(f)
	c.serverCommandBase.SetFlags(f)
	f.BoolVar(&c.debug, "debug", false, "enable debugging output")
	defaultsFromEnv(f)
}

func (c *bundleCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	var config bundleConfig
	config.OIDC.DbPath = "oidc.db"
	config.HTTP.Timeout = 5 * time.Second
	yamlschema.FillFromEnvironment(&config, envFlagPrefix)

	if c.maybeShowConfigHelp(c.Name(), config) {
		return subcommands.ExitSuccess
	}

	if err := c.Load(&config); err != nil {
		log.Printf("error loading config: %v", err)
		return subcommands.ExitUsageError
	}

	var aud apb.AuditClient
	if config.Audit != nil {
		// Audit server client.
		audConn, err := config.Audit.Dial()
		if err != nil {
			log.Printf("couldn't set up connection with audit server: %v", err)
			return subcommands.ExitFailure
		}
		defer audConn.Close()
		aud = apb.NewAuditClient(audConn)
	}

	// Initialize userdb, and wrap it with an audit wrapper.
	udb, err := userdb.New(&config.UserDB, c.configPath)
	if err != nil {
		log.Printf("error initializing database: %v", err)
		return subcommands.ExitFailure
	}
	defer udb.Close()

	if aud != nil {
		udb = userdb.WithAudit(udb, aud, func(ctx context.Context) (*authpb.Log, error) {
			// Log the authenticated user as the operator.
			if auth, ok := login.GetAuth(ctx); ok {
				return auth.MakeAuthLog(), nil
			}
			return nil, errors.New("unauthenticated")
		})
	}

	// Auth server.
	authSrv, err := authn.New(udb, &config.AuthN, &config.Features, aud, filepath.Dir(c.configPath))
	if err != nil {
		log.Printf("initialization error: %v", err)
		return subcommands.ExitFailure
	}
	authClient := login.NewAuthClient(authSrv.LocalStub())

	// OIDC storage.
	oidcStorageImpl, err := storage.NewStorageWithClients(config.OIDC.DbPath, udb, config.OIDC.Clients)
	if err != nil {
		log.Printf("failed to initialize storage: %v", err)
		return subcommands.ExitFailure
	}
	defer oidcStorageImpl.Close()
	var oidcStorage oidc.Storage = oidcStorageImpl
	if c.debug {
		oidcStorage = storage.Debug(oidcStorageImpl)
	}

	tpl := ui.MustParseTemplates(&config.Web)

	// Create all applications (login, oidc, mgmt) and bundle them
	// together into a single http router.
	loginApp, err := login.NewApp(&config.Login, &config.Features, authClient, tpl)
	if err != nil {
		log.Printf("failed to initialize login server: %v", err)
		return subcommands.ExitFailure
	}

	oidcApp, err := oidc.NewApp(&config.OIDC.Config, &config.Features, oidcStorage, loginApp, tpl)
	if err != nil {
		log.Printf("failed to initialize OIDC server: %v", err)
		return subcommands.ExitFailure
	}

	mgmtApp, err := mgmt.NewApp(udb, &config.Mgmt, &config.Features, authClient, tpl)
	if err != nil {
		log.Printf("failed to initialize user management server: %v", err)
		return subcommands.ExitFailure
	}

	mux := http.NewServeMux()
	mux.Handle("/account/", loginApp.WithAuth(mgmtApp))
	// Install the oidc app with a redirect on the root.
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path == "" || req.URL.Path == "/" {
			http.Redirect(w, req, "/account/overview", http.StatusFound)
			return
		}
		oidcApp.ServeHTTP(w, req)
	})

	h := ui.WithAssets(mux)
	h = handlers.LoggingHandler(os.Stdout, h)

	return c.runHTTPServerWithTimeout(ctx, h, config.HTTP.Timeout)
}

func init() {
	subcommands.Register(newBundleCommand(), "")
}
