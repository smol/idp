package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/subcommands"
)

func explain(w io.Writer, cmd subcommands.Command) {
	fmt.Fprintf(w, "Usage: %s %s", subcommands.DefaultCommander.Name(), cmd.Usage())
	subflags := flag.NewFlagSet(cmd.Name(), flag.PanicOnError)
	subflags.SetOutput(w)
	cmd.SetFlags(subflags)
	fmt.Fprintf(w, "Known flags:\n\n")
	subflags.PrintDefaults()
	fmt.Fprintf(w, "\n")
}

func init() {
	subcommands.Register(subcommands.HelpCommand(), "help")
	subcommands.Register(subcommands.CommandsCommand(), "help")

	subcommands.DefaultCommander.ExplainCommand = explain
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("termination signal received, exiting...")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	os.Exit(int(subcommands.Execute(ctx)))
}
