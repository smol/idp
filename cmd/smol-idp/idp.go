package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"git.autistici.org/smol/idp"
	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/netutil"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/internal/yamlschema"
	"git.autistici.org/smol/idp/login"
	"git.autistici.org/smol/idp/mgmt"
	"git.autistici.org/smol/idp/oidc"
	"git.autistici.org/smol/idp/oidc/storage"
	"git.autistici.org/smol/idp/ui"
	"git.autistici.org/smol/idp/userdb"
	"github.com/google/subcommands"
	"github.com/gorilla/handlers"
)

type idpConfig struct {
	UserDB userdb.Spec `yaml:"userdb" doc:"user database backend"`

	Features idp.Features `yaml:"features"`

	Auth *netutil.GRPCEndpointSpec `yaml:"auth"`

	Audit *netutil.GRPCEndpointSpec `yaml:"audit" doc:"configure the audit backend"`

	Web web.Config `yaml:"ui"`

	HTTP struct {
		*netutil.TLSParams
		Timeout time.Duration `yaml:"request_timeout" doc:"request timeout"`
	} `yaml:"http"`

	OIDC struct {
		DbPath  string                     `yaml:"db_path" doc:"path to the session database file"`
		Config  oidc.Config                `yaml:",inline"`
		Clients map[string]*storage.Client `yaml:"clients"`
	} `yaml:"oidc"`

	Login login.Config `yaml:"login"`

	Mgmt mgmt.Config `yaml:"mgmt"`
}

type idpCommand struct {
	*configCommandBase
	*serverCommandBase
}

func newIDPCommand() *idpCommand {
	return &idpCommand{
		configCommandBase: newConfigCommand("/etc/idp/idp.yml"),
		serverCommandBase: newServerCommand(":3131", ":3132"),
	}
}

func (c *idpCommand) Name() string     { return "idp" }
func (c *idpCommand) Synopsis() string { return "run the IDP server" }
func (c *idpCommand) Usage() string {
	return fmt.Sprintf(`idp [<flags>]

Run the IDP HTTP server. This component handles the UI for the login and
OIDC workflows, as well as the account management interface.

Run "%s idp --help-config" for documentation on the configuration
file syntax and structure.

Command-line flags can also be set via environment variables with matching
uppercase names and a "IDP_" prefix, e.g. "IDP_ADDR" etc.

`, subcommands.DefaultCommander.Name())
}

func (c *idpCommand) SetFlags(f *flag.FlagSet) {
	c.configCommandBase.SetFlags(f)
	c.serverCommandBase.SetFlags(f)
	defaultsFromEnv(f)
}

func (c *idpCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}

	var config idpConfig
	config.OIDC.DbPath = "oidc.db"
	config.HTTP.Timeout = 5 * time.Second
	yamlschema.FillFromEnvironment(&config, envFlagPrefix)

	if c.maybeShowConfigHelp(c.Name(), config) {
		return subcommands.ExitSuccess
	}

	if err := c.Load(&config); err != nil {
		log.Printf("error loading config: %v", err)
		return subcommands.ExitUsageError
	}

	// Authentication GRPC client.
	authConn, err := config.Auth.Dial()
	if err != nil {
		log.Printf("couldn't set up connection with authentication server: %v", err)
		return subcommands.ExitFailure
	}
	defer authConn.Close()
	authClient := login.NewAuthClient(authpb.NewAuthNClient(authConn))

	// Audit server client.
	audConn, err := config.Audit.Dial()
	if err != nil {
		log.Printf("couldn't set up connection with audit server: %v", err)
		return subcommands.ExitFailure
	}
	defer audConn.Close()
	aud := apb.NewAuditClient(audConn)

	// Initialize userdb, and wrap it with an audit wrapper.
	udb, err := userdb.New(&config.UserDB, c.configPath)
	if err != nil {
		log.Printf("error initializing database: %v", err)
		return subcommands.ExitFailure
	}
	udb = userdb.WithAudit(udb, aud, func(ctx context.Context) (*authpb.Log, error) {
		// Log the authenticated user as the operator.
		if auth, ok := login.GetAuth(ctx); ok {
			return auth.MakeAuthLog(), nil
		}
		return nil, errors.New("unauthenticated")
	})
	defer udb.Close()

	// OIDC storage.
	storage, err := storage.NewStorageWithClients(config.OIDC.DbPath, udb, config.OIDC.Clients)
	if err != nil {
		log.Printf("failed to initialize storage: %v", err)
		return subcommands.ExitFailure
	}
	defer storage.Close()

	tpl := ui.MustParseTemplates(&config.Web)

	// Create all applications (login, oidc, mgmt) and bundle them
	// together into a single http router.
	loginApp, err := login.NewApp(&config.Login, &config.Features, authClient, tpl)
	if err != nil {
		log.Printf("failed to initialize login server: %v", err)
		return subcommands.ExitFailure
	}

	oidcApp, err := oidc.NewApp(&config.OIDC.Config, &config.Features, storage, loginApp, tpl)
	if err != nil {
		log.Printf("failed to initialize OIDC server: %v", err)
		return subcommands.ExitFailure
	}

	mgmtApp, err := mgmt.NewApp(udb, &config.Mgmt, &config.Features, authClient, tpl)
	if err != nil {
		log.Printf("failed to initialize user management server: %v", err)
		return subcommands.ExitFailure
	}

	mux := http.NewServeMux()
	mux.Handle("/account/", loginApp.WithAuth(mgmtApp))
	mux.Handle("/", oidcApp)

	h := ui.WithAssets(mux)
	h = handlers.LoggingHandler(os.Stdout, h)

	return c.runHTTPServerWithTimeout(ctx, h, config.HTTP.Timeout)
}

func init() {
	subcommands.Register(newIDPCommand(), "")
}
