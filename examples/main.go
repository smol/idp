package main

import (
	"crypto/rsa"
	"crypto/sha256"
	"fmt"
	"log/slog"
	"math/big"
	"net/http"
	"os"

	"git.autistici.org/smol/idp"
	"git.autistici.org/smol/idp/internal/cryptutil"
	"git.autistici.org/smol/idp/internal/testutil"
	"git.autistici.org/smol/idp/internal/web"
	"git.autistici.org/smol/idp/login"
	"git.autistici.org/smol/idp/oidc"
	"git.autistici.org/smol/idp/oidc/storage"
	"git.autistici.org/smol/idp/ui"
	"github.com/gorilla/securecookie"
)

var fixtures = `
INSERT INTO users (id, username, email, password) VALUES (1, 'test', 'testuser@example.com', 'foo');
INSERT INTO asps (uid, id, service, password, comment) VALUES (1, 'abcdef', 'service1', 'foo', 'comment1');
INSERT INTO asps (uid, id, service, password, comment) VALUES (1, 'zxcvbn', 'service2', 'foo', 'comment2');
INSERT INTO users (id, username, email, password) VALUES (2, 'test-otp', 'testuser-otp@example.com', 'foo');
INSERT INTO users (id, username, email, password) VALUES (3, 'test-webauthn', 'testuser-webauthn@example.com', 'foo');
`

// serviceKey1 is a public key which will be used for the JWT Profile Authorization Grant
// the corresponding private key is in the service-key1.json (for demonstration purposes)
var serviceKey1 = &rsa.PublicKey{
	N: func() *big.Int {
		n, _ := new(big.Int).SetString("00f6d44fb5f34ac2033a75e73cb65ff24e6181edc58845e75a560ac21378284977bb055b1a75b714874e2a2641806205681c09abec76efd52cf40984edcf4c8ca09717355d11ac338f280d3e4c905b00543bdb8ee5a417496cb50cb0e29afc5a0d0471fd5a2fa625bd5281f61e6b02067d4fe7a5349eeae6d6a4300bcd86eef331", 16)
		return n
	}(),
	E: 65537,
}

func main() {
	//we will run on :9998
	port := "9998"
	//which gives us the issuer: http://localhost:9998/
	issuer := fmt.Sprintf("http://localhost:%s/", port)

	logger := slog.New(
		slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
			AddSource: true,
			Level:     slog.LevelDebug,
		}),
	)

	udb, cleanup, err := testutil.CreateTestDB(fixtures, "")
	if err != nil {
		logger.Error("failed to init database", "error", err)
		return
	}
	defer cleanup()

	// the OpenIDProvider interface needs a Storage interface handling various checks and state manipulations
	// this might be the layer for accessing your database
	// in this example it will be handled in-memory
	deviceClient := storage.DeviceClient("device", "")
	deviceClient.ServiceKeys = map[string]cryptutil.RSAKey{
		"key1": cryptutil.RSAKey{PublicKey: serviceKey1},
	}
	storage, err := storage.NewStorageWithClients("oidc.db", udb, map[string]*storage.Client{
		"web":    storage.WebClient("web", "secret", "http://localhost:9999/auth/callback"),
		"api":    storage.WebClient("api", "secret"),
		"device": deviceClient,
	})
	if err != nil {
		logger.Error("failed to initialize storage", "error", err)
		return
	}

	tpl := ui.MustParseTemplates(new(web.Config))

	key := sha256.Sum256([]byte("test"))
	config := &oidc.Config{
		Issuer:        issuer,
		EncryptionKey: cryptutil.Binary(key[:]),
		CSRFKey:       securecookie.GenerateRandomKey(32),
	}
	loginConfig := &login.Config{
		AuthService: "login",
	}
	features := &idp.Features{
		Insecure: true,
	}

	loginApp, err := login.NewApp(loginConfig, features, new(testutil.FakeAuthClient), tpl)
	if err != nil {
		logger.Error("login app setup", "error", err)
		os.Exit(1)
	}
	router, err := oidc.NewApp(config, features, storage, loginApp, tpl)
	if err != nil {
		logger.Error("setup", "error", err)
		os.Exit(1)
	}

	server := &http.Server{
		Addr:    ":" + port,
		Handler: router,
	}
	logger.Info("server listening, press ctrl+c to stop", "addr", fmt.Sprintf("http://localhost:%s/", port))
	err = server.ListenAndServe()
	if err != http.ErrServerClosed {
		logger.Error("server terminated", "error", err)
		os.Exit(1)
	}
}
