package authn

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"time"

	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/random"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
	"github.com/go-webauthn/webauthn/protocol"
	"github.com/go-webauthn/webauthn/webauthn"
	"github.com/pquerna/otp/totp"
)

// Authentication handler for interactive services supporting 2FA. Can
// switch between different second factor auth types (OTP / WebAuthN),
// depending on the credentials possessed by the user and those
// presented by the authentication client.
type twoFactorAuthHandler struct {
	wrap authHandler

	handlers map[authpb.Mechanism]authHandlerTFA
}

func newTwoFactorAuthHandler(wrap authHandler, handlers ...authHandlerTFA) *twoFactorAuthHandler {
	tfh := &twoFactorAuthHandler{
		wrap:     wrap,
		handlers: make(map[authpb.Mechanism]authHandlerTFA),
	}
	for _, h := range handlers {
		tfh.handlers[h.Mechanism()] = h
	}
	return tfh
}

func (h *twoFactorAuthHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	if hasTwoFactorAuth(user) {
		// If the user has 2FA credentials, and they're being passed
		// in the request, delegate to the appropriate 2FA handler.
		if tfah, ok := h.handlers[req.Mechanism]; ok {
			return tfah.Authenticate(ctx, req, user)
		}

		// Return an INSUFFICIENT_CREDENTIALS response, augmented with
		// mechanism-specific (challenge) information.
		resp := &authpb.Response{Status: authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS}
		for _, tfah := range h.handlers {
			var err error
			resp, err = tfah.InsufficientCredentials(ctx, req, user, resp)
			if err != nil {
				return nil, err
			}
		}
		return &authResponse{Response: resp}, nil
	}

	// If 2FA enforcement is turned on, we still allow the user to log in.
	// Enforcement is implemented via captive workflows.
	return h.wrap.Authenticate(ctx, req, user)
}

// OTP authentication handler.
type otpAuthHandler struct {
	sts  shortTermStorage
	wrap authHandler
}

var otpAntiReplayTimeout = 5 * time.Minute

func otpAntiReplayKey(userName, otp string) string {
	return fmt.Sprintf("otp/%s/%s", userName, otp)
}

func newOTPAuthHandler(h authHandler, sts shortTermStorage) *otpAuthHandler {
	return &otpAuthHandler{
		wrap: h,
		sts:  sts,
	}
}

func (h *otpAuthHandler) Mechanism() authpb.Mechanism { return authpb.Mechanism_OTP }

func (h *otpAuthHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	if user.TotpSecret == "" {
		return newErr("OTP unavailable"), nil
	}
	if req.Mechanism != authpb.Mechanism_OTP {
		return newErr("unsupported mechanism"), nil
	}

	// Check the short-term storage for anti-replay purposes.
	arKey := otpAntiReplayKey(user.Id, req.Otp)
	if _, err := h.sts.Get(ctx, []byte(arKey)); err == nil {
		return newErr("detected replayed OTP token"), nil
	}

	if !totp.Validate(req.Otp, user.TotpSecret) {
		return newErr("OTP validation failure"), nil
	}

	if err := h.sts.Set(ctx, []byte(arKey), []byte{'1'}, otpAntiReplayTimeout); err != nil {
		// Just log the error as a warning.
		log.Printf("short-term storage SetKey error: %v", err)
	}

	return h.wrap.Authenticate(
		withAuthenticatorID(ctx, pb.NewOTPAuthenticatorID()), req, user)
}

func (h *otpAuthHandler) InsufficientCredentials(ctx context.Context, req *authpb.Request, user *userdb.User, resp *authpb.Response) (*authpb.Response, error) {
	if user.TotpSecret != "" {
		resp.AvailableMechanisms = append(resp.AvailableMechanisms, authpb.Mechanism_OTP)
	}
	return resp, nil
}

// WebAuthN authentication handler.
type webauthnHandler struct {
	wrap     authHandler
	sts      shortTermStorage
	webauthn *webauthn.WebAuthn
	u2fAppID string
}

var webAuthnSessionTTL = 10 * time.Minute

func webAuthnSessionKey(sid string) string {
	return "webauthnsess/" + sid
}

func newWebauthnHandler(h authHandler, sts shortTermStorage, displayName, rpid, origin, u2fAppID string) (*webauthnHandler, error) {
	wh, err := webauthn.New(&webauthn.Config{
		RPDisplayName: displayName,
		RPID:          rpid,
		RPOrigins:     []string{origin},
	})
	if err != nil {
		return nil, err
	}

	return &webauthnHandler{
		wrap:     h,
		sts:      sts,
		webauthn: wh,
		u2fAppID: u2fAppID,
	}, nil
}

func (h *webauthnHandler) Mechanism() authpb.Mechanism { return authpb.Mechanism_WEBAUTHN }

func (h *webauthnHandler) saveSession(ctx context.Context, sid string, session *webauthn.SessionData) error {
	data, err := json.Marshal(session)
	if err != nil {
		return err
	}
	return h.sts.Set(ctx, []byte(webAuthnSessionKey(sid)), data, webAuthnSessionTTL)
}

func (h *webauthnHandler) loadSession(ctx context.Context, sid string) (*webauthn.SessionData, error) {
	data, err := h.sts.Get(ctx, []byte(webAuthnSessionKey(sid)))
	if err != nil {
		return nil, errors.New("session not found")
	}
	var session webauthn.SessionData
	if err := json.Unmarshal(data, &session); err != nil {
		return nil, err
	}
	return &session, nil
}

func (h *webauthnHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	if len(user.WebAuthnRegistrations) == 0 {
		return newErr("WebAuthN unavailable"), nil
	}

	assertionData, err := req.GetWebAuthnAssertion()
	if err != nil {
		return nil, fmt.Errorf("error decoding WebAuthN assertion: %w", err)
	}
	sessionData, err := h.loadSession(ctx, req.WebauthnSessionId)
	if err != nil {
		return nil, fmt.Errorf("error retrieving WebAuthN session: %w", err)
	}

	cred, err := h.webauthn.ValidateLogin(user, *sessionData, assertionData)
	if err != nil {
		return newErr(fmt.Sprintf("WebAuthN validation failure: %v", err)), nil
	}

	return h.wrap.Authenticate(
		withAuthenticatorID(ctx, pb.NewWebAuthnAuthenticatorID(cred)), req, user)
}

func (h *webauthnHandler) InsufficientCredentials(ctx context.Context, req *authpb.Request, user *userdb.User, resp *authpb.Response) (*authpb.Response, error) {
	if len(user.WebAuthnRegistrations) == 0 {
		return resp, nil
	}

	opts := []webauthn.LoginOption{
		// Tell the client to not require user verification
		// (as recommended for the 2FA use case).
		webauthn.WithUserVerification(protocol.VerificationDiscouraged),
	}
	if h.u2fAppID != "" {
		// Extension to enable U2F legacy compatibility mode.
		opts = append(opts, webauthn.WithAssertionExtensions(map[string]interface{}{
			"appid": h.u2fAppID,
		}))
	}

	assertionData, sessionData, err := h.webauthn.BeginLogin(user, opts...)
	if err != nil {
		return nil, fmt.Errorf("WebAuthn.BeginLogin() error: %w", err)
	}

	// Store the session data.
	sid := random.UUID()
	if err := h.saveSession(ctx, sid, sessionData); err != nil {
		return nil, fmt.Errorf("error storing WebAuthN session: %w", err)
	}

	// Return a response that includes the WebAuthN challenge.
	resp = resp.WithWebAuthnAssertion(assertionData)
	resp.WebauthnSessionId = sid
	resp.AvailableMechanisms = append(resp.AvailableMechanisms, authpb.Mechanism_WEBAUTHN)
	return resp, nil
}

func hasTwoFactorAuth(u *userdb.User) bool {
	return u.TotpSecret != "" || len(u.WebAuthnRegistrations) > 0
}
