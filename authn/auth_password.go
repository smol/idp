package authn

import (
	"context"
	"log"

	authpb "git.autistici.org/smol/idp/authn/proto"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
)

// Authenticator for the primary (main) password. 2FA is usually
// chained after this handler so that the primary password is always
// checked too.
type primaryPasswordAuthHandler struct {
	wrap authHandler
}

func newPrimaryPasswordAuthHandler(h authHandler) *primaryPasswordAuthHandler {
	return &primaryPasswordAuthHandler{wrap: h}
}

func (h *primaryPasswordAuthHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	if updated, err := userdb.PasswordHasher.Verify(user.EncryptedPassword, req.Password); err != nil {
		return newErr("wrong primary password"), nil
	} else if updated != "" {
		if err := user.SetEncryptedPrimaryPassword(updated); err != nil {
			log.Printf("error updating password for user %s: %v", user.Info.Name, err)
		}
	}

	authID := pb.NewPrimaryPasswordAuthenticatorID()
	return h.wrap.Authenticate(
		withPasswordAuthenticatorID(
			withAuthenticatorID(ctx, authID),
			authID),
		req, user)
}

// Authenticator for application-specific passwords (for a given service).
type appSpecificPasswordAuthHandler struct {
	service string
	wrap    authHandler
}

func newAppSpecificPasswordAuthHandler(h authHandler, service string) *appSpecificPasswordAuthHandler {
	return &appSpecificPasswordAuthHandler{
		wrap:    h,
		service: service,
	}
}

func (h *appSpecificPasswordAuthHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	// The request mechanism must be PASSWORD, do not accept
	// app-specific passwords along with 2FA.
	if req.Mechanism != authpb.Mechanism_PASSWORD {
		return newErr("unsupported mechanism"), nil
	}

	var ok bool
	var authID *pb.AuthenticatorID
	for _, asp := range user.AppSpecificPasswords {
		if asp.Service != h.service {
			continue
		}
		if updated, err := userdb.PasswordHasher.Verify(asp.EncryptedPassword, req.Password); err != nil {
			continue
		} else if updated != "" {
			asp.EncryptedPassword = updated
			if err := user.UpdateAppSpecificPassword(asp); err != nil {
				log.Printf("error updating app-specific password '%s' for user %s: %v", asp.Id, user.Info.Name, err)
			}
		}
		authID = pb.NewAppSpecificPasswordAuthenticatorID(asp.Id)
		ok = true
		break
	}

	if !ok {
		return newErr("wrong application-specific password"), nil
	}

	return h.wrap.Authenticate(
		withPasswordAuthenticatorID(
			withAuthenticatorID(ctx, authID),
			authID),
		req, user)
}

// Authentication handler for non-interactive services, switches
// between primary and application-specific passwords where possible.
type nonInteractiveAuthHandler struct {
	aspH, pwH authHandler
}

func newNonInteractiveAuthHandler(pwH, aspH authHandler) *nonInteractiveAuthHandler {
	return &nonInteractiveAuthHandler{
		pwH:  pwH,
		aspH: aspH,
	}
}

func (h *nonInteractiveAuthHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	if h.aspH != nil && hasTwoFactorAuth(user) {
		return h.aspH.Authenticate(ctx, req, user)
	}
	return h.pwH.Authenticate(ctx, req, user)
}
