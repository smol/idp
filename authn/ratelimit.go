package authn

import (
	"net"

	"git.autistici.org/smol/ratelimit"
)

type RLOptions struct {
	MaxSize  int                 `yaml:"max_size" doc:"maximum number of tracked entries"`
	Skip     []string            `yaml:"skip" doc:"ignore these IP ranges (addresses or CIDR, default localhost)"`
	V4Mask   int                 `yaml:"v4_mask_bits" doc:"bits to mask in IPv4 addresses (default 32)"`
	V6Mask   int                 `yaml:"v6_mask_bits" doc:"bits to mask in IPv6 addresses (default 64)"`
	GCParams *ratelimit.GCParams `yaml:"gc" doc:"configuration for the rate limiter garbage collector"`
}

func (o *RLOptions) Options() []ratelimit.Option {
	var opts []ratelimit.Option
	if o.MaxSize > 0 {
		opts = append(opts, ratelimit.WithMaxSize(o.MaxSize))
	}
	if len(o.Skip) > 0 {
		opts = append(opts, ratelimit.WithSkipRanges(o.Skip))
	}
	if o.V4Mask > 0 {
		opts = append(opts, ratelimit.WithIPMask(o.V4Mask, o.V6Mask))
	}
	if o.GCParams != nil {
		opts = append(opts, ratelimit.WithGCParams(*o.GCParams))
	}
	return opts
}

type RateLimitConfig struct {
	Volume *struct {
		RLOptions   `yaml:",inline"`
		Threshold   int `yaml:"threshold" doc:"threshold"`
		IntervalSec int `yaml:"interval" doc:"interval (seconds)"`
	} `yaml:"volume" doc:"volume-based rate limiter"`
	Cardinality *struct {
		RLOptions        `yaml:",inline"`
		Threshold        int                          `yaml:"threshold" doc:"score threshold"`
		IntervalSec      int                          `yaml:"interval" doc:"interval (seconds)"`
		BlockIntervalSec int                          `yaml:"block_interval" doc:"block interval (seconds)"`
		Params           *ratelimit.CardinalityParams `yaml:"cardinality" doc:"configuration for the cardinality-based rate limiter"`
	} `yaml:"cardinality" doc:"cardinality-based rate limiter"`
}

func buildLimiters(config *RateLimitConfig) (ratelimiter, blocker) {
	var rl ratelimiter = new(nilRatelimiter)
	if config.Volume != nil {
		rl = ratelimit.NewRateLimiter(
			config.Volume.Threshold,
			config.Volume.IntervalSec,
			config.Volume.Options()...,
		)
	}

	var bl blocker = new(nilBlocker)
	if config.Cardinality != nil {
		bl = ratelimit.NewCardinalityBlocker(
			config.Cardinality.IntervalSec,
			config.Cardinality.BlockIntervalSec,
			config.Cardinality.Threshold,
			config.Cardinality.Options()...,
		)
	}

	return rl, bl
}

type ratelimiter interface {
	Allow(int64, net.IP) bool
}

type blocker interface {
	Allow(int64, net.IP) bool
	Inc(int64, net.IP, string, bool) bool
}

type nilRatelimiter struct{}

func (nilRatelimiter) Allow(_ int64, _ net.IP) bool {
	return true
}

type nilBlocker struct{}

func (nilBlocker) Allow(_ int64, _ net.IP) bool {
	return true
}

func (nilBlocker) Inc(_ int64, _ net.IP, _ string, _ bool) bool {
	return false
}
