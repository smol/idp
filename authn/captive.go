package authn

import (
	"context"

	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/userdb"
)

type captiveWorkflowsHandler struct {
	wrap authHandler
}

func newCaptiveWorkflowsHandler(h authHandler) authHandler {
	return &captiveWorkflowsHandler{wrap: h}
}

func (h *captiveWorkflowsHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	resp, err := h.wrap.Authenticate(ctx, req, user)
	if err != nil || resp.Status != authpb.Response_STATUS_OK {
		return resp, err
	}

	if !hasTwoFactorAuth(user) {
		resp.ForcedWorkflows = append(resp.ForcedWorkflows, "enable-2fa")
	}

	return resp, err
}
