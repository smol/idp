package authn

import (
	"context"
	"log"
	"time"

	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/userdb"
)

var ingestTimeout = 10 * time.Second

// Audit handler for successful requests.
type auditHandler struct {
	wrap   authHandler
	client apb.AuditClient
}

func newAuditHandler(h authHandler, client apb.AuditClient) *auditHandler {
	return &auditHandler{
		wrap:   h,
		client: client,
	}
}

func (h *auditHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	resp, err := h.wrap.Authenticate(ctx, req, user)
	if err != nil || resp.Status != authpb.Response_STATUS_OK {
		return resp, err
	}

	// Build the authpb.Log using request information, and
	// additional information that might have been detected by
	// earlier handlers.
	info := getClientInfo(ctx)

	authLog := &authpb.Log{
		UserId:    user.Id,
		Username:  user.Info.Name,
		Service:   req.Service,
		Mechanism: req.Mechanism,
		SessionId: req.SessionId,
		ClientInfo: &authpb.ClientInfo{
			DeviceInfo: req.DeviceInfo,
			RemoteZone: info.remoteZone,
			Unknown:    info.unknown,
		},
	}
	if authID, ok := getAuthenticatorID(ctx); ok {
		authLog.AuthenticatorId = authID
	}

	l := apb.NewLoginLog(authLog)

	// Call Ingest on a separate goroutine to avoid blocking the
	// main authentication RPC. Give its own Context, decoupled
	// from the request one.
	go func() {
		ctx, cancel := context.WithTimeout(context.Background(), ingestTimeout)
		_, err := h.client.Ingest(ctx, l)
		cancel()
		if err != nil {
			log.Printf("audit.Ingest error: %v", err)
		}
	}()

	return resp, err
}
