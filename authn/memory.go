package authn

import (
	"context"
	"errors"
	"sync"
	"time"
)

type stsEntry struct {
	value  []byte
	expiry time.Time
}

type inMemorySTS struct {
	mx      sync.Mutex
	entries map[string]stsEntry
}

var errNotFound = errors.New("not found")

func newInMemorySTS() *inMemorySTS {
	return &inMemorySTS{
		entries: make(map[string]stsEntry),
	}
}

func (m *inMemorySTS) Get(_ context.Context, key []byte) ([]byte, error) {
	m.mx.Lock()
	defer m.mx.Unlock()

	keyStr := string(key)
	ent, ok := m.entries[keyStr]
	if !ok {
		return nil, errNotFound
	}
	if ent.expiry.Before(time.Now()) {
		delete(m.entries, keyStr)
		return nil, errNotFound
	}
	return ent.value, nil
}

func (m *inMemorySTS) Set(_ context.Context, key, value []byte, ttl time.Duration) error {
	m.mx.Lock()
	defer m.mx.Unlock()

	m.entries[string(key)] = stsEntry{
		value:  value,
		expiry: time.Now().Add(ttl),
	}

	return nil
}
