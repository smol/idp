package authn

import (
	"context"
	"errors"
	"net"
	"time"

	"git.autistici.org/smol/idp"
	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/location"
	"git.autistici.org/smol/idp/userdb"
)

type ServiceConfig struct {
	ASPService         string        `yaml:"asp_service" doc:"service name for app-specific passwords (defaults to the name of the service)"`
	IsInteractive      bool          `yaml:"interactive" doc:"enable for interactive (web) services supporting challenge-response login mechanisms"`
	DisableAudit       bool          `yaml:"disable_audit" doc:"disable audit logging"`
	DisableDeviceCheck bool          `yaml:"disable_device_check" doc:"disable client device checks"`
	FailureDelay       time.Duration `yaml:"failure_delay" doc:"how much to delay auth failures"`

	Webauthn struct {
		DisplayName string `yaml:"display_name" doc:"WebAuthN Display Name"`
		RPID        string `yaml:"rpid" doc:"WebAuthN RP ID"`
		Origin      string `yaml:"origin" doc:"WebAuthN origin"`
		U2FAppID    string `yaml:"legacy_u2f_appid" doc:"AppID for legacy U2F compatibility"`
	} `yaml:"webauthn"`

	RateLimit RateLimitConfig `yaml:"rate_limit" doc:"configuration of rate limiters"`
}

type service struct {
	rl     ratelimiter
	bl     blocker
	userdb userdb.Backend

	handler authHandler
}

// Build the authentication handler pipeline depending on the service configuration.
func makeService(name string, conf *ServiceConfig, features *idp.Features, userdb userdb.Backend, sts shortTermStorage, loc *location.Locator, aud apb.AuditClient) (*service, error) {
	okh := &userHandler{}
	var h authHandler

	if conf.IsInteractive {

		h = newPrimaryPasswordAuthHandler(okh)
		var tfaHandlers []authHandlerTFA

		if features.EnableWebAuthn {
			wh, err := newWebauthnHandler(
				h,
				sts,
				conf.Webauthn.DisplayName,
				conf.Webauthn.RPID,
				conf.Webauthn.Origin,
				conf.Webauthn.U2FAppID)
			if err != nil {
				return nil, err
			}
			tfaHandlers = append(tfaHandlers, wh)
		}

		if features.EnableOTP {
			tfaHandlers = append(tfaHandlers, newOTPAuthHandler(h, sts))
		}

		if len(tfaHandlers) > 0 {
			h = newTwoFactorAuthHandler(h, tfaHandlers...)
		}

		if !conf.DisableDeviceCheck && aud != nil {
			h = newClientCheckHandler(
				newNotifySuspiciousLoginHandler(h), aud)
		}

		// Account recovery bypasses all the above.
		h = newRecoveryAuthHandler(h, okh)

		if features.Enforce2FA {
			h = newCaptiveWorkflowsHandler(h)
		}

	} else {
		var aspHandler authHandler

		if features.EnableASPs {
			aspService := conf.ASPService
			if aspService == "" {
				aspService = name
			}
			aspHandler = newAppSpecificPasswordAuthHandler(okh, aspService)
		}

		h = newNonInteractiveAuthHandler(
			newPrimaryPasswordAuthHandler(okh),
			aspHandler)
	}

	if !conf.DisableAudit && aud != nil {
		h = newAuditHandler(h, aud)
	}

	if conf.FailureDelay > 0 {
		h = newFailureDelayHandler(h, conf.FailureDelay)
	}

	h = newClientLocationHandler(h, loc)

	rl, bl := buildLimiters(&conf.RateLimit)

	return &service{
		rl:      rl,
		bl:      bl,
		userdb:  userdb,
		handler: h,
	}, nil
}

func (s *service) withRatelimit(req *authpb.Request, f func() (*authResponse, error)) (*authResponse, error) {
	// If we have an IP, trigger the IP-based rate limiters before
	// we even attempt to talk to the database, as an optimization
	// against high-qps brute-forcing.
	ip := net.ParseIP(req.RemoteAddr)
	if ip == nil {
		return f()
	}

	t := time.Now().Unix()

	if !s.rl.Allow(t, ip) || !s.bl.Allow(t, ip) {
		return newErr("IP rate limited"), nil
	}

	resp, err := f()

	// Ensure that we do not feed back database errors into the
	// block lists.
	if err == nil {
		// Count the cardinality of username/password combinations.
		key := req.Username + req.Password
		_ = s.bl.Inc(t, ip, key, resp.Status == authpb.Response_STATUS_OK)
	}

	return resp, err
}

func (s *service) withUser(ctx context.Context, req *authpb.Request, f func(*userdb.User) (*authResponse, error)) (*authResponse, error) {
	// Fetch the user from the database.
	user, err := s.userdb.GetUserByName(ctx, req.Username)
	if errors.Is(err, userdb.ErrNotFound) {
		return newErr("user not found"), nil
	}
	if err != nil {
		return nil, err
	}

	resp, err := f(user)

	if err != nil {
		_ = user.Rollback()
	} else {
		err = user.Commit()
	}

	return resp, err
}

// Main authentication request handler. Runs the following steps:
// - run IP-based checks
// - fetch the user from the database
// - run additional user-based checks
// - invoke the authentication handler pipeline
func (s *service) handleRequest(ctx context.Context, req *authpb.Request) (*authResponse, error) {
	return s.withRatelimit(req, func() (*authResponse, error) {
		return s.withUser(ctx, req, func(user *userdb.User) (*authResponse, error) {
			return s.handler.Authenticate(ctx, req, user)
		})
	})
}

// Handler that serves a STATUS_OK response when authentication succeeds.
type userHandler struct{}

func (h *userHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	resp := &authpb.Response{
		Status:    authpb.Response_STATUS_OK,
		Mechanism: req.Mechanism,
	}
	if authID, ok := getAuthenticatorID(ctx); ok {
		resp.AuthenticatorId = authID
	}

	// Fill in user-specific fields.
	resp.UserId = user.Id
	resp.UserInfo = user.Info

	return &authResponse{Response: resp}, nil
}

type failureDelayHandler struct {
	authHandler

	delay time.Duration
}

func newFailureDelayHandler(h authHandler, delay time.Duration) authHandler {
	return &failureDelayHandler{
		authHandler: h,
		delay:       delay,
	}
}

func (h *failureDelayHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	resp, err := h.authHandler.Authenticate(ctx, req, user)
	if err == nil && resp.Status == authpb.Response_STATUS_ERROR {
		time.Sleep(h.delay)
	}
	return resp, err
}
