package authn

import (
	"context"
	"time"

	authpb "git.autistici.org/smol/idp/authn/proto"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
)

// Custom authpb.Response that includes a private error diagnostic
// message. The call to Authenticate has three interesting result
// states: successful auth, unsuccessful auth, and a processing error
// of some kind. Processing errors are eventually turned into
// authentication errors (pushing retry behavior to the client
// attempting authentication, instead of handling it at the RPC
// layer).
//
// But we are also interested in an explanation for the unsuccessful
// auth state, which should not be part of the Response proto since it
// must never go on the wire. That's what authResponse is for: the
// 'err' field is only meaningful when Response.Status is
// STATUS_ERROR.
type authResponse struct {
	*authpb.Response
	err string
}

// newErr creates an annotated STATUS_ERROR authResponse.
func newErr(err string) *authResponse {
	return &authResponse{
		Response: &authpb.Response{
			Status: authpb.Response_STATUS_ERROR,
		},
		err: err,
	}
}

// Authenticator handler interface for chaining handlers. Handlers can pass
// additional values to chained handlers down the line using the Context.
type authHandler interface {
	Authenticate(context.Context, *authpb.Request, *userdb.User) (*authResponse, error)
}

// Two-factor auth handlers have an additional method to modify
// INSUFFICIENT_CREDENTIALS responses before they're returned.
type authHandlerTFA interface {
	authHandler

	Mechanism() authpb.Mechanism
	InsufficientCredentials(context.Context, *authpb.Request, *userdb.User, *authpb.Response) (*authpb.Response, error)
}

type shortTermStorage interface {
	Get(context.Context, []byte) ([]byte, error)
	Set(context.Context, []byte, []byte, time.Duration) error
}

// Keep track of the authenticator ID that last succeeded (i.e. more
// strict), for further handlers to use.
type authenticatorIDCtxKeyType int

var authenticatorIDCtxKey authenticatorIDCtxKeyType = 1

func withAuthenticatorID(ctx context.Context, id *pb.AuthenticatorID) context.Context {
	// Only set the authenticator ID if not already present.
	if ctx.Value(authenticatorIDCtxKey) != nil {
		return ctx
	}
	return context.WithValue(ctx, authenticatorIDCtxKey, id)
}

func getAuthenticatorID(ctx context.Context) (*pb.AuthenticatorID, bool) {
	id, ok := ctx.Value(authenticatorIDCtxKey).(*pb.AuthenticatorID)
	return id, ok
}

// Keep track of the last *password-based* authenticator ID, to be
// used by further handlers that need to compute password-derived data
// (like decrypting encryption keys), in combination with the actual
// password in Request.Password.
type passwordAuthenticatorIDCtxKeyType int

var passwordAuthenticatorIDCtxKey passwordAuthenticatorIDCtxKeyType = 1

func withPasswordAuthenticatorID(ctx context.Context, id *pb.AuthenticatorID) context.Context {
	return context.WithValue(ctx, passwordAuthenticatorIDCtxKey, id)
}

/** unused
func getPasswordAuthenticatorID(ctx context.Context) (*pb.AuthenticatorID, bool) {
	id, ok := ctx.Value(passwordAuthenticatorIDCtxKey).(*pb.AuthenticatorID)
	return id, ok
}
**/

// Store client information detected by early running handlers.
type clientInfo struct {
	remoteZone string
	unknown    bool
}

type clientInfoCtxKeyType int

var clientInfoCtxKey clientInfoCtxKeyType = 1

func withClientInfo(ctx context.Context, info *clientInfo) context.Context {
	return context.WithValue(ctx, clientInfoCtxKey, info)
}

func getClientInfo(ctx context.Context) *clientInfo {
	if info, ok := ctx.Value(clientInfoCtxKey).(*clientInfo); ok {
		return info
	}
	return new(clientInfo)
}
