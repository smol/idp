package authn

import (
	"context"
	"log"
	"time"

	authpb "git.autistici.org/smol/idp/authn/proto"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
)

type recoveryAuthHandler struct {
	wrap, success authHandler
}

func newRecoveryAuthHandler(h, success authHandler) authHandler {
	return &recoveryAuthHandler{wrap: h, success: success}
}

func (h *recoveryAuthHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	if req.Mechanism == authpb.Mechanism_RECOVERY_TOKEN {

		now := time.Now()
		for _, token := range user.RecoveryTokens {
			if token.ExpiresAt != nil && token.ExpiresAt.AsTime().Before(now) {
				continue
			}

			if _, err := userdb.PasswordHasher.Verify(token.EncryptedToken, req.Password); err != nil {
				continue
			}

			// Success! Now consume the token.
			if err := user.DeleteRecoveryToken(token); err != nil {
				log.Printf("error deleting recovery token: %v", err)
			}

			// For account recovery purposes, disable second-factor authentication.
			if err := user.DisableSecondFactorAuth(); err != nil {
				log.Printf("error disabling second-factor auth during recovery: %v", err)
			}

			authID := pb.NewRecoveryAuthenticatorID(token)

			// Intercept the 'success' response and add a
			// forced workflow to reset the password.
			resp, err := h.success.Authenticate(withAuthenticatorID(ctx, authID), req, user)
			if err != nil || resp.Status != authpb.Response_STATUS_OK {
				return resp, err
			}
			resp.ForcedWorkflows = append(resp.ForcedWorkflows, "change-password")
			return resp, nil
		}

	}

	return h.wrap.Authenticate(ctx, req, user)
}
