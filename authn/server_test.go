package authn

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"git.autistici.org/smol/idp"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/testutil"
	pb "git.autistici.org/smol/idp/proto"
	"git.autistici.org/smol/idp/userdb"
	"github.com/descope/virtualwebauthn"
	"github.com/go-webauthn/webauthn/protocol"
	"github.com/go-webauthn/webauthn/webauthn"
	"github.com/pquerna/otp/totp"
)

// Must match the hashes in 'fixtures' below.
const (
	testPassword = "password"
	testASP      = "asp"

	totpSecret = "RGHD5VPD3NNMTH3VAZHSP36YFWM5RPFS"
)

var fixtures = `
INSERT INTO users (id, username, email, password) VALUES (1, 'test', 'testuser@example.com', '$argon2id$v=19$m=65536,t=1,p=4$4cRvKr/X9u9NKMev1jYhSg$WqVnGxKaWBPZrLqV2Zj/+A/NeNANlkzocs+86LubNBM');
INSERT INTO users (id, username, email, password, totp_secret) VALUES (2, 'test-otp', 'testuser@example.com', '$argon2id$v=19$m=65536,t=1,p=4$4cRvKr/X9u9NKMev1jYhSg$WqVnGxKaWBPZrLqV2Zj/+A/NeNANlkzocs+86LubNBM', 'RGHD5VPD3NNMTH3VAZHSP36YFWM5RPFS');
INSERT INTO asps (uid, id, service, password, comment) VALUES (2, 'abcdef', 'noninteractive', '$argon2id$v=19$m=65536,t=1,p=4$No/vItF7tFzgCSqQ+sXCNQ$V9by96KGhTvWyVoubcbKl+C1NRisPZ+j0xvM+3xO+nQ', 'comment1');
INSERT INTO asps (uid, id, service, password, comment) VALUES (2, 'zxcvbn', 'other-service', '$argon2id$v=19$m=65536,t=1,p=4$No/vItF7tFzgCSqQ+sXCNQ$V9by96KGhTvWyVoubcbKl+C1NRisPZ+j0xvM+3xO+nQ', 'comment2');
INSERT INTO users (id, username, email, password) VALUES (3, 'test-webauthn', 'testuser@example.com', '$argon2id$v=19$m=65536,t=1,p=4$4cRvKr/X9u9NKMev1jYhSg$WqVnGxKaWBPZrLqV2Zj/+A/NeNANlkzocs+86LubNBM');
`

func createTestServer(t *testing.T) (*AuthServer, userdb.Backend, func()) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}

	db, cleanup, err := testutil.CreateTestDB(fixtures, dir)
	if err != nil {
		t.Fatal(err)
	}

	features := &idp.Features{
		EnableOTP:      true,
		EnableASPs:     true,
		EnableWebAuthn: true,
		Enforce2FA:     true,
	}

	loginServiceConfig := &ServiceConfig{
		IsInteractive: true,
		DisableAudit:  true,
	}
	loginServiceConfig.Webauthn.DisplayName = "foo"
	loginServiceConfig.Webauthn.RPID = "auth.example.com"
	loginServiceConfig.Webauthn.Origin = "https://auth.example.com"
	config := &Config{
		Services: map[string]*ServiceConfig{
			"login": loginServiceConfig,
			"noninteractive": {
				DisableAudit: true,
			},
		},
	}

	srv, err := New(db, config, features, nil, dir)
	if err != nil {
		t.Fatalf("New(): %v", err)
	}

	return srv, db, func() {
		cleanup()
		os.RemoveAll(dir)
	}
}

func TestAuthN_Password(t *testing.T) {
	srv, _, cleanup := createTestServer(t)
	defer cleanup()

	for i, step := range []struct {
		password string
		expected authpb.Response_Status
	}{
		{testPassword, authpb.Response_STATUS_OK},
		{"wrong password", authpb.Response_STATUS_ERROR},
	} {
		resp, err := srv.Authenticate(context.Background(), &authpb.Request{
			Service:   "login",
			Username:  "test",
			Password:  step.password,
			Mechanism: authpb.Mechanism_PASSWORD,
		})
		if err != nil {
			t.Fatalf("step %d: Authenticate: %v", i, err)
		}

		if resp.Status != step.expected {
			t.Errorf("step %d: unexpected status %s", i, resp.Status)
			continue
		}
		if resp.Status == authpb.Response_STATUS_OK {
			if resp.AuthenticatorId == nil || resp.AuthenticatorId.Type != pb.AuthenticatorID_PRIMARY_PASSWORD {
				t.Errorf("step %d: unexpected authenticator_id: %s", i, resp.AuthenticatorId.Type)
			}
			if len(resp.ForcedWorkflows) != 1 || resp.ForcedWorkflows[0] != "enable-2fa" {
				t.Errorf("step %d: bad forced_workflows returned, expected 'enable-2fa': %+v", i, resp.ForcedWorkflows)
			}
		}
	}
}

func TestAuthN_TOTP(t *testing.T) {
	srv, _, cleanup := createTestServer(t)
	defer cleanup()

	code, _ := totp.GenerateCode(totpSecret, time.Now())

	for i, step := range []struct {
		mech     authpb.Mechanism
		password string
		otp      string
		expected authpb.Response_Status
	}{
		// These two steps represent the normal workflow.
		{authpb.Mechanism_PASSWORD, testPassword, "", authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS},
		{authpb.Mechanism_OTP, testPassword, code, authpb.Response_STATUS_OK},

		// Verify that the password in the first step is not evaluated yet.
		{authpb.Mechanism_PASSWORD, "wrong password", "", authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS},

		// Verify that the password in the second step is evaluated.
		{authpb.Mechanism_OTP, "wrong password", code, authpb.Response_STATUS_ERROR},

		// Verify that a bad OTP code won't succeed.
		{authpb.Mechanism_OTP, testPassword, "000000", authpb.Response_STATUS_ERROR},
	} {

		resp, err := srv.Authenticate(context.Background(), &authpb.Request{
			Service:   "login",
			Username:  "test-otp",
			Password:  step.password,
			Otp:       step.otp,
			Mechanism: step.mech,
		})
		if err != nil {
			t.Fatalf("step %d: Authenticate: %v", i, err)
		}

		if resp.Status != step.expected {
			t.Errorf("step %d: unexpected status %s", i, resp.Status)
			continue
		}
		if resp.Status == authpb.Response_STATUS_OK {
			if resp.AuthenticatorId == nil || resp.AuthenticatorId.Type != pb.AuthenticatorID_OTP {
				t.Errorf("step %d: unexpected authenticator_id: %s", i, resp.AuthenticatorId)
			}
		}
	}
}

func TestAuthN_ASP(t *testing.T) {
	srv, _, cleanup := createTestServer(t)
	defer cleanup()

	for i, step := range []struct {
		password string
		expected authpb.Response_Status
	}{
		{testASP, authpb.Response_STATUS_OK},
		{testPassword, authpb.Response_STATUS_ERROR},
		{"wrong password", authpb.Response_STATUS_ERROR},
	} {
		resp, err := srv.Authenticate(context.Background(), &authpb.Request{
			Service:   "noninteractive",
			Username:  "test-otp",
			Password:  step.password,
			Mechanism: authpb.Mechanism_PASSWORD,
		})
		if err != nil {
			t.Fatalf("step %d: Authenticate: %v", i, err)
		}

		if resp.Status != step.expected {
			t.Errorf("step %d: unexpected status %s", i, resp.Status)
			continue
		}
		if resp.Status == authpb.Response_STATUS_OK {
			if resp.AuthenticatorId == nil || resp.AuthenticatorId.Type != pb.AuthenticatorID_APP_SPECIFIC_PASSWORD {
				t.Errorf("step %d: unexpected authenticator_id: %s", i, resp.AuthenticatorId)
			}
		}
	}
}

func addWebauthnRegistration(db userdb.Backend, username string, rp virtualwebauthn.RelyingParty, authenticator *virtualwebauthn.Authenticator) error {
	w, err := webauthn.New(&webauthn.Config{
		RPDisplayName: "foo",
		RPID:          "auth.example.com",
		RPOrigins:     []string{"https://auth.example.com"},
	})
	if err != nil {
		return err
	}
	credential := virtualwebauthn.NewCredential(virtualwebauthn.KeyTypeEC2)

	user, err := db.GetUserByName(context.Background(), username)
	if err != nil {
		return err
	}

	credentialAssertion, sessionData, err := w.BeginRegistration(user)
	if err != nil {
		return fmt.Errorf("BeginRegistration: %w", err)
	}

	assertionData, _ := json.Marshal(credentialAssertion)
	parsedAttestationOptions, err := virtualwebauthn.ParseAttestationOptions(string(assertionData))
	if err != nil {
		return fmt.Errorf("ParseAttestationOptions: %w", err)
	}
	responseDataStr := virtualwebauthn.CreateAttestationResponse(rp, *authenticator, credential, *parsedAttestationOptions)

	parsedResponse, err := protocol.ParseCredentialCreationResponseBody(strings.NewReader(responseDataStr))
	if err != nil {
		return fmt.Errorf("ParseCredentialCreationResponseBody: %w", err)
	}

	cred, err := w.CreateCredential(user, *sessionData, parsedResponse)
	if err != nil {
		return fmt.Errorf("CreateCredential: %w", err)
	}

	authenticator.AddCredential(credential)

	if err := user.AddWebAuthnRegistration(&pb.WebAuthnRegistration{
		Name:      "key1",
		KeyHandle: string(cred.ID),
		PublicKey: string(cred.PublicKey),
	}); err != nil {
		return fmt.Errorf("AddWebAuthnRegistration: %w", err)
	}

	return user.Commit()
}

func TestAuthN_Webauthn(t *testing.T) {
	srv, db, cleanup := createTestServer(t)
	defer cleanup()

	rp := virtualwebauthn.RelyingParty{Name: "foo", ID: "auth.example.com", Origin: "https://auth.example.com"}
	authenticator := virtualwebauthn.NewAuthenticator()
	if err := addWebauthnRegistration(db, "test-webauthn", rp, &authenticator); err != nil {
		t.Fatalf("failed to add registration: %v", err)
	}

	var prevResp *authpb.Response
	for i, step := range []struct {
		mech     authpb.Mechanism
		password string
		webauthn bool
		expected authpb.Response_Status
	}{
		{authpb.Mechanism_PASSWORD, testPassword, false, authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS},
		{authpb.Mechanism_WEBAUTHN, testPassword, true, authpb.Response_STATUS_OK},

		// Verify that the first password is unused.
		{authpb.Mechanism_PASSWORD, "", false, authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS},
		{authpb.Mechanism_WEBAUTHN, testPassword, true, authpb.Response_STATUS_OK},

		// Correct token, wrong password.
		{authpb.Mechanism_PASSWORD, "", false, authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS},
		{authpb.Mechanism_WEBAUTHN, "wrong password", true, authpb.Response_STATUS_ERROR},
	} {
		req := &authpb.Request{
			Service:   "login",
			Username:  "test-webauthn",
			Password:  step.password,
			Mechanism: step.mech,
		}
		if step.webauthn {
			assertionOptions, err := virtualwebauthn.ParseAssertionOptions(prevResp.WebauthnEncodedAssertion)
			if err != nil {
				t.Errorf("step %d: ParseAssertionOptions: %v", i, err)
				continue
			}

			cred := authenticator.FindAllowedCredential(*assertionOptions)
			if cred == nil {
				t.Errorf("step %d: webauthn credential not found in authenticator", i)
				continue
			}
			assertionResponse := virtualwebauthn.CreateAssertionResponse(rp, authenticator, *cred, *assertionOptions)

			req.WebauthnSessionId = prevResp.WebauthnSessionId
			req.WebauthnEncodedAssertion = assertionResponse
		}

		resp, err := srv.Authenticate(context.Background(), req)
		if err != nil {
			t.Fatalf("step %d: Authenticate: %v", i, err)
		}
		prevResp = resp

		if resp.Status != step.expected {
			t.Errorf("step %d: unexpected status %s", i, resp.Status)
			continue
		}
		if resp.Status == authpb.Response_STATUS_OK {
			if resp.AuthenticatorId == nil || resp.AuthenticatorId.Type != pb.AuthenticatorID_WEBAUTHN {
				t.Errorf("step %d: unexpected authenticator_id: %s", i, resp.AuthenticatorId)
			}
		}
	}
}
