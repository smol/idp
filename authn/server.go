package authn

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"

	"git.autistici.org/smol/idp"
	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/location"
	"git.autistici.org/smol/idp/internal/memcache"
	"git.autistici.org/smol/idp/userdb"
	"github.com/prometheus/client_golang/prometheus"
	"google.golang.org/grpc"
)

type Config struct {
	// Service-specific configuration.
	Services map[string]*ServiceConfig `yaml:"services"`

	// Configure the location service.
	Location []*location.ZoneMapSpec `yaml:"location"`

	ShortTermStorage struct {
		MemcacheServers []string `yaml:"memcache_servers" doc:"list of memcache servers (host:port). DNS names can resolve to multiple IPs, and will be monitored for changes"`
	} `yaml:"short_term_storage"`
}

func (c *Config) makeServices(db userdb.Backend, features *idp.Features, sts shortTermStorage, loc *location.Locator, aud apb.AuditClient) (map[string]*service, error) {
	services := make(map[string]*service)
	for name, spec := range c.Services {
		svc, err := makeService(name, spec, features, db, sts, loc, aud)
		if err != nil {
			return nil, fmt.Errorf("service %s: %w", name, err)
		}
		services[name] = svc
	}
	return services, nil
}

type AuthServer struct {
	services map[string]*service

	authpb.UnimplementedAuthNServer
}

func New(db userdb.Backend, conf *Config, features *idp.Features, aud apb.AuditClient, dir string) (*AuthServer, error) {
	loc, err := location.New(conf.Location)
	if err != nil {
		return nil, err
	}

	var sts shortTermStorage
	if len(conf.ShortTermStorage.MemcacheServers) > 0 {
		mc, err := memcache.NewReplicated(conf.ShortTermStorage.MemcacheServers)
		if err != nil {
			return nil, err
		}
		sts = mc
	} else {
		sts = newInMemorySTS()
	}

	services, err := conf.makeServices(db, features, sts, loc, aud)
	if err != nil {
		return nil, err
	}

	return &AuthServer{
		services: services,
	}, nil
}

var errServiceNotFound = errors.New("service not found")

func (s *AuthServer) Authenticate(ctx context.Context, req *authpb.Request) (*authpb.Response, error) {
	service, ok := s.services[req.Service]
	if !ok {
		return nil, errServiceNotFound
	}

	start := time.Now()

	resp, err := service.handleRequest(ctx, req)

	if err != nil {
		log.Printf("error: %s -> %v", req, err)
		return nil, err
	}

	authRequestsCounter.WithLabelValues(
		resp.Status.String(), req.Service, req.Mechanism.String(),
	).Inc()
	authRequestsLatency.WithLabelValues(
		resp.Status.String(), req.Service, req.Mechanism.String(),
	).Observe(time.Since(start).Seconds() * 1000)

	var debugMsg string
	if resp.err != "" {
		debugMsg = fmt.Sprintf(" (%v)", resp.err)
	}
	log.Printf("%s -> %s%s", req, resp.Response, debugMsg)

	return resp.Response, err
}

// LocalStub returns a wrapper that can turn a local, in-process
// instance of AuthServer into a authpb.AuthNClient for GRPC clients.
func (s *AuthServer) LocalStub() authpb.AuthNClient {
	return &authServerLocalStub{s}
}

type authServerLocalStub struct {
	*AuthServer
}

func (s *authServerLocalStub) Authenticate(ctx context.Context, req *authpb.Request, _ ...grpc.CallOption) (*authpb.Response, error) {
	return s.AuthServer.Authenticate(ctx, req)
}

// Instrumentation.
var (
	authRequestsCounter = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "idp_auth_requests_total",
			Help: "Number of authentication requests",
		},
		[]string{"status", "service", "mechanism"},
	)
	authRequestsLatency = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "idp_auth_latency",
			Help:    "Latency of authentication requests (server-side).",
			Buckets: prometheus.ExponentialBuckets(5, 1.4142, 16),
		},
		[]string{"status", "service", "mechanism"},
	)
)

func init() {
	prometheus.MustRegister(
		authRequestsCounter,
		authRequestsLatency,
	)
}
