package proto

import (
	"encoding/json"
	"strings"

	"github.com/go-webauthn/webauthn/protocol"
)

func (r *Request) GetWebAuthnAssertion() (*protocol.ParsedCredentialAssertionData, error) {
	return protocol.ParseCredentialRequestResponseBody(strings.NewReader(r.WebauthnEncodedAssertion))
}

func (r *Response) WithWebAuthnAssertion(assertionData *protocol.CredentialAssertion) *Response {
	data, _ := json.Marshal(assertionData)
	r.WebauthnEncodedAssertion = string(data)
	return r
}
