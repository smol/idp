package authn

import (
	"context"
	"log"
	"net"
	"time"

	apb "git.autistici.org/smol/idp/audit/proto"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/location"
	"git.autistici.org/smol/idp/userdb"
)

// Authentication handler that adds location information to the
// current context clientInfo.
type clientLocationHandler struct {
	loc  *location.Locator
	wrap authHandler
}

func newClientLocationHandler(h authHandler, loc *location.Locator) *clientLocationHandler {
	return &clientLocationHandler{
		wrap: h,
		loc:  loc,
	}
}

func (h *clientLocationHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	info := getClientInfo(ctx)

	if ip := net.ParseIP(req.RemoteAddr); ip != nil {
		if zone, ok := h.loc.Lookup(ip); ok {
			info.remoteZone = zone
			ctx = withClientInfo(ctx, info)
		}
	}

	return h.wrap.Authenticate(ctx, req, user)
}

// Authentication handler that checks client / device information of
// the current request for suspiciousness, using the user/accounting
// service.
type clientCheckHandler struct {
	wrap   authHandler
	client apb.AuditClient
}

var checkDeviceTimeout = 2 * time.Second

func newClientCheckHandler(h authHandler, client apb.AuditClient) *clientCheckHandler {
	return &clientCheckHandler{
		wrap:   h,
		client: client,
	}
}

func (h *clientCheckHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	info := getClientInfo(ctx)

	if req.DeviceInfo != nil {
		// This request should have a very short timeout to
		// minimize latency impact on interactive authentications.
		sctx, cancel := context.WithTimeout(ctx, checkDeviceTimeout)
		resp, err := h.client.CheckDevice(sctx, &apb.CheckRequest{
			Username:   req.Username,
			Service:    req.Service,
			DeviceInfo: req.DeviceInfo,
			RemoteZone: info.remoteZone,
		})
		cancel()

		if err != nil {
			// Fail open, log error.
			log.Printf("checkDevice error: %v", err)
		} else {
			info.unknown = resp.Unknown
			ctx = withClientInfo(ctx, info)
		}
	}

	return h.wrap.Authenticate(ctx, req, user)
}

// Handler to notify the user when we see an interactive login from a
// previously unknown device + zone.
type notifySuspiciousLoginHandler struct {
	wrap authHandler
}

func newNotifySuspiciousLoginHandler(h authHandler) *notifySuspiciousLoginHandler {
	return &notifySuspiciousLoginHandler{wrap: h}
}

func (h *notifySuspiciousLoginHandler) Authenticate(ctx context.Context, req *authpb.Request, user *userdb.User) (*authResponse, error) {
	resp, err := h.wrap.Authenticate(ctx, req, user)
	if err != nil || resp.Status != authpb.Response_STATUS_OK {
		return resp, err
	}

	if getClientInfo(ctx).unknown {
		// TODO: implement external notification mechanism.
		log.Printf("detected new device for %s (%s)", user.Info.Name, req.DeviceInfo.String())
	}

	return resp, err
}
