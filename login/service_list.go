package login

import (
	"net/http"

	"git.autistici.org/smol/idp/internal/sessionstore"
)

// The serviceListManager keeps track of services the user has logged
// in to, so that we can eventually logout.
type serviceListManager struct {
	store *sessionstore.Store
}

func newServiceListManager(store *sessionstore.Store) *serviceListManager {
	return &serviceListManager{
		store: store,
	}
}

func (s *serviceListManager) getServiceList(req *http.Request) []string {
	var l []string
	s.store.Get(req, &l) //nolint:errcheck
	return l
}

func (s *serviceListManager) del(w http.ResponseWriter) error {
	return s.store.Delete(w)
}

func (s *serviceListManager) save(w http.ResponseWriter, l []string) error {
	return s.store.Set(w, l)
}

// Add a service to the current service list.
func (s *serviceListManager) AddServiceToList(w http.ResponseWriter, req *http.Request, service string) error {
	l := s.getServiceList(req)

	var found bool
	for _, svc := range l {
		if svc == service {
			found = true
			break
		}
	}
	if found {
		return nil
	}

	return s.save(w, append(l, service))
}
