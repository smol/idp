// Package login implements the authentication workflow, protecting
// another application by means of its Wrap() middleware.
//
// Internally, it runs a simple state machine meant to match the
// interactions with the underlying auth-server. State transitions
// happen on POST requests. Request handling is split into two stages:
// the processing stage (processing request parameters and eventually
// modifying the current state), and the rendering stage (which
// renders content to the user).
//
// - we start from the BEGIN state, where the user is asked for
// username and password.
//
// - we make the first AuthRequest, which has two possible non-error
// return values ("ok" and "need 2fa"), resulting in the OK or 2FA
// states.
//
// - in the 2FA state, we present the user with a request for the
// second authentication factor. We make a second AuthRequest that
// includes second factor information, resulting in the OK state if
// successful.
//
// States are tied to specific URLs because we want to make states
// visible to the browser, and possibly give users the option of
// hitting the back button -- though doing so will likely result in
// being reset to the BEGIN state (but it makes it easier to have
// multiple endpoints for the 2FA state).
//
// The login state machine is stored in a short-lived session cookie,
// which is global browser state, but the original_url parameter must
// instead be tracked per-window, so it must be brought along the
// request flow as a form parameter.
package login

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"git.autistici.org/smol/idp"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/sessionstore"
	"git.autistici.org/smol/idp/internal/web"
	lpb "git.autistici.org/smol/idp/login/proto"
	pb "git.autistici.org/smol/idp/proto"
)

type loginServer struct {
	authSessionStore  *sessionstore.Store
	loginSessionStore *sessionstore.Store
	deviceMgr         *deviceManager
	template          *web.Template
	authTTL           time.Duration

	features     *idp.Features
	authClient   AuthClient
	loginHandler loginHandler
}

func newLoginServer(features *idp.Features, authSessionStore *sessionstore.Store, loginSessionStore *sessionstore.Store, authClient AuthClient, loginHandler loginHandler, deviceMgr *deviceManager, template *web.Template, authTTLSecs int) *loginServer {
	return &loginServer{
		authSessionStore:  authSessionStore,
		loginSessionStore: loginSessionStore,
		authClient:        authClient,
		loginHandler:      loginHandler,
		template:          template,
		deviceMgr:         deviceMgr,
		authTTL:           time.Duration(authTTLSecs) * time.Second,
		features:          features,
	}
}

func (l *loginServer) restart(w http.ResponseWriter, req *http.Request, state *lpb.LoginState) {
	state = state.Clear().Error("There's been an error in processing your request, please try again")
	l.redirect(w, req, state, PathPrefix+"/login/user")
}

func (l *loginServer) redirect(w http.ResponseWriter, req *http.Request, state *lpb.LoginState, uri string) {
	l.loginSessionStore.Set(w, state.SessionParams) //nolint:errcheck
	http.Redirect(w, req, state.RequestParams.AddToURL(uri), http.StatusFound)
}

func (l *loginServer) doLogin(w http.ResponseWriter, req *http.Request, ls *lpb.LoginState, dev *pb.DeviceInfo, authReq *authpb.Request, authResp *authpb.Response) {
	// Delete the login session, don't need it around anymore.
	l.loginSessionStore.Delete(w) //nolint:errcheck

	// Create the authenticated session.
	auth := &lpb.Auth{
		UserId:    authResp.UserId,
		Username:  authResp.UserInfo.Name,
		Deadline:  time.Now().Add(l.authTTL).Unix(),
		SessionId: dev.Id,
	}

	// If there are forced workflows, create a captive session.
	// Note that at this point we capture the requestParams into
	// the session, which is unfortunate, because the workflows do
	// not support transparently passing them (yet?). Hopefully
	// the user will complete the workflows in the same browser
	// tab before reloading the other tabs.
	if len(authResp.ForcedWorkflows) > 0 {
		auth.Captive = &lpb.CaptiveStatus{
			Workflows: authResp.ForcedWorkflows,
		}
	}

	// Print a useful (hopefully) log message.
	debugStr := fmt.Sprintf("id=%s, mechanism=%s", authResp.UserId, authReq.Mechanism.String())
	if len(authResp.ForcedWorkflows) > 0 {
		debugStr += fmt.Sprintf(", forced_workflows=%v", authResp.ForcedWorkflows)
	}
	log.Printf("successful authentication for %s (%s)", authResp.UserInfo.Name, debugStr)

	// Finalize the HTTP side of the deal by calling the loginHandler.
	l.authSessionStore.Set(w, auth) //nolint:errcheck
	l.loginHandler.HandleLogin(w, req, ls.RequestParams, authResp.UserId)
}

// Set common attributes for all authpb.Request protos.
func fixAuthRequest(authReq *authpb.Request, req *http.Request) {
	// We can trust the net/http server to resolve X-Forwarded-For to the RemoteAddr.
	var remoteAddr = req.RemoteAddr
	if host, _, err := net.SplitHostPort(remoteAddr); err == nil {
		remoteAddr = host
	}
	authReq.RemoteAddr = remoteAddr
}

// Attempt to authenticate, if successful handle the rest of the request.
func (l *loginServer) authenticate(w http.ResponseWriter, req *http.Request, state *lpb.LoginState, dev *pb.DeviceInfo, authReq *authpb.Request) bool {
	fixAuthRequest(authReq, req)

	authResp, err := l.authClient.Authenticate(req.Context(), authReq)
	if err != nil {
		log.Printf("authentication rpc error: %v", err)
		return false
	}

	switch authResp.Status {
	case authpb.Response_STATUS_OK:
		l.doLogin(w, req, state, dev, authReq, authResp)
		return true

	case authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS:
		state.SessionParams.SetPartial(authReq, authResp)

		switch {
		// Favor WebAuthn if available.
		case state.SessionParams.HasWebAuthn():
			l.redirect(w, req, state, PathPrefix+"/login/webauthn")
		case state.SessionParams.HasOTP():
			l.redirect(w, req, state, PathPrefix+"/login/otp")
		default:
			http.Error(w, "2FA requested but not available", http.StatusInternalServerError)
		}
		return true
	}

	return false
}

func makePasswordAuthRequest(username, password string, dev *pb.DeviceInfo) *authpb.Request {
	return &authpb.Request{
		Username:   username,
		Password:   password,
		Mechanism:  authpb.Mechanism_PASSWORD,
		DeviceInfo: dev,
	}
}

func (l *loginServer) handlePassword(w http.ResponseWriter, req *http.Request, state *lpb.LoginState, dev *pb.DeviceInfo) {
	username := normalizeUsername(req.FormValue("username"))
	password := req.FormValue("password")

	if req.Method == http.MethodPost && username != "" && password != "" {
		authReq := makePasswordAuthRequest(username, password, dev)
		if l.authenticate(w, req, state, dev, authReq) {
			return
		} else {
			state = state.Error("Authentication failed")
		}
	}

	l.render(w, req, state, "login.html", nil)
}

func canWebAuthn(sess *lpb.SessionParams) bool {
	return (sess.Username != "" && sess.Password != "" &&
		sess.HasWebAuthn() && sess.WebauthnSessionId != "" && sess.WebauthnChallenge != "")
}

func makeWebAuthnAuthRequest(sess *lpb.SessionParams, webauthnResponse string, dev *pb.DeviceInfo) *authpb.Request {
	return &authpb.Request{
		Username:                 sess.Username,
		Password:                 sess.Password,
		Mechanism:                authpb.Mechanism_WEBAUTHN,
		DeviceInfo:               dev,
		WebauthnSessionId:        sess.WebauthnSessionId,
		WebauthnEncodedAssertion: webauthnResponse,
	}
}

func (l *loginServer) handleWebAuthn(w http.ResponseWriter, req *http.Request, state *lpb.LoginState, dev *pb.DeviceInfo) {
	if !canWebAuthn(state.SessionParams) {
		l.restart(w, req, state)
		return
	}

	webauthnResponse := req.FormValue("webauthn")

	if req.Method == http.MethodPost && webauthnResponse != "" {
		authReq := makeWebAuthnAuthRequest(state.SessionParams, webauthnResponse, dev)
		if l.authenticate(w, req, state, dev, authReq) {
			return
		}
		state = state.Error("Authentication failed")
		l.redirect(w, req, state, PathPrefix+"/login/user")
		return
	}

	l.render(w, req, state, "login_webauthn.html", nil)
}

func canOTP(sess *lpb.SessionParams) bool {
	return (sess.Username != "" && sess.Password != "" && sess.HasOTP())
}

func makeOTPAuthRequest(sess *lpb.SessionParams, otp string, dev *pb.DeviceInfo) *authpb.Request {
	return &authpb.Request{
		Username:   sess.Username,
		Password:   sess.Password,
		Mechanism:  authpb.Mechanism_OTP,
		DeviceInfo: dev,
		Otp:        otp,
	}
}

func (l *loginServer) handleOTP(w http.ResponseWriter, req *http.Request, state *lpb.LoginState, dev *pb.DeviceInfo) {
	if !canOTP(state.SessionParams) {
		l.restart(w, req, state)
		return
	}

	otp := req.FormValue("otp")

	if req.Method == http.MethodPost && otp != "" {
		authReq := makeOTPAuthRequest(state.SessionParams, otp, dev)
		if l.authenticate(w, req, state, dev, authReq) {
			return
		}
		state = state.Error("Authentication failed")
		l.redirect(w, req, state, PathPrefix+"/login/user")
		return
	}

	l.render(w, req, state, "login_otp.html", nil)
}

func makeRecoveryRequest(sess *lpb.SessionParams, code string) *authpb.Request {
	return &authpb.Request{
		Username:  sess.Username,
		Password:  code,
		Mechanism: authpb.Mechanism_RECOVERY_TOKEN,
	}
}

func (l *loginServer) handleRecovery(w http.ResponseWriter, req *http.Request, state *lpb.LoginState, dev *pb.DeviceInfo) {
	username := req.FormValue("username")
	mech := req.FormValue("mechanism")
	code := req.FormValue("code")

	// We map the stage of the request based on the presence of
	// request parameters. The username is saved onto the session,
	// mostly because we can.
	stage := "start"
	if mech != "" {
		stage = "code"
	} else if username != "" {
		stage = "choose"
	}

	if req.Method == http.MethodPost {
		if code != "" && mech != "" && state.SessionParams.Username != "" {
			authReq := makeRecoveryRequest(state.SessionParams, code)
			if l.authenticate(w, req, state, dev, authReq) {
				return
			}
			log.Printf("account recovery attempt failed for user %s", authReq.Username)
			state = state.Clear().Error("Recovery failed")
			stage = "start"
		} else if mech == "email" {
			// TODO: send email with new recovery code.
			stage = "email_sent"
		} else if username != "" {
			state.SessionParams.Username = username
		}
	}

	l.render(w, req, state, "login_recovery.html", map[string]any{
		"Mechanism": mech,
		"Stage":     stage,
	})
}

func (l *loginServer) render(w http.ResponseWriter, req *http.Request, state *lpb.LoginState, tpl string, extraData map[string]any) {
	// Always update the session.
	errmsg := state.SessionParams.PopError()
	l.loginSessionStore.Set(w, state.SessionParams) //nolint:errcheck

	// Now just render the HTML page.
	setCSP(w, defaultCSP)

	t := l.template.WithData(map[string]any{
		"Login": state,
		"Error": errmsg,
	})
	if extraData != nil {
		t = t.WithData(extraData)
	}
	t.Execute(w, req, tpl)
}

func (l *loginServer) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	state, err := lpb.LoginStateFromRequest(req, l.loginSessionStore)
	if err != nil {
		state, err = lpb.NewLoginState(req)
		if err != nil {
			log.Printf("NewLoginState: %v", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	dev := l.deviceMgr.deviceInfoFromRequest(w, req)

	switch {
	case req.URL.Path == PathPrefix+"/login/user":
		l.handlePassword(w, req, state, dev)
	case req.URL.Path == PathPrefix+"/login/otp" && l.features.EnableOTP:
		l.handleOTP(w, req, state, dev)
	case req.URL.Path == PathPrefix+"/login/webauthn" && l.features.EnableWebAuthn:
		l.handleWebAuthn(w, req, state, dev)
	case req.URL.Path == PathPrefix+"/login/recovery" && l.features.EnableAccountRecovery:
		l.handleRecovery(w, req, state, dev)
	default:
		http.NotFound(w, req)
	}
}
