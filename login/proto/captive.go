package proto

func (s *CaptiveStatus) Current() string {
	if len(s.Workflows) == 0 {
		return ""
	}
	return s.Workflows[0]
}

func (s *CaptiveStatus) Done() *CaptiveStatus {
	if len(s.Workflows) < 1 {
		panic("captive.Status.Done() called when empty")
	}
	s.Workflows = s.Workflows[1:]
	if len(s.Workflows) == 0 {
		return nil
	}
	return s
}

func (s *CaptiveStatus) Empty() bool {
	return len(s.Workflows) == 0 && s.Exit == ""
}
