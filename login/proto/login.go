package proto

import (
	"time"

	authpb "git.autistici.org/smol/idp/authn/proto"
)

func (a *Auth) Valid() bool {
	return a.UserId != "" && a.Deadline > time.Now().Unix()
}

func (a *Auth) IsCaptured() bool {
	return a.Captive != nil && !a.Captive.Empty()
}

func (a *Auth) MakeAuthLog() *authpb.Log {
	return &authpb.Log{
		UserId:    a.UserId,
		Username:  a.Username,
		SessionId: a.SessionId,
	}
}
