package proto

import (
	"encoding/base64"
	"fmt"
	"html/template"
	"net/http"
	"net/url"

	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/sessionstore"
	"google.golang.org/protobuf/proto"
)

const requestParamsField = "_rp"

func (p *RequestParams) FieldName() string { return requestParamsField }

func (p *RequestParams) Encode() string {
	data, _ := proto.Marshal(p)
	return base64.URLEncoding.EncodeToString(data)
}

func (p *RequestParams) InputField() template.HTML {
	// We do know that this is safe.
	//nolint:gosec
	return template.HTML(fmt.Sprintf(
		"<input type=\"hidden\" name=\"%s\" value=\"%s\">",
		p.FieldName(),
		p.Encode(),
	))
}

func (p *RequestParams) AddToURL(uriStr string) string {
	uri, err := url.Parse(uriStr)
	if err != nil {
		panic("AddToURL() called with invalid URL")
	}

	values := uri.Query()
	values.Set(requestParamsField, p.Encode())
	u := *uri
	u.RawQuery = values.Encode()
	return u.String()
}

func newRequestParamsFromURL(uri string) *RequestParams {
	return &RequestParams{
		Redirect: uri,
	}
}

// NewRequestParamsFromRequest creates RequestParams out of query
// arguments, in particular 'redir'. This is the other half of the
// logic in LoginApp.WithAuth() that builds the original request that
// starts the login process.
func NewRequestParamsFromRequest(req *http.Request) (*RequestParams, error) {
	if uri := req.FormValue("redir"); uri != "" {
		return newRequestParamsFromURL(uri), nil
	}
	return nil, fmt.Errorf("no parameters found in %s", req.URL.String())
}

func parseRequestParamsFromRequest(req *http.Request) (*RequestParams, error) {
	data, err := base64.URLEncoding.DecodeString(
		req.FormValue(requestParamsField))
	if err != nil {
		return nil, fmt.Errorf("error decoding requestParams: %w", err)
	}

	var p RequestParams
	if err := proto.Unmarshal(data, &p); err != nil {
		return nil, fmt.Errorf("error decoding requestParams: %w", err)
	}

	return &p, nil
}

func isMechanismInList(l []authpb.Mechanism, mech authpb.Mechanism) bool {
	for _, m := range l {
		if m == mech {
			return true
		}
	}
	return false
}

func (p *SessionParams) HasOTP() bool {
	return isMechanismInList(p.AvailableTfaMechanisms, authpb.Mechanism_OTP)
}

func (p *SessionParams) HasWebAuthn() bool {
	return isMechanismInList(p.AvailableTfaMechanisms, authpb.Mechanism_WEBAUTHN)
}

func (p *SessionParams) PopError() string {
	errmsg := p.ErrorMessage
	p.ErrorMessage = ""
	return errmsg
}

func (p *SessionParams) SetPartial(authReq *authpb.Request, authResp *authpb.Response) {
	p.Username = authReq.Username
	p.Password = authReq.Password
	p.AvailableTfaMechanisms = authResp.AvailableMechanisms
	p.WebauthnSessionId = authResp.WebauthnSessionId
	p.WebauthnChallenge = authResp.WebauthnEncodedAssertion
}

// Read an existing login state from the request.
func LoginStateFromRequest(req *http.Request, store *sessionstore.Store) (*LoginState, error) {
	rp, err := parseRequestParamsFromRequest(req)
	if err != nil {
		return nil, fmt.Errorf("request params: %w", err)
	}

	var sp SessionParams
	if err := store.Get(req, &sp); err != nil {
		return nil, fmt.Errorf("session params: %w", err)
	}

	return &LoginState{
		RequestParams: rp,
		SessionParams: &sp,
	}, nil
}

// NewLoginState creates a new LoginState using parameters of the HTTP request.
func NewLoginState(req *http.Request) (*LoginState, error) {
	// Capture the authRequestId query arg.
	rp, err := NewRequestParamsFromRequest(req)
	if err != nil {
		return nil, err
	}

	return &LoginState{
		RequestParams: rp,
		SessionParams: &SessionParams{
			Valid: true,
		},
	}, nil
}

func (s *LoginState) Error(errmsg string) *LoginState {
	return &LoginState{
		RequestParams: s.RequestParams,
		SessionParams: &SessionParams{
			Valid:        true,
			ErrorMessage: errmsg,
		},
	}
}

func (s *LoginState) Clear() *LoginState {
	return &LoginState{
		RequestParams: s.RequestParams,
		SessionParams: &SessionParams{
			Valid: true,
		},
	}
}
