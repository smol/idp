package login

import (
	"context"
	"errors"
	"log"
	"net/http"
	"net/url"
	"time"

	"git.autistici.org/smol/idp"
	authpb "git.autistici.org/smol/idp/authn/proto"
	"git.autistici.org/smol/idp/internal/cryptutil"
	"git.autistici.org/smol/idp/internal/sessionstore"
	"git.autistici.org/smol/idp/internal/web"
	lpb "git.autistici.org/smol/idp/login/proto"
	"github.com/gorilla/securecookie"
	"google.golang.org/grpc"
)

const (
	// Maximum duration of a single login workflow.
	defaultLoginSessionLifetime = 15 * time.Minute

	// Default value for auth_session_lifetime.
	defaultAuthSessionLifetime = 16 * time.Hour
)

const (
	// A relatively strict CSP.
	defaultCSP = "default-src 'none'; img-src 'self' data:; script-src 'self'; style-src 'self'; connect-src 'self'; frame-ancestors 'none'; base-uri 'none';"

	// Slightly looser CSP for the logout page: it needs to load remote
	// images.
	logoutCSP = "default-src 'none'; img-src *; script-src 'self'; style-src 'self'; connect-src *; frame-ancestors 'none'; base-uri 'none';"
)

type ctxKey int

const authCtxKey ctxKey = 0

// GetAuth returns the current user information, if any. Presence of an Auth
// object implies that the authentication succeeded in all contexts *except*
// the logout handler.
func GetAuth(ctx context.Context) (*lpb.Auth, bool) {
	s, ok := ctx.Value(authCtxKey).(*lpb.Auth)
	return s, ok
}

// Add authentication context to the current request. Exported for testing.
func AddAuthToRequest(req *http.Request, auth *lpb.Auth) *http.Request {
	return req.WithContext(context.WithValue(req.Context(), authCtxKey, auth))
}

// Config holds the configuration for the login service.
type Config struct {
	AuthService          string        `yaml:"auth_service" doc:"service for authentication"`
	AuthSessionLifetime  time.Duration `yaml:"auth_session_lifetime" doc:"lifetime of a SSO session"`
	LoginSessionLifetime time.Duration `yaml:"login_session_lifetime" doc:"max lifetime of a single login session"`

	DeviceAuthKey  cryptutil.SessionAuthenticationKey `yaml:"device_auth_key" doc:"authentication key for device cookies"`
	SessionAuthKey cryptutil.SessionAuthenticationKey `yaml:"session_auth_key" doc:"authentication key for session cookies"`
	SessionEncKey  cryptutil.SessionEncryptionKey     `yaml:"session_enc_key" doc:"encryption key for session cookies"`
}

func (c *Config) sanityCheck() error {
	// Sanity checks on the configuration.
	if len(c.DeviceAuthKey) == 0 {
		log.Printf("Warning: device_auth_key not set, generating temporary random device secret")
		c.DeviceAuthKey = securecookie.GenerateRandomKey(64)
	}
	if len(c.SessionAuthKey) == 0 {
		log.Printf("Warning: session_auth_key not set, generating temporary random session secrets")
		c.SessionAuthKey = securecookie.GenerateRandomKey(64)
		c.SessionEncKey = securecookie.GenerateRandomKey(32)
	}

	if c.AuthSessionLifetime == 0 {
		c.AuthSessionLifetime = defaultAuthSessionLifetime
	}
	if c.LoginSessionLifetime == 0 {
		c.LoginSessionLifetime = defaultLoginSessionLifetime
	}

	if c.AuthService == "" {
		return errors.New("auth_service is not set")
	}

	return nil
}

// AuthClient is a wrapper interface for an id/auth.Client that adds
// support for a Logout event. This allows injection of state-aware
// components that can trigger on both successful authentication and
// logout to maintain external session-scoped state.
type AuthClient interface {
	Authenticate(context.Context, *authpb.Request, ...grpc.CallOption) (*authpb.Response, error)
	Logout(context.Context, *lpb.Auth) error
}

type loginHandler interface {
	HandleLogin(http.ResponseWriter, *http.Request, *lpb.RequestParams, string)
}

type redirectHandler struct{}

// Finalize the login workflow by redirecting to the original URL.
func (*redirectHandler) HandleLogin(w http.ResponseWriter, req *http.Request, params *lpb.RequestParams, userID string) {
	if params.Redirect == "" {
		log.Printf("loginHandler: don't know what to do! %s", params)
		http.Error(w, "don't know what to do!", http.StatusBadRequest)
		return
	}

	http.Redirect(w, req, params.Redirect, http.StatusFound)
}

// The LoginApp provides a traditional cookie-based session approach,
// which protects the login process itself long-term in order to
// provide single sign-on functionality. Integrations with auth
// mechanisms (e.g. OIDC) are then implemented as protected handlers.
//
// The cookie-based workflow is triggered by a request to
// /auth/login/user with the "redir" query argument containing the
// original (protected) URL. You shouldn't be doing this manually: the
// object offers a WithAuth() wrapper method that should be used by
// applications directly tied to the login workflow itself, such as
// the OIDC callback app and the user management app (so that it is
// possible to implement captive workflows outside of the OIDC
// domain).
type LoginApp struct {
	http.Handler

	authSessionStore *sessionstore.Store
}

const PathPrefix = "/auth"

const (
	authSessionCookieName    = "_auth_s"
	loginSessionCookieName   = "_login_s"
	svcListSessionCookieName = "_svclist_s"
)

// Wrap a http.Handler with an authentication web UI backed by
// authClient. The wrapped code can then call GetAuth() to obtain the
// authenticated user's details.
func NewApp(config *Config, features *idp.Features, authClient AuthClient, template *web.Template) (*LoginApp, error) {
	if err := config.sanityCheck(); err != nil {
		return nil, err
	}

	authClient = newTracingAuthClient(
		newServiceBoundAuthClient(authClient, config.AuthService))

	// Create the cookie factories we'll use.

	// Long-term authenticated session of the login app. This
	// session cookie hard-codes a path of /, to ensure that all
	// applications in the current origin can be protected.
	authSessionStore := sessionstore.NewProtobuf(
		authSessionCookieName, "/", config.SessionAuthKey, config.SessionEncKey, 0,
	).WithSecure(!features.Insecure)

	// Short-term state of the login workflow.
	loginSessionStore := sessionstore.NewProtobuf(
		loginSessionCookieName, PathPrefix+"/login/", config.SessionAuthKey, config.SessionEncKey, int(config.LoginSessionLifetime.Seconds()),
	).WithSecure(!features.Insecure)

	// Long-term list of authenticated services (tracked for
	// logout purposes), its scope must include /auth/logout and
	// the SSO application. This is a separate cookie so we can
	// re-authenticate an expired session without losing this
	// information.
	svcListStore := sessionstore.NewProtobuf(
		svcListSessionCookieName, PathPrefix+"/", config.SessionAuthKey, config.SessionEncKey, 0,
	).WithSecure(!features.Insecure)
	svcListManager := newServiceListManager(svcListStore)

	app := &LoginApp{
		authSessionStore: authSessionStore,
	}

	// Build the login/logout handlers.
	loginHandler := new(redirectHandler)
	logoutHandler := newLogoutHandler(authClient, template, svcListManager)

	// Build the login engine, which runs the login workflow application.
	engine := newLoginServer(
		features,
		authSessionStore,
		loginSessionStore,
		authClient,
		loginHandler,
		newDeviceManager(config.DeviceAuthKey, PathPrefix+"/login/", !features.Insecure),
		template,
		int(config.AuthSessionLifetime.Seconds()),
	)

	// Build the final HTTP router.
	mux := http.NewServeMux()
	mux.HandleFunc(PathPrefix+"/login/", func(w http.ResponseWriter, req *http.Request) {
		var auth lpb.Auth
		if authSessionStore.Get(req, &auth) == nil && auth.Valid() {
			// Already logged in.
			if rparams, err := lpb.NewRequestParamsFromRequest(req); err == nil {
				loginHandler.HandleLogin(w, req, rparams, auth.UserId)
				return
			}
		}
		engine.ServeHTTP(w, req)
	})
	mux.HandleFunc(PathPrefix+"/logout", func(w http.ResponseWriter, req *http.Request) {
		authSessionStore.Delete(w) //nolint:errcheck
		logoutHandler.ServeHTTP(w, req)
	})
	app.Handler = mux

	return app, nil
}

// Middleware for the cookie-based workflow, to wrap external
// applications sharing the same origin as the login app.
func (a *LoginApp) WithAuth(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		var auth lpb.Auth
		if err := a.authSessionStore.Get(req, &auth); err == nil {
			// Is the user authenticated?
			if auth.Valid() {
				// Do we have an active captive workflow?
				if auth.IsCaptured() {
					workflow := auth.Captive.Current()
					if workflow == "" {
						finalizeCaptive(w, req, a.authSessionStore, &auth)
						return
					}
					// Capture the current URL into the captive session.
					if auth.Captive.Exit == "" {
						auth.Captive.Exit = req.URL.String()
						a.authSessionStore.Set(w, &auth) //nolint:errcheck
					}
					// Redirect to the current workflow if the requested URL does not match it.
					if !workflows.MatchURL(workflow, req.URL.Path) {
						workflows.Redirect(w, req, workflow)
						return
					}
					req = addAuthStoreToRequest(req, a.authSessionStore)
				}

				// Serve the wrapped handler, with Auth in the context.
				h.ServeHTTP(w, AddAuthToRequest(req, &auth))
				return
			}
		}

		// Redirect to login workflow.
		v := make(url.Values)
		v.Set("redir", req.URL.String())
		http.Redirect(w, req, PathPrefix+"/login/user?"+v.Encode(), http.StatusFound)
	})
}
