package login

import (
	"errors"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"time"

	"git.autistici.org/smol/idp"
	"git.autistici.org/smol/idp/internal/testutil"
	"git.autistici.org/smol/idp/internal/web"
	"github.com/gorilla/securecookie"
)

const dumpCookies = true

type logTransport struct {
	http.RoundTripper
}

func (t *logTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	resp, err := t.RoundTripper.RoundTrip(req)
	if err != nil {
		log.Printf("%s %s -> %v", req.Method, req.URL.String(), err)
	} else {
		var extra string
		if resp.StatusCode >= 300 && resp.StatusCode < 399 {
			extra = fmt.Sprintf(" (%s)", resp.Header.Get("Location"))
		}
		log.Printf("%s %s -> %d%s", req.Method, req.URL.String(), resp.StatusCode, extra)

		if dumpCookies {
			if vv, ok := req.Header["Cookie"]; ok {
				for _, v := range vv {
					log.Printf("  -> Cookie: %s", v)
				}
			}
			if vv, ok := resp.Header["Set-Cookie"]; ok {
				for _, v := range vv {
					log.Printf("  <- Set-Cookie: %s", v)
				}
			}
		}
	}
	return resp, err
}

// In order to abstract the UI away, this test uses its own special
// template format, which represents the page title, the current
// error, and a serialization of the request state. The http client
// then interprets this in order to maintain the RequestParams across
// multiple requests.
//
// The same mechanism makes it easy to verify which page is actually
// being rendered, rather than relying on matching UI elements.
type httpClient struct {
	client  *http.Client
	baseURI string

	requestParams string
}

func newClient(baseURI string) *httpClient {
	jar, _ := cookiejar.New(nil)
	return &httpClient{
		client: &http.Client{
			Jar:       jar,
			Transport: &logTransport{new(http.Transport)},
		},
		baseURI: baseURI,
	}
}

func (c *httpClient) withRequestParams(uri string) string {
	return uri + "?_rp=" + strings.ReplaceAll(c.requestParams, "=", "%3D")
}

func (c *httpClient) formData(values map[string]string) io.Reader {
	vv := make(url.Values)
	for k, v := range values {
		vv.Set(k, v)
	}
	if c.requestParams != "" {
		vv.Set("_rp", c.requestParams)
	}
	return strings.NewReader(vv.Encode())
}

func (c *httpClient) request(method, uri string, r io.Reader) (string, string, error) {
	uri = c.baseURI + uri
	req, err := http.NewRequest(method, uri, r)
	if err != nil {
		return "", "", err
	}
	if r != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return "", "", err
	}
	defer resp.Body.Close()
	switch resp.StatusCode {
	case 200:
		bdata, _ := io.ReadAll(resp.Body)
		data := string(bdata)
		c.requestParams = pageRequestParams(data)
		return data, resp.Request.URL.Path, nil
	default:
		return "", "", fmt.Errorf("HTTP status %d", resp.StatusCode)
	}
}

// Base form of our response page template.
func makeTestPage(title string) string {
	return fmt.Sprintf("%s\n{{.Login.RequestParams.Encode}}\n{{.Error}}", title)
}

// Assertions on the data returned by makeTestPage.
func isPage(data, title string) bool {
	return strings.HasPrefix(data, title+"\n")
}

func pageRequestParams(data string) string {
	l := strings.Split(data, "\n")
	if len(l) == 3 {
		return l[1]
	}
	return ""
}

func pageError(data string) string {
	return strings.Split(data, "\n")[2]
}

var (
	targetURL     = "/target"
	targetContent = "ok"
)

func isTargetWebsite(data string) error {
	if data != targetContent {
		return fmt.Errorf("'%s' != target_content", data)
	}
	return nil
}

func isChangePasswordPage(data string) error {
	if data != "changepw" {
		return fmt.Errorf("'%s' != 'changepw'", data)
	}
	return nil
}

// Quick verification that the above page template scheme works.
func TestTestPages(t *testing.T) {
	p1 := "login\ntag\n"
	if !isPage(p1, "login") {
		t.Errorf("isPage check failed")
	}
	if s := pageError(p1); s != "" {
		t.Errorf("pageError not empty: '%s'", s)
	}

	p2 := "login\ntag\nerror"
	if pageError(p2) == "" {
		t.Errorf("pageError empty")
	}
}

// Start a test server, with the login app wrapping a test
// application. The login app will be using the FakeAuthClient, with
// its own hard-coded fake test users.
func startServer(t *testing.T) (*httptest.Server, AuthClient) {
	ac := new(testutil.FakeAuthClient)

	// Create our custom template hierarchy.
	tpl := template.New("")
	template.Must(tpl.New("login.html").Parse(makeTestPage("login")))
	template.Must(tpl.New("login_webauthn.html").Parse(makeTestPage("login_webauthn")))
	template.Must(tpl.New("login_otp.html").Parse(makeTestPage("login_otp")))
	template.Must(tpl.New("login_recovery.html").Parse(makeTestPage("login_recovery")))
	uitpl := &web.Template{Template: tpl}

	config := Config{
		AuthService:         "service",
		AuthSessionLifetime: 1 * time.Hour,
		DeviceAuthKey:       securecookie.GenerateRandomKey(64),
		SessionAuthKey:      securecookie.GenerateRandomKey(64),
		SessionEncKey:       securecookie.GenerateRandomKey(32),
	}
	features := &idp.Features{
		Insecure:              true,
		EnableOTP:             true,
		EnableWebAuthn:        true,
		EnableAccountRecovery: true,
	}

	app, err := NewApp(&config, features, ac, uitpl)
	if err != nil {
		t.Fatalf("NewApp: %v", err)
	}

	mux := http.NewServeMux()
	mux.Handle("/auth/", app)
	mux.HandleFunc("/change-password", func(w http.ResponseWriter, _ *http.Request) {
		io.WriteString(w, "changepw") //nolint:errcheck
	})
	RegisterWorkflow("change-password", "/change-password")
	mux.Handle(targetURL, app.WithAuth(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		io.WriteString(w, targetContent) //nolint:errcheck
	})))

	return httptest.NewServer(mux), ac
}

type step struct {
	requestFunc func(*httpClient) (string, string, error)
	checkFunc   func(string) error
	expectedURI string
	expectErr   bool
}

func runStep(t *testing.T, c *httpClient, idx int, step step) {
	t.Helper()

	data, uri, err := step.requestFunc(c)
	if err != nil && !step.expectErr {
		t.Fatalf("error at step %d: unexpected error: %v", idx, err)
	}
	if err == nil && step.expectErr {
		t.Fatalf("error at step %d: was expecting an error but did not get one", idx)
	}
	if step.expectedURI != "" && step.expectedURI != uri {
		t.Errorf("error at step %d: bad response URI, got '%s' expected '%s'", idx, uri, step.expectedURI)
	}
	if step.checkFunc != nil {
		if err := step.checkFunc(data); err != nil {
			t.Fatalf("error at step %d: bad response: %v\n%s", idx, err, data)
		}
	}
}

func runConversation(t *testing.T, steps ...step) {
	t.Helper()

	srv, _ := startServer(t)
	defer srv.Close()

	c := newClient(srv.URL)

	for idx, step := range steps {
		runStep(t, c, idx+1, step)
	}
}

func expectPage(name string) func(string) error {
	return func(data string) error {
		if !isPage(data, name) {
			return fmt.Errorf("page != '%s'", name)
		}
		if s := pageError(data); s != "" {
			return fmt.Errorf("page error=%s", s)
		}
		return nil
	}
}

func expectPageError(name string) func(string) error {
	return func(data string) error {
		if !isPage(data, name) {
			return fmt.Errorf("page != '%s'", name)
		}
		if s := pageError(data); s == "" {
			return errors.New("request did not fail as expected")
		}
		return nil
	}
}

// Cookie-based login workflow.

func requestSiteStep() step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("GET", targetURL, nil)
		},
		checkFunc:   expectPage("login"),
		expectedURI: "/auth/login/user",
	}
}

func loginStep(username, password, expURI string, checkFunc func(string) error) step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("POST", "/auth/login/user", c.formData(map[string]string{
				"username": username,
				"password": password,
			}))
		},
		checkFunc:   checkFunc,
		expectedURI: expURI,
	}
}

func loginOKStep() step {
	return loginStep("test", "password", targetURL, isTargetWebsite)
}

func TestLogin_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOKStep(),
	)
}

func TestLogin_Fail(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginStep("test", "wrong-password", "/auth/login/user", expectPageError("login")),
	)
}

func TestLogin_Ok_ShortCircuit(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOKStep(),
		// Now ask for the login page again, expect to be
		// redirected directly to the main site (not the
		// target).
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("GET", "/auth/login/user?redir=/target", nil)
			},
			expectedURI: "/target",
		},
	)
}

func loginOTPStep1() step {
	return loginStep("test-otp", "password", "/auth/login/otp", expectPage("login_otp"))
}

func loginOTPStep2(otp, expURI string, checkFunc func(string) error) step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("POST", "/auth/login/otp", c.formData(map[string]string{
				"otp": otp,
			}))
		},
		checkFunc:   checkFunc,
		expectedURI: expURI,
	}
}

func loginOTPOKStep(otp string) step {
	return loginOTPStep2(otp, targetURL, isTargetWebsite)
}

func TestLogin_OTP_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOTPStep1(),
		loginOTPOKStep("123456"),
	)
}

func TestLogin_OTP_Fail(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOTPStep1(),
		loginOTPStep2("234567", "/auth/login/user", expectPageError("login")),
	)
}

func TestLogin_OTP_FailBadPassword(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginStep("test-otp", "bad password", "/auth/login/otp", expectPage("login_otp")),
		loginOTPStep2("234567", "/auth/login/user", expectPageError("login")),
	)
}

func TestLogin_OTP_FailSkipPassword(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOTPStep2("234567", "/auth/login/user", expectPageError("login")),
	)
}

func TestLogin_OTP_FailSwitchToU2F(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginOTPStep1(),
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("GET", "/auth/login/webauthn", nil)
			},
			checkFunc:   expectPageError("login"),
			expectedURI: "/auth/login/user",
		},
	)
}

func loginWebAuthnStep1() step {
	return loginStep("test-webauthn", "password", "/auth/login/webauthn", expectPage("login_webauthn"))
}

func loginWebAuthnStep2() step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("POST", "/auth/login/webauthn", c.formData(map[string]string{
				"webauthn": testCredentialRequestBody,
			}))
		},
		checkFunc: isTargetWebsite,
	}
}

func TestLogin_WebAuthn_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginWebAuthnStep1(),
		loginWebAuthnStep2(),
	)
}

func TestLogin_WebAuthn_Fail(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginWebAuthnStep1(),
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("POST", "/auth/login/webauthn", c.formData(map[string]string{
					"webauthn": "foo",
				}))
			},
			checkFunc: expectPageError("login"),
		},
	)
}

func TestLogin_2FA_Switch_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		loginStep("test-both", "password", "/auth/login/webauthn", expectPage("login_webauthn")),
		// Switch to OTP directly.
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("GET", c.withRequestParams("/auth/login/otp"), nil)
			},
			checkFunc: expectPage("login_otp"),
		},
		loginOTPOKStep("123456"),
	)
}

func recoveryStart() step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("GET", c.withRequestParams("/auth/login/recovery"), nil)
		},
		checkFunc: expectPage("login_recovery"),
	}
}

func recoveryChooseUsername() step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("POST", "/auth/login/recovery", c.formData(map[string]string{
				"username": "test",
			}))
		},
		checkFunc: expectPage("login_recovery"),
	}
}

func recoveryChooseCodeMech() step {
	return step{
		requestFunc: func(c *httpClient) (string, string, error) {
			return c.request("POST", "/auth/login/recovery", c.formData(map[string]string{
				"mechanism": "code",
			}))
		},
		checkFunc: expectPage("login_recovery"),
	}
}

func TestLogin_Recovery_Ok(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		recoveryStart(),
		recoveryChooseUsername(),
		recoveryChooseCodeMech(),
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("POST", "/auth/login/recovery", c.formData(map[string]string{
					"mechanism": "code",
					"code":      "recovery",
				}))
			},
			// Expect to be sent to the change-password workflow.
			checkFunc: isChangePasswordPage,
		},
	)
}

func TestLogin_Recovery_Fail(t *testing.T) {
	runConversation(t,
		requestSiteStep(),
		recoveryStart(),
		recoveryChooseUsername(),
		recoveryChooseCodeMech(),
		step{
			requestFunc: func(c *httpClient) (string, string, error) {
				return c.request("POST", "/auth/login/recovery", c.formData(map[string]string{
					"mechanism": "code",
					"code":      "123456",
				}))
			},
			checkFunc: expectPageError("login_recovery"),
		},
	)
}

var testCredentialRequestBody = `{
	"id":"6xrtBhJQW6QU4tOaB4rrHaS2Ks0yDDL_q8jDC16DEjZ-VLVf4kCRkvl2xp2D71sTPYns-exsHQHTy3G-zJRK8g",
	"rawId":"6xrtBhJQW6QU4tOaB4rrHaS2Ks0yDDL_q8jDC16DEjZ-VLVf4kCRkvl2xp2D71sTPYns-exsHQHTy3G-zJRK8g",
	"type":"public-key",
	"response":{
                "authenticatorData":"dKbqkhPJnC90siSSsyDPQCYqlMGpUKA5fyklC2CEHvBFXJJiGa3OAAI1vMYKZIsLJfHwVQMANwCOw-atj9C0vhWpfWU-whzNjeQS21Lpxfdk_G-omAtffWztpGoErlNOfuXWRqm9Uj9ANJck1p6lAQIDJiABIVggKAhfsdHcBIc0KPgAcRyAIK_-Vi-nCXHkRHPNaCMBZ-4iWCBxB8fGYQSBONi9uvq0gv95dGWlhJrBwCsj_a4LJQKVHQ",
		"clientDataJSON":"eyJjaGFsbGVuZ2UiOiJFNFBUY0lIX0hmWDFwQzZTaWdrMVNDOU5BbGdlenROMDQzOXZpOHpfYzlrIiwibmV3X2tleXNfbWF5X2JlX2FkZGVkX2hlcmUiOiJkbyBub3QgY29tcGFyZSBjbGllbnREYXRhSlNPTiBhZ2FpbnN0IGEgdGVtcGxhdGUuIFNlZSBodHRwczovL2dvby5nbC95YWJQZXgiLCJvcmlnaW4iOiJodHRwczovL3dlYmF1dGhuLmlvIiwidHlwZSI6IndlYmF1dGhuLmdldCJ9",
		"signature":"MEUCIBtIVOQxzFYdyWQyxaLR0tik1TnuPhGVhXVSNgFwLmN5AiEAnxXdCq0UeAVGWxOaFcjBZ_mEZoXqNboY5IkQDdlWZYc"
		}
	}`
