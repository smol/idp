package login

import (
	"context"
	"net/http"
	"strings"

	"git.autistici.org/smol/idp/internal/sessionstore"
	lpb "git.autistici.org/smol/idp/login/proto"
)

type authStoreKeyType int

const authStoreKey authStoreKeyType = 0

func getAuthStore(ctx context.Context) (*sessionstore.Store, bool) {
	s, ok := ctx.Value(authStoreKey).(*sessionstore.Store)
	return s, ok
}

func addAuthStoreToRequest(req *http.Request, store *sessionstore.Store) *http.Request {
	return req.WithContext(context.WithValue(req.Context(), authStoreKey, store))
}

func CompleteWorkflow(w http.ResponseWriter, req *http.Request, workflow string) {
	auth, ok := GetAuth(req.Context())
	if !ok {
		return
	}

	if !auth.IsCaptured() || auth.Captive.Current() != workflow {
		return
	}

	store, ok := getAuthStore(req.Context())
	if !ok {
		return
	}

	auth.Captive = auth.Captive.Done()

	store.Set(w, auth) //nolint:errcheck
}

func finalizeCaptive(w http.ResponseWriter, req *http.Request, store *sessionstore.Store, auth *lpb.Auth) {
	nextURL := auth.Captive.Exit
	auth.Captive = nil
	store.Set(w, auth) //nolint:errcheck
	http.Redirect(w, req, nextURL, http.StatusFound)
}

type workflowMap map[string][]string

var workflows = make(workflowMap)

func RegisterWorkflow(workflow string, paths ...string) {
	workflows[workflow] = paths
}

func (m workflowMap) MatchURL(workflow, path string) bool {
	return matchURLToList(path, m[workflow])
}

func (m workflowMap) Redirect(w http.ResponseWriter, req *http.Request, workflow string) {
	uri := m[workflow][0]
	http.Redirect(w, req, uri, http.StatusFound)
}

func matchURLToList(path string, l []string) bool {
	for _, pfx := range l {
		if (!strings.HasSuffix(pfx, "/") && path == pfx) ||
			(strings.HasSuffix(pfx, "/") && strings.HasPrefix(path, pfx)) {
			return true
		}
	}
	return false
}
