package login

import (
	"context"
	"net/http"
	"strings"

	authpb "git.autistici.org/smol/idp/authn/proto"
	lpb "git.autistici.org/smol/idp/login/proto"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
)

func normalizeUsername(s string) string {
	return strings.ToLower(strings.TrimSpace(s))
}

type grpcAuthClientWrapper struct {
	authpb.AuthNClient
}

func (w *grpcAuthClientWrapper) Logout(_ context.Context, _ *lpb.Auth) error {
	return nil
}

// NewAuthClient creates a login.AuthClient from a grpc connection.
func NewAuthClient(client authpb.AuthNClient) AuthClient {
	return &grpcAuthClientWrapper{client}
}

// AuthClient wrapper that is bound to a specific service.
type serviceBoundAuthClient struct {
	AuthClient

	service string
}

func newServiceBoundAuthClient(c AuthClient, service string) *serviceBoundAuthClient {
	return &serviceBoundAuthClient{
		AuthClient: c,
		service:    service,
	}
}

func (c *serviceBoundAuthClient) Authenticate(ctx context.Context, req *authpb.Request, opts ...grpc.CallOption) (*authpb.Response, error) {
	req.Service = c.service
	return c.AuthClient.Authenticate(ctx, req, opts...)
}

// AuthClient wrapper that annotates the RPC trace with authentication call spans.
type tracingAuthClient struct {
	AuthClient
}

func newTracingAuthClient(c AuthClient) *tracingAuthClient {
	return &tracingAuthClient{AuthClient: c}
}

func (c *tracingAuthClient) Authenticate(ctx context.Context, req *authpb.Request, opts ...grpc.CallOption) (*authpb.Response, error) {
	// Trace the authentication request.
	ctx, span := otel.GetTracerProvider().Tracer("login").Start(
		ctx, "auth", trace.WithSpanKind(trace.SpanKindClient), trace.WithAttributes(
			attribute.String("auth.user", req.Username),
			attribute.String("auth.service", req.Service),
			attribute.String("auth.mechanism", req.Mechanism.String()),
		))
	defer span.End()

	resp, err := c.AuthClient.Authenticate(ctx, req, opts...)

	// Record the authentication response status in the trace.
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
	} else if resp.Status == authpb.Response_STATUS_OK {
		span.SetStatus(codes.Ok, "OK")
	} else {
		span.SetStatus(codes.Error, resp.Status.String())
	}

	return resp, err
}

func setCSP(w http.ResponseWriter, csp string) {
	w.Header().Set("Content-Security-Policy", csp)
}
