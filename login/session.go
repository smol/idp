package login

import (
	"log"
	"net/http"

	"git.autistici.org/smol/idp/internal/random"
	pb "git.autistici.org/smol/idp/proto"
	"github.com/gorilla/securecookie"
	"github.com/mssola/user_agent"
)

const (
	deviceIDSize         = 32
	deviceIDCookie       = "_did"
	deviceIDCookieMaxAge = 10 * 365 * 86400
)

// The device manager tracks long-term 'devices', i.e. users across
// multiple sessions, with a cookie.
type deviceManager struct {
	sc         *securecookie.SecureCookie
	cookiePath string
	secure     bool
}

func newDeviceManager(authKey []byte, path string, secure bool) *deviceManager {
	// Cookie has authentication but does not need encryption as
	// its contents are opaque.
	sc := securecookie.New(authKey, nil)
	sc.MaxAge(deviceIDCookieMaxAge)
	sc.SetSerializer(securecookie.NopEncoder{})

	return &deviceManager{
		sc:         sc,
		cookiePath: path,
		secure:     secure,
	}
}

func (m *deviceManager) deviceInfoFromRequest(w http.ResponseWriter, req *http.Request) *pb.DeviceInfo {
	devID, ok := m.getDeviceCookie(req)
	if !ok || len(devID) == 0 {
		// Generate a new Device ID and save it on the client.
		devID = randomDeviceID()
		if err := m.setDeviceCookie(w, devID); err != nil {
			// This is likely a misconfiguration issue, so
			// we want to know about it.
			log.Printf("error saving device manager session: %v", err)
		}
	}

	uaStr := req.UserAgent()
	ua := user_agent.New(uaStr)
	browser, _ := ua.Browser()
	return &pb.DeviceInfo{
		Id:        devID,
		UserAgent: uaStr,
		Browser:   browser,
		Os:        ua.OS(),
		Mobile:    ua.Mobile(),
	}
}

func (m *deviceManager) getDeviceCookie(r *http.Request) (string, bool) {
	if cookie, err := r.Cookie(deviceIDCookie); err == nil && cookie.Value != "" {
		var value []byte
		if err = m.sc.Decode(deviceIDCookie, cookie.Value, &value); err == nil {
			return string(value), true
		} else {
			log.Printf("getDeviceCookie.Decode() error: %v", err)
		}
	}
	return "", false
}

func (m *deviceManager) setDeviceCookie(w http.ResponseWriter, value string) error {
	encoded, err := m.sc.Encode(deviceIDCookie, []byte(value))
	if err != nil {
		return err
	}
	cookie := &http.Cookie{
		Name:     deviceIDCookie,
		Value:    encoded,
		Path:     m.cookiePath,
		Secure:   m.secure,
		HttpOnly: true,
		MaxAge:   deviceIDCookieMaxAge,
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, cookie)
	return nil
}

func randomDeviceID() string {
	return random.Hex(deviceIDSize)
}
