package login

import (
	"log"
	"net/http"

	"git.autistici.org/smol/idp/internal/web"
)

type logoutData struct {
	ServiceList   []string
	LoginAgainURL string
}

type logoutHandler struct {
	slm        *serviceListManager
	template   *web.Template
	authClient AuthClient

	loginAgainURL string
}

func newLogoutHandler(authClient AuthClient, template *web.Template, slm *serviceListManager) *logoutHandler {
	return &logoutHandler{
		slm:        slm,
		template:   template,
		authClient: authClient,
	}
}

func (h *logoutHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	svcList := h.slm.getServiceList(req)
	if err := h.slm.del(w); err != nil {
		log.Printf("logout error: deleting service_list cookie: %v", err)
	}

	// If the user is autenticated, invoke the Logout
	// method on the authentication client.
	if auth, ok := GetAuth(req.Context()); ok && auth.Valid() {
		if err := h.authClient.Logout(req.Context(), auth); err != nil {
			log.Printf("logout error for %s: %v", auth.UserId, err)
		}
	}

	setCSP(w, logoutCSP)
	h.template.WithData(map[string]any{
		"Logout": &logoutData{
			ServiceList:   svcList,
			LoginAgainURL: h.loginAgainURL,
		},
	}).Execute(w, req, "logout.html") //nolint:errcheck
}
