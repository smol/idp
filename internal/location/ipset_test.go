package location

import (
	"net"
	"os"
	"path/filepath"
	"testing"
)

var testIPs = `192.168.1.0/24
172.16.0.0/16
10.0.0.0/8
2.3.4.5
2a0b:f4c2:0002:0000:0000:0000:0000:0060
`

func TestIPSet_Mapper(t *testing.T) {
	dir, err := os.MkdirTemp("", "ipset-test-*")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	ipListPath := filepath.Join(dir, "ips")
	os.WriteFile(ipListPath, []byte(testIPs), 0600) // nolint: errcheck

	r, err := newZoneMapper(&ZoneMapSpec{
		Type:  "ipset",
		Value: "foo",
		Path:  ipListPath,
	})
	if err != nil {
		t.Fatal(err)
	}

	for _, td := range []struct {
		ip string
		ok bool
	}{
		{"192.168.1.128", true},
		{"10.18.55.42", true},
		{"1.2.3.4", false},
		{"2.3.4.5", true},
		{"2a0b:f4c2:0002:0000:0000:0000:0000:0050", false},
		{"2a0b:f4c2:0002:0000:0000:0000:0000:0060", true},
		{"2a0b:f4c2:2::60", true},
	} {
		zone, ok := r.Lookup(net.ParseIP(td.ip))
		if ok != td.ok {
			t.Errorf("bad result for %s: %s, %v", td.ip, zone, err)
		}
		if ok && zone != "foo" {
			t.Errorf("bad zone for %s: %v", td.ip, zone)
		}
	}

}
