package location

import (
	"fmt"
	"net"
	"net/http"
)

type zoneMapper interface {
	Lookup(net.IP) (string, bool)
}

// ZoneMapSpec describes ip -> zone lookup maps.
type ZoneMapSpec struct {
	Type           string   `yaml:"type"`
	GeoIPDataFiles []string `yaml:"geoip_data_files"`
	Path           string   `yaml:"path"`
	Value          string   `yaml:"value"`
}

func newZoneMapper(spec *ZoneMapSpec) (zoneMapper, error) {
	switch spec.Type {
	case "geoip":
		return newGeoIPCountry(spec.GeoIPDataFiles)
	case "ipset":
		return newIPSetFileZoneMapper(spec.Path, spec.Value)
	default:
		return nil, fmt.Errorf("unknown zone map type '%s'", spec.Type)
	}
}

type Locator struct {
	zoneMappers []zoneMapper
}

func New(zoneMaps []*ZoneMapSpec) (*Locator, error) {
	// Instantiate all our IP -> zone lookup tables. Reminder that
	// GeoIP should go last because it always has an answer.
	zoneMappers := []zoneMapper{
		new(privateRangeDetector),
	}
	for _, spec := range zoneMaps {
		m, err := newZoneMapper(spec)
		if err != nil {
			return nil, fmt.Errorf("error in zone_maps: %w", err)
		}
		zoneMappers = append(zoneMappers, m)
	}

	return &Locator{
		zoneMappers: zoneMappers,
	}, nil
}

func (l *Locator) Lookup(ip net.IP) (string, bool) {
	for _, zm := range l.zoneMappers {
		if zone, ok := zm.Lookup(ip); ok {
			return zone, true
		}
	}
	return "", false
}

func (l *Locator) LookupRequest(req *http.Request) (string, bool) {
	// We can trust net/http to resolve X-Forwarded-For and
	// similar headers to req.RemoteAddr.
	//
	// Attempt to drop an eventual :PORT suffix.
	host, _, err := net.SplitHostPort(req.RemoteAddr)
	if err != nil {
		host = req.RemoteAddr
	}

	ip := net.ParseIP(host)
	if ip == nil {
		return "", false
	}

	return l.Lookup(ip)
}

const (
	localZone    = "local"
	internalZone = "internal"
)

// Detect loopback / private IP ranges and map them to special zones.
type privateRangeDetector struct{}

func (*privateRangeDetector) Lookup(ip net.IP) (string, bool) {
	if ip.IsLoopback() {
		return localZone, true
	}
	if ip.IsPrivate() || ip.IsLinkLocalUnicast() || !ip.IsGlobalUnicast() {
		return internalZone, true
	}
	return "", false
}
