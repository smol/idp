package location

import (
	"net"
	"os"

	"github.com/oschwald/maxminddb-golang"
)

var defaultGeoIPPaths = []string{
	"/var/lib/GeoIP/GeoLite2-Country.mmdb",
}

// Generic MaxMind GeoIP resolver.
type geoIPDb struct {
	readers []*maxminddb.Reader
}

func newGeoIP(paths []string) (*geoIPDb, error) {
	if len(paths) == 0 {
		paths = defaultGeoIPPaths
	}

	db := new(geoIPDb)
	for _, path := range paths {
		// Only attempt to load zone files if they actually exist.
		if _, err := os.Stat(path); os.IsNotExist(err) {
			continue
		}
		geodb, err := maxminddb.Open(path)
		if err != nil {
			return nil, err
		}
		db.readers = append(db.readers, geodb)
	}

	return db, nil
}

// Specific GeoIP resolver for country-level data.
type geoIPCountry struct {
	*geoIPDb
}

func newGeoIPCountry(paths []string) (*geoIPCountry, error) {
	db, err := newGeoIP(paths)
	if err != nil {
		return nil, err
	}
	return &geoIPCountry{geoIPDb: db}, nil
}

func (db *geoIPCountry) Lookup(ip net.IP) (string, bool) {
	// Only look up a single attribute (country).
	var record struct {
		Country struct {
			ISOCode string `maxminddb:"iso_code"`
		} `maxminddb:"country"`
	}

	for _, r := range db.readers {
		if err := r.Lookup(ip, &record); err == nil {
			return record.Country.ISOCode, true
		}
	}

	return "", false
}
