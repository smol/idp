package testutil

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"git.autistici.org/smol/idp/userdb"
	sqlbackend "git.autistici.org/smol/idp/userdb/backend/sql"
	"gopkg.in/yaml.v3"
)

func buildSQLUserdbSpec(driver, dburi string) *userdb.Spec {
	s := fmt.Sprintf("driver: \"%s\"\ndburi: \"%s\"\n", driver, dburi)
	var n yaml.Node
	yaml.Unmarshal([]byte(s), &n) //nolint:errcheck
	return &userdb.Spec{
		Type:   "sql",
		Params: n,
	}
}

func execStatements(dburi string, sqlstr string) error {
	db, err := sql.Open("sqlite3", dburi)
	if err != nil {
		return err
	}
	defer db.Close()

	for _, stmt := range strings.Split(sqlstr, ";\n") {
		stmt = strings.TrimSpace(stmt)
		if stmt == "" {
			continue
		}
		if _, err := db.Exec(stmt); err != nil {
			return fmt.Errorf("sql error in statement \"%s\": %w", stmt, err)
		}
	}

	return nil
}

func CreateTestDB(fixtures, dir string) (userdb.Backend, func(), error) {
	dbdir := dir
	if dbdir == "" {
		tmpDir, err := os.MkdirTemp("", "")
		if err != nil {
			return nil, nil, err
		}
		dbdir = tmpDir
	}

	dburi := filepath.Join(dbdir, "user.db")

	b, err := userdb.New(buildSQLUserdbSpec("sqlite3", dburi), "")
	if err != nil {
		return nil, nil, err
	}

	if err := execStatements(dburi, sqlbackend.DefaultSchema); err != nil {
		return nil, nil, err
	}
	if err := execStatements(dburi, fixtures); err != nil {
		return nil, nil, err
	}

	return b, func() {
		b.Close()
		if dir == "" {
			_ = os.RemoveAll(dbdir)
		}
	}, nil
}
