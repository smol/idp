package testutil

import (
	"context"
	"log"

	authpb "git.autistici.org/smol/idp/authn/proto"
	lpb "git.autistici.org/smol/idp/login/proto"
	pb "git.autistici.org/smol/idp/proto"
	"google.golang.org/grpc"
)

var (
	fakeWebauthnSession          = "webauthn_session"
	fakeWebauthnEncodedAssertion = "{\"Challenge\":\"webauthn_challenge\"}"
	testOrigin                   = "https://webauthn.io"
)

func validWebAuthnRequest(req *authpb.Request) bool {
	// We don't really perform a WebAuthN validation, just verify
	// that the session data is round-tripped correctly through
	// the login app.
	if req.WebauthnSessionId != fakeWebauthnSession {
		return false
	}

	a, err := req.GetWebAuthnAssertion()
	if err != nil {
		return false
	}
	if a.Response.CollectedClientData.Origin != testOrigin {
		log.Printf("bad webauthn assertion: '%s'", req.WebauthnEncodedAssertion)
		return false
	}
	return true
}

type FakeAuthClient struct{}

//nolint:goconst
func (c *FakeAuthClient) Authenticate(ctx context.Context, req *authpb.Request, _ ...grpc.CallOption) (*authpb.Response, error) {
	p := req.Password
	info := &pb.UserInfo{
		Name:   "test",
		Email:  "test@example.com",
		Groups: []string{"users"},
	}
	log.Printf("fake_auth: %+v", req)
	switch {
	case req.Username == "test" && p == "password" && req.Mechanism == authpb.Mechanism_PASSWORD:
		return &authpb.Response{Status: authpb.Response_STATUS_OK, UserId: "1", UserInfo: info}, nil

	case req.Username == "test-otp" && p == "password" && req.Otp == "123456" && req.Mechanism == authpb.Mechanism_OTP:
		return &authpb.Response{Status: authpb.Response_STATUS_OK, UserId: "2", UserInfo: info}, nil
	case req.Username == "test-otp" && req.Otp == "":
		return &authpb.Response{
			Status:              authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS,
			AvailableMechanisms: []authpb.Mechanism{authpb.Mechanism_OTP},
		}, nil

	case req.Username == "test-webauthn" && p == "password" && validWebAuthnRequest(req) && req.Mechanism == authpb.Mechanism_WEBAUTHN:
		return &authpb.Response{Status: authpb.Response_STATUS_OK, UserId: "3", UserInfo: info}, nil
	case req.Username == "test-webauthn" && req.WebauthnSessionId == "":
		return &authpb.Response{
			Status:                   authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS,
			AvailableMechanisms:      []authpb.Mechanism{authpb.Mechanism_WEBAUTHN},
			WebauthnSessionId:        fakeWebauthnSession,
			WebauthnEncodedAssertion: fakeWebauthnEncodedAssertion,
		}, nil

	case req.Username == "test-both" && p == "password" && req.Otp == "123456" && req.Mechanism == authpb.Mechanism_OTP:
		return &authpb.Response{Status: authpb.Response_STATUS_OK, UserId: "4", UserInfo: info}, nil
	case req.Username == "test-both" && p == "password" && validWebAuthnRequest(req) && req.Mechanism == authpb.Mechanism_WEBAUTHN:
		return &authpb.Response{Status: authpb.Response_STATUS_OK, UserId: "4", UserInfo: info}, nil
	case req.Username == "test-both" && req.WebauthnSessionId == "" && req.Otp == "":
		return &authpb.Response{
			Status:                   authpb.Response_STATUS_INSUFFICIENT_CREDENTIALS,
			AvailableMechanisms:      []authpb.Mechanism{authpb.Mechanism_WEBAUTHN, authpb.Mechanism_OTP},
			WebauthnSessionId:        fakeWebauthnSession,
			WebauthnEncodedAssertion: fakeWebauthnEncodedAssertion,
		}, nil

	case req.Username == "test" && req.Password == "recovery" && req.Mechanism == authpb.Mechanism_RECOVERY_TOKEN:
		return &authpb.Response{
			Status:          authpb.Response_STATUS_OK,
			UserId:          "1",
			UserInfo:        info,
			ForcedWorkflows: []string{"change-password"},
		}, nil
	}

	return &authpb.Response{Status: authpb.Response_STATUS_ERROR}, nil
}

func (c *FakeAuthClient) Logout(_ context.Context, _ *lpb.Auth) error {
	return nil
}
