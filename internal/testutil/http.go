package testutil

import (
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

type logTransport struct {
	http.RoundTripper
}

func (t *logTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	resp, err := t.RoundTripper.RoundTrip(req)
	if err != nil {
		log.Printf("%s %s -> %v", req.Method, req.URL.String(), err)
	} else {
		var redir string
		if resp.StatusCode >= 300 && resp.StatusCode < 400 {
			redir = resp.Header.Get("Location")
		}
		log.Printf("%s %s -> %d %s", req.Method, req.URL.String(), resp.StatusCode, redir)

		if v := req.Header.Values("Cookie"); len(v) > 0 {
			log.Printf("  cookies >> %s", strings.Join(v, ", "))
		}
		if v := resp.Header.Values("Set-Cookie"); len(v) > 0 {
			log.Printf("  cookies << %s", strings.Join(v, ", "))
		}
	}
	return resp, err
}

type HTTPClient struct {
	*http.Client
}

func NewClient() *HTTPClient {
	jar, _ := cookiejar.New(nil)
	return &HTTPClient{
		Client: &http.Client{
			Jar: jar,
			Transport: &logTransport{
				&http.Transport{
					TLSClientConfig: &tls.Config{
						InsecureSkipVerify: true, //nolint:gosec
					},
				},
			},
		},
	}
}

func (c *HTTPClient) FormData(values map[string]string) io.Reader {
	vv := make(url.Values)
	for k, v := range values {
		vv.Set(k, v)
	}
	return strings.NewReader(vv.Encode())
}

func (c *HTTPClient) Request(method, uri string, r io.Reader) (string, *url.URL, error) {
	req, err := http.NewRequest(method, uri, r)
	if err != nil {
		return "", nil, err
	}
	if r != nil {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	resp, err := c.Client.Do(req)
	if err != nil {
		return "", nil, err
	}
	defer resp.Body.Close()
	switch resp.StatusCode {
	case 200:
		bdata, _ := io.ReadAll(resp.Body)
		data := string(bdata)
		return data, resp.Request.URL, nil
	default:
		return "", nil, fmt.Errorf("HTTP status %d", resp.StatusCode)
	}
}

type Form struct {
	Action *url.URL
	Method string
	Fields url.Values
}

func ParseForm(uri *url.URL, body string) (*Form, error) {
	doc, err := goquery.NewDocumentFromReader(strings.NewReader(body))
	if err != nil {
		return nil, err
	}

	form := Form{
		Method: "GET",
		Fields: make(url.Values),
	}

	doc.Find("form").Each(func(_ int, f *goquery.Selection) {
		if action, ok := f.Attr("action"); ok {
			actionURL, _ := url.Parse(action)
			form.Action = uri.ResolveReference(actionURL)
		}
		if method, ok := f.Attr("method"); ok {
			form.Method = strings.ToUpper(method)
		}
		f.Find("input").Each(func(i int, s *goquery.Selection) {
			name, ok := s.Attr("name")
			if !ok {
				return
			}
			value, ok := s.Attr("value")
			if !ok {
				return
			}
			form.Fields.Add(name, value)
		})
	})

	return &form, nil
}

// Rough approximation of a browser, used for testing.
type UserAgent struct {
	*HTTPClient
	cur  *url.URL
	data string
}

func NewUserAgent() *UserAgent {
	return &UserAgent{
		HTTPClient: NewClient(),
	}
}

func (u *UserAgent) Request(method, uri string, r io.Reader) error {
	data, newURI, err := u.HTTPClient.Request(method, uri, r)
	if err != nil {
		return err
	}
	u.cur = newURI
	u.data = data
	return nil
}

func (u *UserAgent) SubmitForm(extraValues map[string]string) error {
	form, err := ParseForm(u.cur, u.data)
	if err != nil {
		return err
	}

	for k, v := range extraValues {
		form.Fields.Set(k, v)
	}

	return u.Request(form.Method, form.Action.String(), strings.NewReader(form.Fields.Encode()))
}

func (u *UserAgent) URL() *url.URL {
	return u.cur
}

func (u *UserAgent) Data() string {
	return u.data
}
