package sessionstore

import "google.golang.org/protobuf/proto"

// ProtobufEncoder is a securecookie.Serializer that deals with protobuf messages.
type ProtobufEncoder struct{}

func (p *ProtobufEncoder) Serialize(src interface{}) ([]byte, error) {
	m := src.(proto.Message)
	return proto.Marshal(m)
}

func (p *ProtobufEncoder) Deserialize(src []byte, dst interface{}) error {
	m := dst.(proto.Message)
	return proto.Unmarshal(src, m)
}

// NewProtobuf returns a new Store with protobuf encoder.
func NewProtobuf(name, path string, authKey, encKey []byte, maxAge int) *Store {
	return New(name, path, authKey, encKey, maxAge).WithSerializer(&ProtobufEncoder{})
}
