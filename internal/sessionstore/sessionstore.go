package sessionstore

import (
	"net/http"
	"strings"

	"github.com/gorilla/securecookie"
)

// A SessionStore is a slightly simpler version of gorilla
// sessions.Store, without the global session registry, serializing
// specific types rather than generic interfaces. Each Store manages a
// single session cookie.
type Store struct {
	name   string
	path   string
	secure bool

	sc *securecookie.SecureCookie
}

func New(name, path string, authKey, encKey []byte, maxAge int) *Store {
	sc := securecookie.New(authKey, encKey)

	// Set the securecookie time-to-live (0 disables this check).
	sc.MaxAge(maxAge)

	// The JSON encoder generates smaller data than gob in our case.
	sc.SetSerializer(&securecookie.JSONEncoder{})

	return &Store{
		name:   name,
		path:   trimSlash(path),
		sc:     sc,
		secure: true,
	}
}

// WithSecure allows to explicitly toggle the 'Secure' bit on cookies.
func (s *Store) WithSecure(secure bool) *Store {
	s.secure = secure
	return s
}

// WithSerializer allows changing the default JSON serializer.
func (s *Store) WithSerializer(serializer securecookie.Serializer) *Store {
	s.sc.SetSerializer(serializer)
	return s
}

// Get decodes the current session, if any, into 'obj'.
func (s *Store) Get(req *http.Request, obj interface{}) error {
	cookie, err := req.Cookie(s.name)
	if err != nil {
		return err
	}
	return s.sc.Decode(s.name, cookie.Value, obj)
}

// Set the session value.
func (s *Store) Set(w http.ResponseWriter, obj interface{}) error {
	return s.setSessionCookie(w, obj, false)
}

// Delete the current session cookie.
func (s *Store) Delete(w http.ResponseWriter) error {
	return s.setSessionCookie(w, nil, true)
}

func (s *Store) setSessionCookie(w http.ResponseWriter, obj interface{}, shouldDelete bool) error {
	// The session cookie will always have MaxAge==0 (delete on
	// browser exit), regardless of whether the session contains an
	// internal timestamp or not.
	cookie := &http.Cookie{
		Name:     s.name,
		Path:     s.path,
		HttpOnly: true,
		Secure:   s.secure,
		SameSite: http.SameSiteStrictMode,
	}
	if shouldDelete {
		cookie.MaxAge = -1
	} else {
		enc, err := s.sc.Encode(s.name, obj)
		if err != nil {
			return err
		}
		cookie.Value = enc
	}

	http.SetCookie(w, cookie)

	return nil
}

func trimSlash(s string) string {
	if s == "/" {
		return s
	}
	return strings.TrimRight(s, "/")
}
