package memcache

import (
	"context"
	"errors"
	"sync"
	"sync/atomic"
	"time"
)

// ReplicatedClient is a client for a cluster of memcache servers,
// which performs replication of the data in order to maximize the
// statistical probability of successful retrieval at a later time.
// Lack of any versioning and conflict resolution makes it only
// suitable for immutable data.
//
// Data is written to 'wr' servers, while only awaiting successful
// confirmation from 'wc' of them in order to minimize latencies on
// the write path.
type ReplicatedClient struct {
	wr, wc int

	w *dnsWatcher

	servers atomic.Value
	initCh  chan struct{}
}

// NewReplicated returns a new replicated client. Servers in the list
// must be in host:port form. You can use names for the host, in which
// case the client will perform a new DNS resolution every minute, and
// it will update the list of servers if their IP addresses
// change. DNS names resolving to multiple IP addresses are supported.
func NewReplicated(servers []string) (*ReplicatedClient, error) {
	if len(servers) < 1 {
		return nil, errors.New("not enough servers")
	}

	rc := &ReplicatedClient{
		wr: 3,
		wc: 1,
		w:  newDNSWatcher(servers),
	}
	go rc.watchForChanges()
	<-rc.initCh

	return rc, nil
}

func (c *ReplicatedClient) Close() {
	c.w.stop()
}

func (c *ReplicatedClient) getServers() []*Client {
	return c.servers.Load().([]*Client)
}

func (c *ReplicatedClient) pickServers(_ []byte, n int) []*Client {
	servers := c.getServers()
	if len(servers) > n {
		servers = servers[:n]
	}
	return servers
}

func (c *ReplicatedClient) watchForChanges() {
	for targets := range c.w.Changes() {
		var servers []*Client
		for _, target := range targets {
			servers = append(servers, New(target))
		}

		if len(servers) < 1 {
			continue
		}

		if c.servers.Swap(servers) == nil {
			close(c.initCh)
		}
	}
}

// Set key, with desired replication 'wr' and confirmation 'wc'.
func (c *ReplicatedClient) Set(ctx context.Context, key, value []byte, ttl time.Duration) error {
	// The channel must buffer at least the number of values we
	// want to read from it.
	ch := make(chan bool, c.wc)
	var wg sync.WaitGroup

	for _, mc := range c.pickServers(key, c.wr) {
		wg.Add(1)
		go func(mc *Client) {
			if err := mc.Set(ctx, key, value, ttl); err == nil {
				// Do not block if the reader is gone.
				select {
				case ch <- true:
				default:
				}
			}
			wg.Done()
		}(mc)
	}

	// Close the channel when all write attempts are done,
	// regardless of their success.
	go func() {
		wg.Wait()
		close(ch)
	}()

	// Just wait for 'wc' confirmations.
	for i := 0; i < c.wc; i++ {
		if ok := <-ch; !ok {
			return errors.New("all servers failed")
		}
	}
	return nil
}

// Get key, returning the first response from any of the servers that might have it.
func (c *ReplicatedClient) Get(ctx context.Context, key []byte) ([]byte, error) {
	ch := make(chan []byte, 1)
	var wg sync.WaitGroup

	// Create a sub-Context that will be canceled after we get the
	// first result, ensuring that no goroutines are left running
	// once we return.
	gctx, cancel := context.WithCancel(ctx)
	defer cancel()

	// Issue a Get request to all selected servers.
	for _, mc := range c.pickServers(key, c.wr) {
		wg.Add(1)
		go func(mc *Client) {
			if value, err := mc.Get(gctx, key); err == nil {
				select {
				case ch <- value:
				default:
				}
			}
			wg.Done()
		}(mc)
	}

	// Close the channel once all read attempts are done.
	go func() {
		wg.Wait()
		close(ch)
	}()

	// No value results in an ErrNotFound.
	value := <-ch
	if value == nil {
		return nil, ErrNotFound
	}
	return value, nil
}
