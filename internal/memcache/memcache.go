package memcache

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"sync"
	"time"

	"github.com/lunixbochs/struc"
)

// Memcache binary protocol magic values.
const (
	magicRequest  = 0x80
	magicResponse = 0x81
)

// Memcache binary protocol opcodes (relevant subset).
const (
	opcodeGet  = 0x00
	opcodeSet  = 0x01
	opcodeSetQ = 0x11
	opcodeDelQ = 0x14
)

// Memcache binary protocol status code (relevant subset).
const (
	statusOK               = 0x00
	statusNotFound         = 0x01
	statusInvalidArguments = 0x04
)

// Return true if the status implies we should close the connection.
func isUnsafeStatus(status uint16) bool {
	return status == statusInvalidArguments || status >= 0x80
}

// ErrNotFound is returned when memcached can't find the key.
var ErrNotFound = errors.New("key not found")

// Request / response header.
type header struct {
	Magic     uint8
	Opcode    uint8
	KeyLen    uint16
	ExtrasLen uint8
	DataType  uint8
	Status    uint16 // or vbucket_id in the request.
	BodyLen   uint32
	Opaque    uint32
	CAS       uint64
}

// Request, including extras, key and value fields.
type request struct {
	hdr    header
	extras []byte
	key    []byte
	value  []byte
	quiet  bool
}

// Response, with decoded extras, key and value fields.
type response struct {
	hdr    header
	extras []byte
	key    []byte
	value  []byte
}

func (r *response) ok() bool {
	return r.hdr.Status == statusOK
}

func (r *response) err() error {
	switch r.hdr.Status {
	case statusNotFound:
		return ErrNotFound
	default:
		s := fmt.Sprintf("status code 0x%02x", r.hdr.Status)
		if len(r.value) > 0 {
			s += ": "
			s += string(r.value)
		}
		return errors.New(s)
	}
}

// Client for the Memcache binary protocol. This is a partial client that only
// supports the Set (actually its "quiet" version SetQ) and Get commands. It
// supports high concurrency on a single connection via request pipelining,
// and its API understand Context semantics such as cancellation.
type Client struct {
	dialer *net.Dialer
	addr   string

	mx  sync.Mutex
	cur *conn
}

// New creates a new Client.
func New(addr string) *Client {
	return &Client{
		dialer: new(net.Dialer),
		addr:   addr,
	}
}

func (c *Client) dialConn(ctx context.Context) (*conn, error) {
	nc, err := c.dialer.DialContext(ctx, "tcp", c.addr)
	if err != nil {
		return nil, err
	}

	return newConn(nc), nil
}

func (c *Client) getConn(ctx context.Context) (*conn, bool, error) {
	c.mx.Lock()
	defer c.mx.Unlock()

	if c.cur != nil {
		return c.cur, false, nil
	}

	conn, err := c.dialConn(ctx)
	if err != nil {
		return nil, false, err
	}
	c.cur = conn
	return conn, true, err
}

func (c *Client) failConn(conn *conn) {
	c.mx.Lock()
	if c.cur == conn {
		_ = conn.Close()
		c.cur = nil
	}
	c.mx.Unlock()
}

func (c *Client) roundtrip(ctx context.Context, req *request) (resp *response, err error) {
	var retry bool
	var conn *conn

	for {
		var isFresh bool
		conn, isFresh, err = c.getConn(ctx)
		if err != nil {
			goto fail
		}

		resp, err = conn.roundtrip(ctx, req)
		if err != nil {
			goto fail
		}

		// Some MC errors might indicate issues at the protocol layer,
		// so we'd better reset the connection.
		if resp != nil && isUnsafeStatus(resp.hdr.Status) {
			c.failConn(conn)
		}

		return //nolint:nakedret

	fail:
		if conn != nil {
			c.failConn(conn)
		}
		// Retry once if we get an error on a stale (cached) connection.
		if isFresh || retry {
			return //nolint:nakedret
		}
		retry = true
	}
}

type setExtras struct {
	Flags      uint32
	Expiration uint32
}

// Set key.
func (c *Client) Set(ctx context.Context, key, value []byte, ttl time.Duration) error {
	extras, err := packToBuf(&setExtras{
		Expiration: uint32(ttl.Seconds()),
	})
	if err != nil {
		return err
	}

	_, err = c.roundtrip(ctx, &request{
		hdr: header{
			Opcode: opcodeSetQ,
		},
		quiet:  true,
		extras: extras,
		key:    key,
		value:  value,
	})
	return err
}

// Get value associated with key, or ErrNotFound.
func (c *Client) Get(ctx context.Context, key []byte) ([]byte, error) {
	resp, err := c.roundtrip(ctx, &request{
		hdr: header{
			Opcode: opcodeGet,
		},
		key: key,
	})
	if err != nil {
		return nil, err
	}
	if !resp.ok() {
		return nil, resp.err()
	}
	return resp.value, nil
}

type conn struct {
	net.Conn

	wmx sync.Mutex

	mx      sync.Mutex
	counter uint32
	pending map[uint32]*promise
}

func newConn(nc net.Conn) *conn {
	c := &conn{
		Conn:    nc,
		pending: make(map[uint32]*promise),
	}
	go c.readloop()
	return c
}

type promise struct {
	// Lock acts as a barrier, so that nil'ing the ch is visible
	// to the promise fulfiller.
	mx sync.Mutex
	ch chan *response
}

func (p *promise) cancel() {
	p.mx.Lock()
	close(p.ch)
	p.ch = nil
	p.mx.Unlock()
}

func (p *promise) fulfill(resp *response) {
	p.mx.Lock()
	if p.ch != nil {
		p.ch <- resp
	}
	p.mx.Unlock()
}

func (c *conn) roundtrip(ctx context.Context, req *request) (*response, error) {
	if req.quiet {
		c.wmx.Lock()
		err := c.writeRequest(ctx, req)
		c.wmx.Unlock()
		return nil, err
	}

	ch := make(chan *response, 1)
	p := &promise{ch: ch}
	defer p.cancel()

	c.mx.Lock()
	req.hdr.Opaque = c.counter
	c.pending[c.counter] = p
	c.counter++
	c.mx.Unlock()

	c.wmx.Lock()
	err := c.writeRequest(ctx, req)
	c.wmx.Unlock()
	if err != nil {
		return nil, err
	}

	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	case resp := <-ch:
		if resp == nil {
			return nil, net.ErrClosed
		}
		return resp, nil
	}
}

func (c *conn) readloop() {
	for {
		resp, err := c.readResponse()
		if err != nil {
			// Cancel pending promises.
			c.mx.Lock()
			for _, p := range c.pending {
				p.fulfill(nil)
			}
			c.mx.Unlock()
			break
		}

		c.mx.Lock()
		p, ok := c.pending[resp.hdr.Opaque]
		delete(c.pending, resp.hdr.Opaque)
		c.mx.Unlock()

		if ok {
			p.fulfill(resp)
		}
	}
}

func (c *conn) writeRequest(ctx context.Context, req *request) error {
	// Translate context deadlines into net.Conn deadlines.
	if deadline, ok := ctx.Deadline(); ok {
		if err := c.SetWriteDeadline(deadline); err != nil {
			return err
		}
	}

	// Interpose a bufio.Writer to pack the output into as fewer
	// network packets as possible.
	w := bufio.NewWriter(c.Conn)

	// Build header and check sizes as we go. Disable gosec warnings
	// because we're checking the values before conversion.
	req.hdr.Magic = magicRequest
	if len(req.extras) > 256 {
		return errors.New("extras size too large")
	}
	req.hdr.ExtrasLen = uint8(len(req.extras)) //#nosec G115
	if len(req.key) > 65536 {
		return errors.New("key too large")
	}
	req.hdr.KeyLen = uint16(len(req.key)) //#nosec G115
	if len(req.value) > (4294967296 - len(req.extras) - len(req.key)) {
		return errors.New("value too large")
	}
	req.hdr.BodyLen = uint32(len(req.key) + len(req.extras) + len(req.value)) //#nosec G115

	if err := struc.Pack(w, &req.hdr); err != nil {
		return err
	}

	for _, part := range [][]byte{req.extras, req.key, req.value} {
		if part != nil {
			if _, err := w.Write(part); err != nil {
				return err
			}
		}
	}

	return w.Flush()
}

func (c *conn) readResponse() (*response, error) {
	var resp response

	// Read into a 24-byte buffer to avoid too many syscalls.
	var hdrbuf [24]byte
	if _, err := io.ReadFull(c.Conn, hdrbuf[:]); err != nil {
		return nil, err
	}
	if err := struc.Unpack(bytes.NewReader(hdrbuf[:]), &resp.hdr); err != nil {
		return nil, err
	}

	if resp.hdr.Magic != magicResponse {
		return nil, errors.New("bad magic number in response")
	}

	if resp.hdr.ExtrasLen > 0 {
		resp.extras = make([]byte, resp.hdr.ExtrasLen)
		if _, err := io.ReadFull(c.Conn, resp.extras); err != nil {
			return nil, err
		}
	}

	if resp.hdr.KeyLen > 0 {
		resp.key = make([]byte, resp.hdr.KeyLen)
		if _, err := io.ReadFull(c.Conn, resp.key); err != nil {
			return nil, err
		}
	}

	valueLen := resp.hdr.BodyLen - uint32(resp.hdr.ExtrasLen) - uint32(resp.hdr.KeyLen)
	if valueLen > 0 {
		resp.value = make([]byte, valueLen)
		if _, err := io.ReadFull(c.Conn, resp.value); err != nil {
			return nil, err
		}
	}

	return &resp, nil
}

func packToBuf(obj any) ([]byte, error) {
	var buf bytes.Buffer
	if err := struc.Pack(&buf, obj); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
