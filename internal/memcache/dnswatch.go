package memcache

import (
	"log"
	"net"
	"sort"
	"strings"
	"time"
)

var dnsCheckInterval = 1 * time.Minute

// Watch a set of DNS hostnames and notify when the list of their
// aggregated resolved IP addresses change.
type dnsWatcher struct {
	addrs    []string
	resolved []string
	ch       chan []string
	stopCh   chan struct{}
}

func newDNSWatcher(addrs []string) *dnsWatcher {
	w := &dnsWatcher{
		addrs:  addrs,
		ch:     make(chan []string, 1),
		stopCh: make(chan struct{}),
	}
	go w.watch()
	return w
}

func (w *dnsWatcher) Changes() <-chan []string {
	return w.ch
}

func (w *dnsWatcher) stop() {
	close(w.stopCh)
}

func (w *dnsWatcher) watch() {
	tick := time.NewTicker(dnsCheckInterval)
	defer tick.Stop()

	for {
		select {
		case <-w.stopCh:
			return

		case <-tick.C:
			resolved, err := resolveAll(w.addrs)
			if err != nil {
				log.Printf("resolving %s: %v", strings.Join(w.addrs, ", "), err)
				continue
			}

			if sortedListEqual(resolved, w.resolved) {
				continue
			}

			w.resolved = resolved
			w.ch <- resolved
		}
	}
}

func resolveAddr(addr string) ([]string, error) {
	host, port, _ := net.SplitHostPort(addr)
	ips, err := net.LookupIP(host)
	if err != nil {
		return nil, err
	}
	out := make([]string, 0, len(ips))
	for _, ip := range ips {
		out = append(out, net.JoinHostPort(ip.String(), port))
	}
	return out, nil
}

func resolveAll(addrs []string) ([]string, error) {
	var out []string
	var outErr error
	for _, addr := range addrs {
		resolved, err := resolveAddr(addr)
		if err == nil {
			out = append(out, resolved...)
		} else {
			outErr = err
		}
	}
	if len(out) == 0 {
		return nil, outErr
	}
	sort.Strings(out)
	return out, nil
}

func sortedListEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
