package netutil

import (
	"context"
	"crypto/x509"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
)

func getPeerCertificateFromContext(ctx context.Context) (*x509.Certificate, bool) {
	p, ok := peer.FromContext(ctx)
	if !ok {
		return nil, false
	}
	mtls, ok := p.AuthInfo.(credentials.TLSInfo)
	if !ok {
		return nil, false
	}
	if len(mtls.State.PeerCertificates) == 0 {
		return nil, false
	}
	return mtls.State.PeerCertificates[0], true
}

// Create a new Context with the mTLS peer certificate CN as GRPC
// metadata so that it can be used with an authz policy.
func newGRPCmTLSContext(ctx context.Context, cert *x509.Certificate) context.Context {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		md = metadata.MD{}
	}
	md.Set("mtls.peer", cert.Subject.CommonName)
	return metadata.NewIncomingContext(ctx, md)
}

// GRPC unary interceptor for authz.
func authUnaryInterceptor(ctx context.Context, req any, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (any, error) {
	cert, ok := getPeerCertificateFromContext(ctx)
	if !ok {
		return nil, status.Error(codes.Unauthenticated, "no client certificate")
	}

	return handler(newGRPCmTLSContext(ctx, cert), req)
}

// wrappedStream wraps a grpc.ServerStream associated with an incoming RPC, and
// a custom context containing the username derived from the authorization header
// specified in the incoming RPC metadata
type wrappedStream struct {
	grpc.ServerStream
	ctx context.Context
}

func (w *wrappedStream) Context() context.Context {
	return w.ctx
}

func newWrappedStream(ctx context.Context, s grpc.ServerStream) grpc.ServerStream {
	return &wrappedStream{s, ctx}
}

// authStreamInterceptor looks up the authorization header from the incoming RPC context,
// retrieves the username from it and creates a new context with the username before invoking
// the provided handler.
func authStreamInterceptor(srv any, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	cert, ok := getPeerCertificateFromContext(ss.Context())
	if !ok {
		return status.Error(codes.Unauthenticated, "no client certificate")
	}

	return handler(srv, newWrappedStream(newGRPCmTLSContext(ss.Context(), cert), ss))
}
