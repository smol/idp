package netutil

import (
	"context"
	"crypto/tls"
	"net"
	"os"
	"sync"

	grpcprom "github.com/grpc-ecosystem/go-grpc-middleware/providers/prometheus"
	"github.com/prometheus/client_golang/prometheus"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/authz"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
)

type GRPCEndpointSpec struct {
	TLSParams `yaml:",inline"`

	Addr              string `yaml:"addr" doc:"GRPC endpoint (host:port)"`
	ServiceConfigJSON string `yaml:"service_config" doc:"additional GRPC client config (JSON-encoded)"`

	init      sync.Once
	connCache *grpcConnCache
}

func (e *GRPCEndpointSpec) DialOptions() ([]grpc.DialOption, error) {
	var creds credentials.TransportCredentials

	if e.SSLCert != "" && e.SSLKey != "" && e.SSLCA != "" {
		tlsconf, err := ClientTLSConfig(e.SSLCert, e.SSLKey, e.SSLCA)
		if err != nil {
			return nil, err
		}
		creds = credentials.NewTLS(tlsconf)
	} else {
		creds = insecure.NewCredentials()
	}

	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(creds),
		grpc.WithStatsHandler(otelgrpc.NewClientHandler()),
	}
	if e.ServiceConfigJSON != "" {
		opts = append(opts, grpc.WithDefaultServiceConfig(e.ServiceConfigJSON))
	}

	return opts, nil
}

type GRPCServerSpec struct {
	TLSParams

	ACL string `yaml:"acl"`
}

var grpcLatencyHistogramBuckets = []float64{0.001, 0.01, 0.1, 0.3, 0.6, 1, 3, 6, 9, 20, 30, 60, 90, 120}

func (s *GRPCServerSpec) ServerOptions() ([]grpc.ServerOption, *grpcprom.ServerMetrics, error) {
	var tlsconf *tls.Config
	if s.SSLCert != "" && s.SSLKey != "" && s.SSLCA != "" {
		var err error
		tlsconf, err = ServerTLSConfig(s.SSLCert, s.SSLKey, s.SSLCA)
		if err != nil {
			return nil, nil, err
		}
	}

	opts := []grpc.ServerOption{
		grpc.StatsHandler(otelgrpc.NewServerHandler()),
	}

	srvMetrics := grpcprom.NewServerMetrics(
		grpcprom.WithServerHandlingTimeHistogram(
			grpcprom.WithHistogramBuckets(grpcLatencyHistogramBuckets),
		),
	)
	prometheus.MustRegister(srvMetrics)
	unaryInts := []grpc.UnaryServerInterceptor{
		srvMetrics.UnaryServerInterceptor(),
	}
	streamInts := []grpc.StreamServerInterceptor{
		srvMetrics.StreamServerInterceptor(),
	}

	if tlsconf == nil {
		opts = append(opts, grpc.Creds(insecure.NewCredentials()))
	} else {
		opts = append(opts, grpc.Creds(credentials.NewTLS(tlsconf)))
		if s.ACL != "" {
			authzInt, err := authz.NewStatic(s.ACL)
			if err != nil {
				return nil, nil, err
			}
			unaryInts = append(unaryInts, authUnaryInterceptor)
			unaryInts = append(unaryInts, authzInt.UnaryInterceptor)
			streamInts = append(streamInts, authStreamInterceptor)
			streamInts = append(streamInts, authzInt.StreamInterceptor)
		}
	}

	opts = append(opts, grpc.ChainUnaryInterceptor(unaryInts...))
	opts = append(opts, grpc.ChainStreamInterceptor(streamInts...))

	return opts, srvMetrics, nil
}

// Run a GRPC server controlled by a Context.
func RunGRPCServerWithContext(ctx context.Context, server *grpc.Server, addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	defer l.Close()

	if f, ok := startTelemetry(ctx); ok {
		defer f()
	}

	go func() {
		<-ctx.Done()
		server.GracefulStop()
	}()

	return server.Serve(l)
}

func init() {
	if s := os.Getenv("GRPC_ENABLE_TRACING"); s != "n" {
		grpc.EnableTracing = true
	}
}
