package netutil

import (
	"context"
	"log"
	"os"
	"sync"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/sdk/trace"
)

// Set up telemetry based on environment variables, check
// https://pkg.go.dev/go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc
// for reference.
func setupOTEL(ctx context.Context) (func(), error) {
	exporter, err := otlptracegrpc.New(
		ctx,
		otlptracegrpc.WithInsecure(),
		otlptracegrpc.WithCompressor("gzip"),
	)
	if err != nil {
		return nil, err
	}

	traceProvider := trace.NewTracerProvider(
		trace.WithBatcher(exporter))

	otel.SetTracerProvider(traceProvider)

	return func() {
		traceProvider.Shutdown(context.Background()) //nolint:errcheck
	}, nil
}

var telemetryOnce sync.Once

// Start telemetry once, returning a cleanup function. We attach the
// lifetime of the tracer with the first call to
// RunGRPCServerWithContext or RunHTTPServerWithContext.
func startTelemetry(ctx context.Context) (cleanup func(), ok bool) {
	telemetryOnce.Do(func() {
		if os.Getenv("OTEL_ENABLE") == "" {
			return
		}

		f, err := setupOTEL(ctx)
		if err != nil {
			log.Printf("error setting up telemetry: %v", err)
		} else {
			cleanup = f
			ok = true
		}
	})
	return
}
