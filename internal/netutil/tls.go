package netutil

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"os"
)

type TLSParams struct {
	SSLCert string `yaml:"ssl_cert" doc:"SSL certificate file"`
	SSLKey  string `yaml:"ssl_key" doc:"SSL private key file"`
	SSLCA   string `yaml:"ssl_ca" doc:"SSL CA"`
}

func ClientTLSConfig(sslCert, sslKey, sslCA string) (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(sslCert, sslKey)
	if err != nil {
		return nil, err
	}
	ca, err := loadCA(sslCA)
	if err != nil {
		return nil, err
	}
	return &tls.Config{
		Certificates: []tls.Certificate{cert},
		MinVersion:   tls.VersionTLS12,
		RootCAs:      ca,
	}, nil
}

func ServerTLSConfig(sslCert, sslKey, sslCA string) (*tls.Config, error) {
	cert, err := tls.LoadX509KeyPair(sslCert, sslKey)
	if err != nil {
		return nil, err
	}
	ca, err := loadCA(sslCA)
	if err != nil {
		return nil, err
	}
	return &tls.Config{
		Certificates:             []tls.Certificate{cert},
		MinVersion:               tls.VersionTLS12,
		PreferServerCipherSuites: true,
		ClientAuth:               tls.RequireAndVerifyClientCert,
		ClientCAs:                ca,
	}, nil
}

func loadCA(path string) (*x509.CertPool, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	cas := x509.NewCertPool()
	if !cas.AppendCertsFromPEM(data) {
		return nil, fmt.Errorf("no certificates could be parsed in %s", path)
	}
	return cas, nil
}
