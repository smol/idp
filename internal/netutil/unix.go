package netutil

import (
	"container/list"
	"errors"
	"net"
	"net/textproto"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/coreos/go-systemd/v22/activation"
	"github.com/gofrs/flock"
)

// Handler for UNIX socket server connections.
type SocketHandler interface {
	ServeConnection(c net.Conn)
}

// SocketServer accepts connections on a UNIX socket, speaking the
// line-based wire protocol, and dispatches incoming requests to the
// wrapped Server.
type SocketServer struct {
	l       net.Listener
	lock    *flock.Flock
	closing atomic.Value
	wg      sync.WaitGroup
	handler SocketHandler

	// Keep track of active connections so we can shut them down
	// on Close.
	connMx sync.Mutex
	conns  list.List
}

func newServer(l net.Listener, lock *flock.Flock, h SocketHandler) *SocketServer {
	s := &SocketServer{
		l:       l,
		lock:    lock,
		handler: h,
	}
	s.closing.Store(false)
	return s
}

// NewUNIXSocketServer returns a new SocketServer listening on the given path.
func NewUNIXSocketServer(socketPath string, h SocketHandler) (*SocketServer, error) {
	// The simplest workflow is: create a new socket, remove it on
	// exit. However, if the program crashes, the socket might
	// stick around and prevent the next execution from starting
	// successfully. We could remove it before starting, but that
	// would be dangerous if another instance was listening on
	// that socket. So we wrap socket access with a file lock.
	lock := flock.New(socketPath + ".lock")
	locked, err := lock.TryLock()
	if err != nil {
		return nil, err
	}
	if !locked {
		return nil, errors.New("socket is locked by another process")
	}

	addr, err := net.ResolveUnixAddr("unix", socketPath)
	if err != nil {
		return nil, err
	}

	// Always try to unlink the socket before creating it.
	os.Remove(socketPath) // nolint

	l, err := net.ListenUnix("unix", addr)
	if err != nil {
		return nil, err
	}

	return newServer(l, lock, h), nil
}

// NewSystemdSocketServer uses systemd socket activation, receiving
// the open socket as a file descriptor on exec.
func NewSystemdSocketServer(h SocketHandler) (*SocketServer, error) {
	listeners, err := activation.Listeners()
	if err != nil {
		return nil, err
	}
	// Our server loop implies a single listener, so find
	// the first one passed by systemd and ignore all others.
	// TODO: listen on all fds.
	for _, l := range listeners {
		if l != nil {
			return newServer(l, nil, h), nil
		}
	}
	return nil, errors.New("no available sockets found")
}

// Close the socket listener and release all associated resources.
// Waits for active connections to terminate before returning.
func (s *SocketServer) Close() {
	s.closing.Store(true)

	// Close the listener to stop incoming connections.
	s.l.Close() // nolint

	// Close all active connections (this will return an error to
	// the client if the connection is not idle).
	s.connMx.Lock()
	for el := s.conns.Front(); el != nil; el = el.Next() {
		el.Value.(net.Conn).Close() // nolint
	}
	s.connMx.Unlock()

	s.wg.Wait()
	if s.lock != nil {
		s.lock.Unlock() // nolint
	}
}

func (s *SocketServer) isClosing() bool {
	return s.closing.Load().(bool)
}

// Serve connections.
func (s *SocketServer) Serve() error {
	for {
		conn, err := s.l.Accept()
		if err != nil {
			if s.isClosing() {
				return nil
			}
			return err
		}

		s.wg.Add(1)

		s.connMx.Lock()
		connEl := s.conns.PushBack(conn)
		s.connMx.Unlock()

		go func() {
			s.handler.ServeConnection(conn)
			conn.Close() //nolint:errcheck
			if !s.isClosing() {
				s.connMx.Lock()
				s.conns.Remove(connEl)
				s.connMx.Unlock()
			}
			s.wg.Done()
		}()
	}
}

// A LineConn is just a slightly improved net/textproto.Conn, with
// timeout support and a byte-oriented API.
type LineConn struct {
	net.Conn

	tconn *textproto.Conn

	WriteTimeout time.Duration
	ReadTimeout  time.Duration
}

// NewLineConn creates a new LineConn. As opposed to the
// textproto.Conn, LineConn owns the net.Conn so you should call
// Close() on it and not on the original connection.
func NewLineConn(conn net.Conn) *LineConn {
	return &LineConn{
		Conn:         conn,
		tconn:        textproto.NewConn(conn),
		ReadTimeout:  10 * time.Minute,
		WriteTimeout: 5 * time.Second,
	}
}

func (c *LineConn) ReadRequest() ([]byte, error) {
	c.Conn.SetReadDeadline(time.Now().Add(c.ReadTimeout)) //nolint:errcheck
	return c.tconn.ReadLineBytes()
}

func (c *LineConn) WriteResponse(response []byte) error {
	c.Conn.SetWriteDeadline(time.Now().Add(c.WriteTimeout)) //nolint:errcheck

	w := c.tconn.W
	if _, err := w.Write(response); err != nil {
		return err
	}
	if _, err := w.WriteString("\r\n"); err != nil {
		return err
	}
	return w.Flush()
}
