package netutil

import (
	"fmt"
	"sync"

	"google.golang.org/grpc"
)

// In order to do simple target selection per request, we maintain a
// cache of grpc.ClientConn objects for each specific target. Since
// these are persistent objects, this is fairly trivial. Once we
// figure out how to do custom load balancing policies in Go, that
// might be a nicer alternative.
//
// Obviously it's silly to think of this as "connection caching for
// GRPC", as GRPC already does that by itself, it's best to see it as
// a collection of named connections. Dial() is probably a bad choice
// for the retrieve-connection method, as it introduces scope issues:
// if the grpc.ClientConn is closed, the cache will break.
type grpcConnCache struct {
	mx    sync.Mutex
	conns map[string]*grpc.ClientConn
	opts  []grpc.DialOption
}

func newConnCache(opts []grpc.DialOption) *grpcConnCache {
	return &grpcConnCache{
		conns: make(map[string]*grpc.ClientConn),
		opts:  opts,
	}
}

func (c *grpcConnCache) Close() error {
	for _, conn := range c.conns {
		_ = conn.Close()
	}
	return nil
}

func (c *grpcConnCache) Dial(addr string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	c.mx.Lock()
	defer c.mx.Unlock()

	if conn, ok := c.conns[addr]; ok {
		return conn, nil
	}

	fullOpts := append([]grpc.DialOption{}, c.opts...)
	fullOpts = append(fullOpts, opts...)
	conn, err := grpc.NewClient(addr, fullOpts...)
	if err != nil {
		return nil, err
	}
	c.conns[addr] = conn
	return conn, nil
}

func (e *GRPCEndpointSpec) Dial(opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	var err error
	e.init.Do(func() {
		var opts []grpc.DialOption
		opts, err = e.DialOptions()
		if err != nil {
			return
		}
		e.connCache = newConnCache(opts)
	})
	if err != nil {
		return nil, err
	}

	return e.connCache.Dial(e.Addr, opts...)
}

func (e *GRPCEndpointSpec) DialShard(shard string, opts ...grpc.DialOption) (*grpc.ClientConn, error) {
	addr := e.Addr
	if shard != "" {
		addr = fmt.Sprintf("%s.%s", shard, e.Addr)
	}
	return e.connCache.Dial(addr, opts...)
}
