package netutil

import (
	"context"
	"net/http"
	"time"

	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
)

var gracefulShutdownTimeout = 10 * time.Second

// Run a HTTP server controlled by a Context.
func RunHTTPServerWithContext(ctx context.Context, server *http.Server) error {
	if f, ok := startTelemetry(ctx); ok {
		defer f()
	}

	server.Handler = otelhttp.NewHandler(server.Handler, "")

	go func() {
		<-ctx.Done()
		sctx, cancel := context.WithTimeout(context.Background(), gracefulShutdownTimeout)
		defer cancel()
		if err := server.Shutdown(sctx); err != nil {
			server.Close() // nolint: errcheck
		}
	}()

	err := server.ListenAndServe()

	// Ignore the ErrServerClosed error, since we're presumably
	// shutting down on purpose.
	if err == http.ErrServerClosed {
		err = nil
	}
	return err
}
