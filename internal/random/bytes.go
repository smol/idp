package random

import (
	"crypto/rand"
	"io"
)

// Bytes reads n random bytes.
func Bytes(n int) []byte {
	b := make([]byte, n)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		panic(err)
	}
	return b
}
