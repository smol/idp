package random

import (
	"encoding/base32"
	"encoding/hex"

	"github.com/google/uuid"
)

// Hex encoded random identifier of the given length.
func Hex(n int) string {
	return hex.EncodeToString(Bytes(n / 2))
}

// Base-32 encoded random identifier of the given length.
func Base32(n int) string {
	s := base32.StdEncoding.EncodeToString(Bytes(1 + (n*5)/8))
	return s[:n]
}

// UUID returns a globally unique random identifier.
func UUID() string {
	return uuid.Must(uuid.NewRandom()).String()
}

func init() {
	// Enable the process-wide random pool to make UUID generation
	// significantly faster.
	uuid.EnableRandPool()
}
