package forms

import (
	"context"
	"fmt"

	"github.com/trustelem/zxcvbn"
	"github.com/trustelem/zxcvbn/match"
)

type feedback struct {
	Warning     string
	Suggestions []string
}

func getFeedback(result zxcvbn.Result) feedback {
	fb := feedback{
		Suggestions: []string{
			"Use a few words, avoid common phrases.",
			"No need for symbols, digits, or uppercase letters.",
		},
	}

	if len(result.Sequence) == 0 {
		return fb
	}

	if result.Score > 2 {
		return feedback{}
	}

	var longestIdx, maxLen int
	for i := 0; i < len(result.Sequence); i++ {
		if n := len(result.Sequence[i].Token); n > maxLen {
			longestIdx = i
			maxLen = n
		}
	}

	fb = getMatchFeedback(result.Sequence[longestIdx], len(result.Sequence) == 1)

	fb.Suggestions = append(fb.Suggestions,
		"Add another word or two. Uncommon words are better")

	return fb
}

func getMatchFeedback(match *match.Match, isSoleMatch bool) feedback {
	switch match.Pattern {
	case "dictionary":
		return getDictionaryMatchFeedback(match, isSoleMatch)

	case "spatial":
		warning := "Short keyboard patterns are easy to guess"
		if match.Turns == 1 {
			warning = "Straight rows of keys are easy to guess"
		}
		return feedback{
			Warning:     warning,
			Suggestions: []string{"Use a longer keyboard pattern with more turns"},
		}

	case "repeat":
		warning := "Repeats like \"abcabcabc\" are only slightly harder to guess than \"abc\""
		if len(match.BaseToken) == 1 {
			warning = "Repeats like \"aaa\" are easy to guess"
		}
		return feedback{
			Warning:     warning,
			Suggestions: []string{"Avoid repeated words and characters"},
		}

	case "sequence":
		return feedback{
			Warning:     "Sequences like abc or 6543 are easy to guess",
			Suggestions: []string{"Avoid sequences"},
		}

	case "regex":
		if match.RegexName == "recent_year" {
			return feedback{
				Warning: "Recent years are easy to guess",
				Suggestions: []string{
					"Avoid recent years",
					"Avoid years that are associated with you",
				},
			}
		}

	case "date":
		return feedback{
			Warning:     "Dates are often easy to guess",
			Suggestions: []string{"Avoid dates and years that are associated with you"},
		}
	}

	return feedback{}
}

func getDictionaryMatchFeedback(match *match.Match, isSoleMatch bool) feedback {
	var warning string
	switch match.DictionaryName {
	case "passwords":
		if isSoleMatch && !match.L33t && !match.Reversed {
			if match.Rank <= 10 {
				warning = "This is a top-10 common password"
			} else if match.Rank <= 100 {
				warning = "This is a top-100 common password"
			} else {
				warning = "This is a very common password"
			}
		} else if match.Guesses <= 10000 {
			warning = "This is similar to a commonly used password"
		}
	case "english_wikipedia":
		if isSoleMatch {
			warning = "A word by itself is easy to guess"
		}
	case "surnames", "male_names", "female_names":
		if isSoleMatch {
			warning = "Names and surnames by themselves are easy to guess"
		} else {
			warning = "Common names and surnames are easy to guess"
		}
	}

	return feedback{
		Warning: warning,
	}
}

func GoodQualityPassword(minScore int, dictionary []string) FieldValidatorFunc {
	return func(_ context.Context, s string) (string, error) {
		result := zxcvbn.PasswordStrength(s, dictionary)
		if result.Score < minScore {
			fb := getFeedback(result)
			return "", fmt.Errorf("%s.", fb.Warning)
		}
		return s, nil
	}
}
