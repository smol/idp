package forms

import (
	"context"
	"net/http"
)

type FieldValidatorFunc func(context.Context, string) (string, error)

type FormValidatorFunc func(context.Context, *Form) error

type Option struct {
	Name  string
	Value string
}

type Field struct {
	Name        string
	Type        string
	Label       string
	Description string

	Choices []Option
	Value   string
	Errors  []string

	Validators []FieldValidatorFunc
}

func (f Field) WithDescription(s string) Field {
	f.Description = s
	return f
}

func (f Field) WithValidators(validators ...FieldValidatorFunc) Field {
	f.Validators = validators
	return f
}

type Form struct {
	Fields     []Field
	Errors     []string
	Validators []FormValidatorFunc
}

func (f *Form) AddError(err error) {
	f.Errors = append(f.Errors, err.Error())
}

func (f *Form) GetField(name string) Field {
	for i := 0; i < len(f.Fields); i++ {
		if f.Fields[i].Name == name {
			return f.Fields[i]
		}
	}
	return Field{}
}

func (f *Form) Get(name string) string {
	return f.GetField(name).Value
}

func (f *Form) WithDefaults(dflt map[string]string) *Form {
	fields := make([]Field, 0, len(f.Fields))
	for _, field := range f.Fields {
		if value, ok := dflt[field.Name]; ok {
			field.Value = value
		}
		fields = append(fields, field)
	}
	return &Form{
		Fields:     fields,
		Validators: f.Validators,
	}
}

func (f *Form) fromRequest(req *http.Request) (*Form, bool) {
	ok := true

	// Create a list of Field objects with Value fields obtained
	// from the request. We can modify 'field' in-place because it
	// is a value not a pointer.
	fields := make([]Field, 0, len(f.Fields))
	for _, field := range f.Fields {
		field.Value = req.FormValue(field.Name)
		for _, vf := range field.Validators {
			var err error
			field.Value, err = vf(req.Context(), field.Value)
			if err != nil {
				ok = false
				field.Errors = append(field.Errors, err.Error())
			}
		}
		fields = append(fields, field)
	}

	out := &Form{Fields: fields}
	for _, vf := range f.Validators {
		if err := vf(req.Context(), out); err != nil {
			ok = false
			out.Errors = append(out.Errors, err.Error())
		}
	}

	return out, ok
}

func (f *Form) ValidateOnSubmit(req *http.Request) (*Form, bool) {
	if req.Method != http.MethodPost {
		return f, false
	}

	return f.fromRequest(req)
}

func NewPasswordField(name, label string) Field {
	return Field{
		Type:  "password",
		Name:  name,
		Label: label,
	}
}

func NewTextField(name, label string) Field {
	return Field{
		Type:  "text",
		Name:  name,
		Label: label,
	}
}

func NewHiddenField(name string) Field {
	return Field{
		Type: "hidden",
		Name: name,
	}
}

func NewSelectField(name, label string, choices []Option) Field {
	return Field{
		Type:    "select",
		Name:    name,
		Label:   label,
		Choices: choices,
	}
}
