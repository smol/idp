package forms

import (
	"context"
	"errors"
)

func FormFieldsEqual(a, b, errMsg string) FormValidatorFunc {
	return func(_ context.Context, form *Form) error {
		if form.Get(a) != form.Get(b) {
			return errors.New(errMsg)
		}
		return nil
	}
}

func FormFieldsDiffer(a, b, errMsg string) FormValidatorFunc {
	return func(_ context.Context, form *Form) error {
		if form.Get(a) == form.Get(b) {
			return errors.New(errMsg)
		}
		return nil
	}
}
