package forms

import (
	"context"
	"errors"
	"fmt"
	"strings"
)

func Strip(_ context.Context, s string) (string, error) {
	return strings.TrimSpace(s), nil
}

func Lower(_ context.Context, s string) (string, error) {
	return strings.ToLower(s), nil
}

func NotEmpty(_ context.Context, s string) (string, error) {
	if s == "" {
		return "", errors.New("field should not be empty")
	}
	return s, nil
}

func MinLength(n int) FieldValidatorFunc {
	return func(_ context.Context, s string) (string, error) {
		if len(s) < n {
			return "", fmt.Errorf("should be at least %d characters", n)
		}
		return s, nil
	}
}

func OneOf(choices []Option) FieldValidatorFunc {
	return func(_ context.Context, s string) (string, error) {
		for i := 0; i < len(choices); i++ {
			if s == choices[i].Value {
				return s, nil
			}
		}
		return "", errors.New("invalid value")
	}
}

func EqualTo(expected string) FieldValidatorFunc {
	return func(_ context.Context, s string) (string, error) {
		if s != expected {
			return "", errors.New("invalid value")
		}
		return s, nil
	}
}
