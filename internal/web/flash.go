package web

import (
	"context"
	"net/http"

	"git.autistici.org/smol/idp/internal/sessionstore"
)

const (
	FlashCategoryError = "error"
	FlashCategoryOK    = "ok"
)

type FlashMessage struct {
	Category string
	Message  string
}

const flashCookieName = "_flmsgs"

type flashContext struct {
	store    *sessionstore.Store
	messages []FlashMessage
}

type flashCtxKeyType int

var flashCtxKey flashCtxKeyType = 1

func WithFlash(authKey, encKey []byte, path string, h http.Handler) http.Handler {
	if path == "" {
		path = "/"
	}
	store := sessionstore.New(flashCookieName, path, authKey, encKey, 0)

	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		flashCtx := &flashContext{
			store: store,
		}
		if err := store.Get(req, &flashCtx.messages); err == nil {
			req = req.WithContext(context.WithValue(req.Context(), flashCtxKey, flashCtx))
		}
		h.ServeHTTP(w, req)
	})
}

func Flash(w http.ResponseWriter, req *http.Request, msg FlashMessage) {
	if flashCtx, ok := req.Context().Value(flashCtxKey).(*flashContext); ok {
		flashCtx.messages = append(flashCtx.messages, msg)
		flashCtx.store.Set(w, flashCtx.messages) //nolint:errcheck
	}
}

func FlashErr(w http.ResponseWriter, req *http.Request, s string) {
	Flash(w, req, FlashMessage{Category: FlashCategoryError, Message: s})
}

func FlashOK(w http.ResponseWriter, req *http.Request, s string) {
	Flash(w, req, FlashMessage{Category: FlashCategoryOK, Message: s})
}

func GetFlashes(w http.ResponseWriter, req *http.Request) []FlashMessage {
	if flashCtx, ok := req.Context().Value(flashCtxKey).(*flashContext); ok {
		flashCtx.store.Delete(w) //nolint:errcheck
		return flashCtx.messages
	}
	return nil
}

func (t *Template) WithFlashes(w http.ResponseWriter, req *http.Request) *Template {
	return t.WithData(map[string]any{
		"Flashes": GetFlashes(w, req),
	})
}
