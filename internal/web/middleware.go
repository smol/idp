package web

import (
	"log"
	"net/http"

	"github.com/gorilla/csrf"
)

type Middleware func(http.Handler) http.Handler

func noopMiddleware(h http.Handler) http.Handler { return h }

func CSRF(key []byte, cookieName, path string) Middleware {
	if len(key) > 0 {
		return csrf.Protect(
			key,
			csrf.CookieName(cookieName),
			csrf.Path(path),
			csrf.Secure(true),
		)
	}

	log.Printf("Warning: csrf key not set, CSRF disabled")
	return noopMiddleware
}
