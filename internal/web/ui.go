package web

import (
	"bytes"
	"embed"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"

	"github.com/gorilla/csrf"
	"github.com/vearutop/statigz"
	"github.com/vearutop/statigz/brotli"
)

var (
	CacheControlMaxAge = 259200
)

// templateData collects "horizontal" template data, such as
// configuration, fields that are always present, etc. The various
// app-specific bits are delegated via 'any' types.
type templateData map[string]any

func (d templateData) Copy() templateData {
	m := make(templateData, len(d))
	for k, v := range d {
		m[k] = v
	}
	return m
}

type AssetInfo struct {
	Path string `json:"path"`
	SRI  string `json:"sri"`
}

type Config struct {
	SiteName string `yaml:"site_name"`
}

// Template wraps a html/template.Template with default parameters and
// an enhanced execution method.
type Template struct {
	*template.Template
	config *Config

	data templateData
}

func (t *Template) Execute(w http.ResponseWriter, req *http.Request, name string) {
	// As a small optimization, we don't need to make a copy of
	// 'data' here since we are setting the same fields on every
	// Execute() call.
	data := t.data
	if data == nil {
		data = make(templateData)
	}
	data["CSRFField"] = csrf.TemplateField(req)
	if t.config != nil {
		data["SiteName"] = t.config.SiteName
		// More UI configuration?
	}

	var buf bytes.Buffer
	if err := t.Template.ExecuteTemplate(&buf, name, data); err != nil {
		log.Printf("template error on %s: %v", req.URL.String(), err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Cache-Control", "no-store")
	io.Copy(w, &buf) //nolint:errcheck
}

func (t *Template) WithData(m map[string]any) *Template {
	data := t.data.Copy()
	for k, v := range m {
		data[k] = v
	}

	return &Template{
		Template: t.Template,
		config:   t.config,
		data:     data,
	}
}

// MustParseTemplates parses and returns the embedded templates.
func MustParseTemplates(templateFS embed.FS, assets map[string]AssetInfo, config *Config) *Template {
	tpl, err := template.New("").Funcs(map[string]any{
		"ToJSON": toJSON,
		"Asset":  getAsset(assets),
	}).ParseFS(templateFS, "templates/*.html", "templates/inc/*.html")
	if err != nil {
		panic(err)
	}
	return &Template{
		Template: tpl,
		config:   config,
	}
}

func getAsset(assetPaths map[string]AssetInfo) func(string) AssetInfo {
	return func(path string) AssetInfo {
		return assetPaths[path]
	}
}

func toJSON(obj any) string {
	data, _ := json.Marshal(obj)
	return string(data)
}

// AssetsHandler is a http.Handler to serve embedded static content.
func AssetsHandler(assetsFS embed.FS) http.Handler {
	srv := statigz.FileServer(assetsFS, brotli.AddEncoding)
	cchdr := fmt.Sprintf("max-age=%d", CacheControlMaxAge)
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Cache-Control", cchdr)
		srv.ServeHTTP(w, req)
	})
}
