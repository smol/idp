package loaders

import "path/filepath"

// ResolvePath returns the path evaluated as relative to base.
func ResolvePath(path, base string) string {
	if !filepath.IsAbs(path) {
		path = filepath.Join(base, path)
	}
	return path
}
