package loaders

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"

	"gopkg.in/yaml.v3"
)

func LoadYAML(path string, obj any) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	return yaml.NewDecoder(f).Decode(obj)
}

func LoadYAMLFromDir(dir, pattern string, obj any) error {
	files, err := filepath.Glob(filepath.Join(dir, pattern))
	if err != nil {
		return err
	}

	sort.Strings(files)

	for _, file := range files {
		if err := LoadYAML(file, obj); err != nil {
			return fmt.Errorf("error loading %s: %w", file, err)
		}
	}

	return nil
}
