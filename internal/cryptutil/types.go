package cryptutil

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"regexp"

	"gopkg.in/yaml.v3"
)

// Binary is a []byte slice that can be safely unmarshaled from YAML.
// For the time being, the value is just decoded as-is, but the custom
// type allows us to switch to other formats in the future such as
// base64 or hex-encoding.
type Binary []byte

// UnmarshalYAML implements the yaml.Unmarshaler interface.
func (b *Binary) UnmarshalYAML(value *yaml.Node) error {
	var s string
	if err := value.Decode(&s); err != nil {
		return err
	}
	*b = []byte(s)
	return nil
}

// SessionAuthenticationKey is a binary type for gorilla.session
// authentication keys, that is YAML-unserializable.
type SessionAuthenticationKey Binary

func (k *SessionAuthenticationKey) UnmarshalYAML(value *yaml.Node) error {
	if err := (*Binary)(k).UnmarshalYAML(value); err != nil {
		return err
	}
	switch len(*k) {
	case 32, 64:
	default:
		return errors.New("session authentication keys must be exactly 32 or 64 bytes long")
	}
	return nil
}

// SessionEncryptionKey is a binary type for gorilla.session
// encryption keys, that is YAML-unserializable.
type SessionEncryptionKey Binary

func (k *SessionEncryptionKey) UnmarshalYAML(value *yaml.Node) error {
	if err := (*Binary)(k).UnmarshalYAML(value); err != nil {
		return err
	}
	switch len(*k) {
	case 16, 24, 32:
	default:
		return errors.New("session encryption keys must be exactly 16, 24 or 32 bytes long")
	}
	return nil
}

type Regexp struct {
	*regexp.Regexp
}

func (r *Regexp) UnmarshalYAML(value *yaml.Node) error {
	var s string
	err := value.Decode(&s)
	if err != nil {
		return err
	}

	r.Regexp, err = regexp.Compile(s)
	return err
}

func unmarshalPEMBlock(value *yaml.Node) (*pem.Block, error) {
	var s string
	if err := value.Decode(&s); err != nil {
		return nil, err
	}

	block, _ := pem.Decode([]byte(s))
	if block == nil {
		return nil, errors.New("no PEM block found")
	}

	return block, nil
}

// A rsa.PrivateKey wrapper that can be deserialized from YAML as a
// PEM-encoded string value.
type RSAPrivateKey struct {
	*rsa.PrivateKey
}

func (k *RSAPrivateKey) UnmarshalYAML(value *yaml.Node) error {
	block, err := unmarshalPEMBlock(value)
	if err != nil {
		return err
	}

	switch block.Type {
	case "PRIVATE KEY":
		key, err := x509.ParsePKCS8PrivateKey(block.Bytes)
		if err != nil {
			return fmt.Errorf("error parsing PKCS8 private key: %w", err)
		}

		switch rsaKey := key.(type) {
		case *rsa.PrivateKey:
			k.PrivateKey = rsaKey
		default:
			return errors.New("unsupported private key type")
		}

	case "RSA PRIVATE KEY":
		key, err := x509.ParsePKCS1PrivateKey(block.Bytes)
		if err != nil {
			return fmt.Errorf("error parsing PKCS1 private key: %w", err)
		}
		k.PrivateKey = key

	default:
		return errors.New("unexpected PEM block found in data")
	}

	return nil
}

// A rsa.PublicKey wrapper that can be deserialized from YAML as a
// PEM-encoded string value.
type RSAKey struct {
	*rsa.PublicKey
}

func (k *RSAKey) UnmarshalYAML(value *yaml.Node) error {
	block, err := unmarshalPEMBlock(value)
	if err != nil {
		return err
	}

	switch block.Type {
	case "PUBLIC KEY":
		key, err := x509.ParsePKIXPublicKey(block.Bytes)
		if err != nil {
			return fmt.Errorf("error parsing public key: %w", err)
		}

		switch rsaKey := key.(type) {
		case *rsa.PublicKey:
			k.PublicKey = rsaKey
		default:
			return errors.New("unsupported public key type")
		}

	case "RSA PUBLIC KEY":
		key, err := x509.ParsePKCS1PublicKey(block.Bytes)
		if err != nil {
			return fmt.Errorf("error parsing PKCS1 private key: %w", err)
		}
		k.PublicKey = key

	default:
		return errors.New("unexpected PEM block found in data")
	}

	return nil
}
