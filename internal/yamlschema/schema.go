package yamlschema

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"reflect"
	"strings"
	"time"

	"git.autistici.org/smol/idp/internal/cryptutil"
	"gopkg.in/yaml.v3"
)

// DumpSchema dumps a struct schema (recursively) to 'w', following
// the same semantics as the YAML marshaler. In addition, struct
// fields that have the 'doc' tag will have that value displayed as
// documentation.
func DumpSchema(w io.Writer, obj any, prefix string) {
	bufw := bufio.NewWriter(w)
	defer bufw.Flush() //nolint:errcheck

	fmt.Fprintf(bufw, "\n")

	f := &formatter{w: bufw, envPrefix: prefix}
	walkRecursive(reflect.ValueOf(obj), nil, nil, true, f.printField)
}

// Fill in an object with variables from the environment.
func FillFromEnvironment(obj any, prefix string) {
	s := &setter{prefix: prefix}
	walkRecursive(reflect.ValueOf(obj), nil, nil, true, s.setFieldFromEnv)
}

type formatter struct {
	w         io.Writer
	envPrefix string
}

func (f *formatter) printField(val reflect.Value, path []string, field *reflect.StructField, real bool) {
	if field == nil {
		return
	}

	var col1, col2, col3 string

	if len(path) > 0 {
		col1 = path[len(path)-1]
	}

	if isLeafType(val.Type()) {
		col2 = typeName(val.Type())
		// For settable types, print the environment variable that can
		// be used to set them, and the default if different from the
		// zero value.
		if real && isSettableType(val.Type()) {
			envVar := f.envPrefix + strings.ToUpper(strings.Join(path, "_"))
			col3 = fmt.Sprintf("(env var: %s)", envVar)
			if isInterestingValue(val) {
				col3 += fmt.Sprintf(" (default: %v)", val)
			}
		}
	} else {
		switch val.Kind() {
		case reflect.Slice:
			col1 += " []"
		case reflect.Map:
			col1 += " map[string]{}"
		}
	}

	// Get the field docstring from the 'doc' tag.
	if s := field.Tag.Get("doc"); s != "" {
		col3 = capitalize(s) + " " + col3
	}

	fmt.Fprintf(f.w, "%-30s %-25s %s\n", offsetStr(len(path))+col1, col2, col3)
}

type setter struct {
	prefix string
}

func (s *setter) setFieldFromEnv(val reflect.Value, path []string, field *reflect.StructField, real bool) {
	if field == nil || !real {
		return
	}
	if !isLeafType(val.Type()) || !isSettableType(val.Type()) {
		return
	}

	envVar := s.prefix + strings.ToUpper(strings.Join(path, "_"))
	valStr := os.Getenv(envVar)
	if valStr == "" {
		return
	}

	log.Printf("setting field %s from env var %s", strings.Join(path, "."), envVar)
	setValueFromString(val, valStr)
}

// Use YAML to unmarshal, so that values with special unmarshalers are treated
// equally coming from the configuration or from the environment.
func setValueFromString(val reflect.Value, s string) {
	var marshaled []byte

	switch val.Kind() {
	case reflect.Bool:
		marshaled = []byte(s)
	case reflect.Float64:
		marshaled = []byte(s)
	case reflect.Int:
		marshaled = []byte(s)
	default:
		marshaled, _ = yaml.Marshal(s)
	}

	if err := yaml.Unmarshal(marshaled, val.Addr().Interface()); err != nil {
		log.Printf("unmarshal error: %v -> %v", val, err)
	}
}

func isSettableType(t reflect.Type) bool {
	_, ok := settableTypes[t]
	return ok
}

var (
	knownTypes    map[reflect.Type]string
	settableTypes map[reflect.Type]struct{}
)

func init() {
	knownTypes = make(map[reflect.Type]string)
	settableTypes = make(map[reflect.Type]struct{})

	// Complex types with known descriptions.
	knownTypes[reflect.TypeOf(yaml.Node{})] = "<parameters...>"
	knownTypes[reflect.TypeOf(cryptutil.RSAKey{})] = "RSA public key"
	knownTypes[reflect.TypeOf(cryptutil.RSAPrivateKey{})] = "RSA private key"
	knownTypes[reflect.TypeOf(cryptutil.Binary{})] = "string"
	knownTypes[reflect.TypeOf(cryptutil.SessionEncryptionKey{})] = "string (16/32 bytes)"
	knownTypes[reflect.TypeOf(cryptutil.SessionAuthenticationKey{})] = "string (32/64 bytes)"

	// Types that can be set from the environment.
	settableTypes[reflect.TypeOf(cryptutil.RSAKey{})] = struct{}{}
	settableTypes[reflect.TypeOf(cryptutil.RSAPrivateKey{})] = struct{}{}
	settableTypes[reflect.TypeOf(cryptutil.Binary{})] = struct{}{}
	settableTypes[reflect.TypeOf(cryptutil.SessionEncryptionKey{})] = struct{}{}
	settableTypes[reflect.TypeOf(cryptutil.SessionAuthenticationKey{})] = struct{}{}
	settableTypes[reflect.TypeOf("")] = struct{}{}
	settableTypes[reflect.TypeOf(0)] = struct{}{}
	settableTypes[reflect.TypeOf(false)] = struct{}{}
	settableTypes[reflect.TypeOf(time.Duration(0))] = struct{}{}
}

type walkFunc func(reflect.Value, []string, *reflect.StructField, bool)

//nolint:gocognit
func walkRecursive(val reflect.Value, path []string, field *reflect.StructField, real bool, fn walkFunc) {
	if isLeafType(val.Type()) {
		fn(val, path, field, real)
		return
	}

	switch val.Kind() {
	case reflect.Ptr:
		// To get the actual value of the original we have to call Elem()
		// At the same time this unwraps the pointer so we don't end up in
		// an infinite recursion
		unwrapped := val.Elem()

		// Check if the pointer is nil, replace it with new zero object.
		if !unwrapped.IsValid() {
			newUnwrapped := reflect.New(val.Type().Elem())
			walkRecursive(newUnwrapped, path, field, real, fn)
			return
		}

		walkRecursive(unwrapped, path, field, real, fn)

	case reflect.Slice, reflect.Map:
		unwrapped := reflect.New(val.Type().Elem())
		fn(val, path, field, real)
		walkRecursive(unwrapped, path, nil, false, fn)

	case reflect.Interface:
		unwrapped := val.Elem()
		walkRecursive(unwrapped, path, field, real, fn)

	case reflect.Struct:
		fn(val, path, field, real)

		for _, f := range reflect.VisibleFields(val.Type()) {
			if f.Anonymous {
				fval := val.FieldByIndex(f.Index)
				// Nil anonymous field? Create an empty
				// placeholder so that subsequent
				// FieldByIndex() calls do not panic.
				if fval.Kind() == reflect.Ptr && !fval.Elem().IsValid() {
					fval.Set(reflect.New(fval.Type().Elem()))
				}
			}
			if f.Anonymous || !f.IsExported() {
				continue
			}

			fieldName, inline, skip := parseTag("yaml", f)
			if skip {
				continue
			}
			fieldPath := concat(path, fieldName)
			if inline {
				fieldPath = path
			}

			walkRecursive(val.FieldByIndex(f.Index), fieldPath, &f, real, fn)
		}

	default:
		fn(val, path, field, real)
	}

}

func parseTag(tagName string, field reflect.StructField) (fieldName string, inline bool, skip bool) {
	fieldName = field.Name

	tag, ok := field.Tag.Lookup(tagName)
	if !ok {
		return
	}

	parts := strings.Split(tag, ",")

	if parts[0] == "-" {
		skip = true
		return
	}

	fieldName = parts[0]

	for _, flag := range parts[1:] {
		if flag == "inline" {
			inline = true
		}
	}

	return
}

func typeName(t reflect.Type) string {
	if name, ok := knownTypes[t]; ok {
		return name
	}
	switch t.Kind() {
	case reflect.Map:
		return "map[string]" + typeName(t.Elem())
	case reflect.Slice:
		return "[]" + typeName(t.Elem())
	default:
		return t.String()
	}
}

func isLeafType(t reflect.Type) bool {
	if _, ok := knownTypes[t]; ok {
		return true
	}
	switch t.Kind() {
	case reflect.Ptr, reflect.Slice, reflect.Map:
		return isLeafType(t.Elem())
	case reflect.Struct:
		return false
	default:
		return true
	}
}

func isInterestingValue(val reflect.Value) bool {
	return val.IsValid() && !val.IsZero()
}

func concat(a []string, b string) []string {
	l := append([]string{}, a...)
	l = append(l, b)
	return l
}

func offsetStr(offset int) string {
	var s string
	for i := 0; i < offset; i++ {
		s += "  "
	}
	return s
}

func capitalize(s string) string {
	return strings.ToUpper(s[:1]) + s[1:]
}
